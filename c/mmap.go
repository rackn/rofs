package c

import (
	"io"
)

type mmapedFile struct {
	Buffer
}

type MmappedSrc interface {
	io.Closer
	Src
}

// Mmap will return an open memory-mapped version of src
// if src is really an *os.File, otherwise it will return nil.
// The caller is responsible for making sure that the resulting object is
// Closed when you are finished with it.  When the returned MmapedSrc
// is used as an argument to rofs.Open or the individual package Open
// functions, the common routines in c will recognize that the backing
// file is memory-mapped and optimize handling to reduce the number of data
// copies that are required as part of extent handling, and using io.Copy
// and friends will default to zero copy behaviour to the extent possible.
// Use of Mmap is optional.
func Mmap(src Src) MmappedSrc {
	return mmap(src)
}
