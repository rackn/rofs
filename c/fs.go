// Package c provides common interfaces and functionality for all
// supported read-only filesystems.
package c

import (
	"io"
	"io/fs"
	"path"
	"sort"
	"strings"
	"syscall"
)

// Src is anything we can use to provide a filesystem.
// All filesystems using the common routines in this package
// also provide fs.Files that satisfy this interface.
type Src interface {
	io.ReadSeeker
	io.ReaderAt
}

// ByteFetcher is satisfied by anything that can directly return a byte slice
// from an io.ReaderAt without needing to perform extra IO or byte copies.
// The intent is to eventually use this and mmap to optimize IO.
type ByteFetcher interface {
	io.ReaderAt
	WithBytesAt(offset int64, size uint, f func([]byte) error) error
}

type ourProvider interface {
	Src
	io.WriterTo
}

// Inode is what filesystems in this package must
// provide when the common routines want to access a file in
// a filesystem.
type Inode interface {
	fs.DirEntry
	fs.FileInfo
	Open() *Reader
	IsSymlink() bool
	ReadLink() (string, error)
	ReadDir() ([]fs.DirEntry, error)
	ReadXattrs() map[string]string
}

// InodeProvider is the interface that all filesystems using these
// common routines must satisfy file lookup requests.  The common
// code handles looking up inodes to properly handle symlink
// resolution, which can get tricky.
type InodeProvider interface {
	Root() Inode
	Close() error
}

func pathifyErr(op, p string, err error) error {
	if err == nil {
		return nil
	}
	return &fs.PathError{Op: op, Path: p, Err: err}
}

// File implements fs.File and Src.
type File struct {
	ourProvider
	ino        Inode
	dentOffset int
}

var _ fs.File = &File{}
var _ Src = &File{}

// Close closes a File.
func (f *File) Close() error {
	if f.ino == nil {
		return fs.ErrClosed
	}
	f.ino = nil
	f.ourProvider = NoData{fs.ErrClosed}
	return nil
}

// Stat gets the fs.FileInfo for an open File.  We just return
// the Inode, which satisfies the fs.FileInfo interface.
func (f *File) Stat() (fs.FileInfo, error) {
	if f.ino == nil {
		return nil, fs.ErrClosed
	}
	return f.ino, nil
}

// ReadDir returns the requested directory entries, or an error
// if the File is not a directory.
func (f *File) ReadDir(count int) (dents []fs.DirEntry, err error) {
	if f.ino == nil {
		return nil, fs.ErrClosed
	}
	dents, err = f.ino.ReadDir()
	if err != nil || count <= 0 {
		f.dentOffset = 0
		return
	}
	if f.dentOffset >= len(dents) {
		f.dentOffset = 0
		return nil, io.EOF
	}
	if f.dentOffset+count > len(dents) {
		count = len(dents) - f.dentOffset
	}
	res := dents[f.dentOffset : count+f.dentOffset]
	f.dentOffset += count
	return res, nil
}

func (f *File) ReadXattrs() (map[string]string, error) {
	if f.ino == nil {
		return nil, fs.ErrInvalid
	}
	return f.ino.ReadXattrs(), nil
}

// Filesystem provides common functionality needed to implement fs.FS and
// associated interfaces in a standard way for all supported read-only filesystems.
type Filesystem struct {
	InodeProvider
}

var _ fs.FS = Filesystem{}

// Lookup returns the inode for a given path. If followSymlink is false and
// a symlink is found in the path, it will be followed anyway. If however the
// target file is a symlink, then its inode will be returned.
// There are caching opportunities here.
func (f Filesystem) Lookup(name string, followSymlinks bool) (ent Inode, err error) {
	if !fs.ValidPath(name) {
		return nil, &fs.PathError{Op: "stat", Path: name, Err: fs.ErrInvalid}
	}
	parts := strings.Split(name, "/")
	if parts[0] == "." {
		ent = f.Root()
		return
	}
	// If we follow more than 128 symlinks, just assume it is an infinite loop
	// of symlinks and fail.
	for count := 128; count > 0; count-- {
		ent = f.Root()
		var link string
		for offset := 0; offset <= len(parts); offset++ {
			if offset == len(parts) {
				return ent, nil
			}
			var subs []fs.DirEntry
			subs, err = ent.ReadDir()
			if err != nil {
				return nil, fs.ErrNotExist
			}
			i, found := sort.Find(len(subs), func(i int) int {
				return strings.Compare(parts[offset], subs[i].Name())
			})
			if !found {
				return nil, fs.ErrNotExist
			}
			ent = subs[i].(Inode)
			if link, err = ent.ReadLink(); err == nil && (offset < len(parts)-1 || followSymlinks) {
				if parts, err = ProcessSymlink(parts, offset, link); err == nil {
					break
				}
				return nil, err
			}
		}
	}
	return nil, syscall.ETOOMANYREFS
}

// Open opens the fs.File at a p.
func (f Filesystem) Open(p string) (res fs.File, err error) {
	var ent Inode
	if ent, err = f.Lookup(p, true); err == nil {
		fi := &File{ino: ent}
		if ent.IsDir() {
			fi.ourProvider = NoData{Err: syscall.EISDIR}
		} else if ent.Type().IsRegular() {
			rdr := ent.Open()
			rdr.siz = ent.Size()
			fi.ourProvider = rdr
		} else {
			fi.ourProvider = NoData{Err: fs.ErrInvalid}
		}
		res = fi
	}
	err = pathifyErr("open", p, err)
	return
}

// Stat returns the fs.FileInfo for the File at path.  It will follow
// all symbolic links.
func (f Filesystem) Stat(path string) (fi fs.FileInfo, err error) {
	fi, err = f.Lookup(path, true)
	err = pathifyErr("stat", path, err)
	return
}

// Lstat returns the fs.FileInfo for the File at path.  If the final
// target is a symbolic link, the stat information for the link will be
// returned instead of following the symbolic link.
func (f Filesystem) Lstat(path string) (fi fs.FileInfo, err error) {
	fi, err = f.Lookup(path, false)
	err = pathifyErr("lstat", path, err)
	return
}

// ReadLink reads the symbolic link target at p.
// If p does not resolve to a link, an error will be returned.
func (f Filesystem) ReadLink(p string) (link string, err error) {
	var ent Inode
	ent, err = f.Lookup(p, false)
	if err == nil {
		link, err = ent.ReadLink()
	}
	err = pathifyErr("readlink", p, err)
	return
}

// ReadDir returns the directory listings at p, or returns an error
// if p is not a directory.
func (f Filesystem) ReadDir(p string) (res []fs.DirEntry, err error) {
	var ent Inode
	if ent, err = f.Lookup(p, true); err == nil {
		res, err = ent.ReadDir()
	}
	err = pathifyErr("readdir", p, err)
	return
}

// ReadXattrs returns the extended attributes associated with p, or return nil
// if there are no extended attributes or if we cannot read extended attributes from
// the underlying archive.
func (f Filesystem) ReadXattrs(p string) (map[string]string, error) {
	if ent, err := f.Lookup(p, true); err == nil {
		return ent.ReadXattrs(), nil
	} else {
		return nil, pathifyErr("readxattrs", p, err)
	}
}

// ProcessSymlink handles generating a new target path given a source path , the offset in the source path,
// that the symbolic link was found at, and a symbolic link.
func ProcessSymlink(parts []string, offset int, sl string) ([]string, error) {
	if sl[0] == '/' {
		sl = path.Join(sl[1:], path.Join(parts[offset+1:]...))
	} else {
		sl = path.Join(
			path.Join(parts[:offset]...),
			sl,
			path.Join(parts[offset+1:]...))
	}
	if !fs.ValidPath(sl) {
		return nil, fs.ErrInvalid
	}
	return strings.Split(sl, "/"), nil
}
