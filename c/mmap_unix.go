//go:build darwin || dragonfly || freebsd || linux || openbsd || solaris || netbsd

package c

import (
	"math"
	"os"
	"runtime"
	"syscall"
)

func (m *mmapedFile) Close() error {
	data := []byte(m.SimpleBuffer)
	runtime.SetFinalizer(m, nil)
	return syscall.Munmap(data)
}

func mmap(src Src) MmappedSrc {
	fi, ok := src.(*os.File)
	if !ok {
		return nil
	}
	st, err := fi.Stat()
	if err != nil || st.Size() < 1 || st.Size() > math.MaxInt {
		return nil
	}
	data, err := syscall.Mmap(int(fi.Fd()), 0, int(st.Size()), syscall.PROT_READ, syscall.MAP_SHARED)
	if err != nil {
		return nil
	}
	res := &mmapedFile{
		Buffer: Buffer{SimpleBuffer: data},
	}
	runtime.SetFinalizer(res, (*mmapedFile).Close)
	return res
}
