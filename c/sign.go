package c

import (
	"crypto"
	"crypto/ed25519"
	"crypto/rand"
	"io/fs"
	"os"
)

const SignTag = "user.drpsign"

func SignHash(key ed25519.PrivateKey, sum []byte) (signature []byte, err error) {
	signature, err = key.Sign(rand.Reader, sum, crypto.Hash(0))
	return
}

// SignFile signs the SHA256sum of the provided fi with the provided signature.
// The signature is stored in the SignTag extended attribute upon success.
func SignFile(key ed25519.PrivateKey, fi *os.File) (err error) {
	mta := &ModTimeSha{}
	if _, err = mta.Regenerate(fi); err != nil {
		return err
	}
	var signature []byte
	signature, err = SignHash(key, mta.ShaSum)
	if err != nil {
		return err
	}
	return SaveXattr(fi, SignTag, string(signature))
}

// CheckSignatures checks to see if the SHA256sum of fi (as recorded in the MTATag extended attribute)
// has been signed with one of the provided keys.  It returns fs.ErrNotExist if neither the MTATag
// nor the SignTag extended attributes are valid, fs.ErrInvalid if the MTATag is out of date or the
// SignTag fails validation with any of the provided keys. If the file is signed appropriately, then
// it returns nil.
func CheckSignatures(fi fs.File, keys ...ed25519.PublicKey) (err error) {
	mta := &ModTimeSha{}
	if err = mta.ReadFromXattr(fi); err != nil {
		return
	}
	if !mta.UpToDate(fi) {
		err = fs.ErrInvalid
		return
	}
	var signature string
	signature, err = ReadXattr(fi, SignTag)
	if err != nil {
		return err
	}
	sig := []byte(signature)
	for _, k := range keys {
		if ed25519.Verify(k, mta.ShaSum, sig) {
			return nil
		}
	}
	return fs.ErrInvalid
}
