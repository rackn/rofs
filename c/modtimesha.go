package c

import (
	"crypto/sha256"
	"encoding/hex"
	"github.com/pkg/xattr"
	"io"
	"io/fs"
	"io/ioutil"
	"os"
	"runtime"
	"time"
)

const MTATag = "user.drpetag"

type ModTimeSha struct {
	ModTime time.Time
	ShaSum  []byte
}

func SaveXattr(fi *os.File, k, v string) (err error) {
	if runtime.GOOS != "windows" {
		var st os.FileInfo
		st, err = fi.Stat()
		if err != nil {
			return err
		}
		mode := st.Mode()
		if mode&0600 != 0600 {
			if err = os.Chmod(fi.Name(), mode|0600); err != nil {
				return err
			}
		}
		err = xattr.FSet(fi, k, []byte(v))
		if err == nil && mode&0600 != 0600 {
			err = os.Chmod(fi.Name(), mode)
		}
	} else {
		err = ioutil.WriteFile(fi.Name()+":"+k, []byte(v), 0600)
	}
	return err
}

func ReadXattr(fi fs.File, k string) (string, error) {
	var buf []byte
	var err error
	switch realFi := fi.(type) {
	case *os.File:
		if runtime.GOOS != "windows" {
			buf, err = xattr.FGet(realFi, k)
		} else {
			buf, err = ioutil.ReadFile(realFi.Name() + ":" + k)
		}
	case *File:
		xattrs := realFi.ino.ReadXattrs()
		if xattrs == nil {
			return "", fs.ErrNotExist
		}
		v, ok := xattrs[k]
		if !ok {
			return "", fs.ErrNotExist
		}
		buf = []byte(v)
	default:
		return "", fs.ErrNotExist
	}
	if err != nil {
		return "", fs.ErrNotExist
	}
	return string(buf), nil
}

func (m *ModTimeSha) MarshalBinary() ([]byte, error) {
	res := []byte{}
	res = append(res, byte(len(m.ShaSum)))
	res = append(res, m.ShaSum...)
	t, err := m.ModTime.MarshalBinary()
	if err != nil {
		return nil, err
	}
	return append(res, t...), nil
}

func (m *ModTimeSha) UnmarshalBinary(buf []byte) error {
	hStop := int(buf[0] + 1)
	m.ShaSum = append([]byte{}, buf[1:hStop]...)
	return m.ModTime.UnmarshalBinary(buf[hStop:])
}

func (m *ModTimeSha) String() string {
	return hex.EncodeToString(m.ShaSum)
}

func (m *ModTimeSha) UpToDate(fi fs.File) bool {
	stat, err := fi.Stat()
	return err == nil && !stat.IsDir() && stat.ModTime().Equal(m.ModTime)
}

func (m *ModTimeSha) GenerateStat(fi fs.File, stat fs.FileInfo) error {
	if stat.IsDir() {
		return fs.ErrInvalid
	}
	sk, ok := fi.(io.Seeker)
	if !ok {
		return fs.ErrInvalid
	}
	mtime := stat.ModTime()
	if _, err := sk.Seek(0, io.SeekStart); err != nil {
		return err
	}
	shasum := sha256.New()
	_, err := io.CopyN(shasum, fi, stat.Size())
	sk.Seek(0, io.SeekStart)
	if err != nil {
		return err
	}
	m.ModTime = mtime
	m.ShaSum = shasum.Sum(nil)
	return nil
}

func (m *ModTimeSha) Generate(fi fs.File) error {
	stat, err := fi.Stat()
	if err != nil {
		return err
	}
	return m.GenerateStat(fi, stat)
}

func (m *ModTimeSha) Regenerate(fi *os.File) (bool, error) {
	present := m.ReadFromXattr(fi)
	if present == nil && m.UpToDate(fi) {
		return true, nil
	}
	err := m.Generate(fi)
	if err != nil {
		return false, err
	}
	return false, m.SaveToXattr(fi)
}

func (m *ModTimeSha) ReadFromXattr(fi fs.File) error {
	buf, err := ReadXattr(fi, MTATag)
	if err != nil || m.UnmarshalBinary([]byte(buf)) != nil {
		return fs.ErrNotExist
	}
	return nil
}

func (m *ModTimeSha) SaveToXattr(fi *os.File) error {
	xb, _ := m.MarshalBinary()
	return SaveXattr(fi, MTATag, string(xb))
}

func ReadMTA(s fs.FS, p string) (res *ModTimeSha, upToDate bool, err error) {
	var fi fs.File
	fi, err = s.Open(p)
	if fi != nil {
		defer fi.Close()
	}
	if err != nil {
		return
	}
	res = &ModTimeSha{}
	if err = res.ReadFromXattr(fi); err != nil {
		return
	}
	upToDate = res.UpToDate(fi)
	return
}
