package c

import (
	"io"
	"io/fs"
	"sync"
)

type z struct{}

func (zz z) ReadAt(buf []byte, _ int64) (int, error) {
	for i := range buf {
		buf[i] = 0
	}
	return len(buf), nil
}

func (zz z) Read(buf []byte) (int, error) {
	return zz.ReadAt(buf, 0)
}

var bufs = &sync.Pool{New: func() any { return make([]byte, 32*1024) }}

func getBuf() []byte {
	return bufs.Get().([]byte)
}

func putBuf(v []byte) {
	bufs.Put(v)
}

// Zeroes is an io.Reader and an io.ReaderAt that returns an
// endless stream of zeroes.
var Zeroes io.ReaderAt = z{}

// extent is a range of data in a backing store for a segment of a File.
type extent struct {
	src          io.ReaderAt
	offset, size int64
}

// Extents keep track of byte ranges and offsets in underlying Filesystems.
type Extents []extent

// extentsPool is used to reduce allocation costs for Extents.
var extentsPool = [5]sync.Pool{
	sync.Pool{}, sync.Pool{}, sync.Pool{}, sync.Pool{}, sync.Pool{},
}

func szdx(size int) (int, int) {
	switch {
	case size <= 1:
		return 0, 1
	case size <= 4:
		return 1, 4
	case size <= 16:
		return 2, 16
	case size <= 32:
		return 3, 32
	default:
		return 4, size
	}
}

// AllocExtents allocates Extents.  The passed size is used to separate
// requested extent sizes into buckets and allow different sizes of Extents
// to have their own pools.  Extents allocated using this function will be released
// internally as part of Reader.ReadAt and Reader.WriteTo.
//
// Extents returned from this function will have 0 length.
func AllocExtents(size int) Extents {
	idx, sz := szdx(size)
	extents := extentsPool[idx].Get()
	if extents == nil {
		return make(Extents, 0, sz)
	}
	ex := extents.(Extents)
	if cap(ex) < size {
		extentsPool[idx].Put(ex)
		return make(Extents, 0, sz)
	}
	return ex[:0]
}

func releaseExtents(ex Extents) {
	for i := range ex {
		ex[i].src = nil
		ex[i].offset = 0
		ex[i].size = 0
	}
	idx, _ := szdx(cap(ex))
	extentsPool[idx].Put(ex)
}

// Append a range to the passed-in Extents.
func (ex Extents) Append(src io.ReaderAt, offset int64, size int64) Extents {
	if src == nil || src == Zeroes {
		return append(ex, extent{src: Zeroes, size: size})
	}
	if ours, ok := src.(*File); ok {
		other := ours.ino.Open().ex(offset, size)
		ex = append(ex, other...)
		releaseExtents(other)
		return ex
	}
	if m, ok := src.(*mmapedFile); ok {
		return append(ex, extent{src: &Buffer{SimpleBuffer: m.SimpleBuffer[offset : offset+size]}, size: size})
	}
	return append(ex, extent{src: src, offset: offset, size: size})
}

// ReadAt reads bytes at a particular offset in an extent.
func (e extent) ReadAt(buf []byte, offset int64) (int, error) {
	if offset < 0 || offset >= e.size {
		return 0, io.EOF
	}
	rd := int64(len(buf))
	if overflow := e.size - (offset + rd); overflow < 0 {
		rd += overflow
	} else {
		return e.src.ReadAt(buf, offset+e.offset)
	}
	n, err := e.src.ReadAt(buf[:rd], offset+e.offset)
	if err == nil {
		err = io.EOF
	}
	return n, err
}

// GetExtents is a function that fetches a sufficient number of
// Extents to satisfy the Read or ReadAt request.  There must be
// no gaps in the Extents returned -- the Offset of extent n+1 must
// equal Offset + Size of extent n.  If no Extents are
// returned, the Reader functions will interpret that as io.EOF,
// and if the aggregate size of the Extents is not equal to size,
// the Reader functions will interpret that as trying to read past the
// end of the File and handle that appropriately.
type GetExtents func(offset, size int64) Extents

// Reader creates a *Reader from the GetExtents iterator.
func (e GetExtents) Reader() *Reader {
	return &Reader{ex: e}
}

// extentReader implements io.Reader, io.Seeker, and io.WriterTo for an extent
type extentReader struct {
	extent
	pos int64
}

// Read implements io.Reader for an extentReader
func (e *extentReader) Read(buf []byte) (int, error) {
	if e.pos < 0 || e.pos >= e.size {
		return 0, io.EOF
	}
	res, err := e.ReadAt(buf, e.pos)
	e.pos += int64(res)
	return res, err
}

// writeTo implements io.WriterTo for the extentReader
func (e *extentReader) writeTo(w io.Writer) (written int64, err error) {
	if br, ok := e.src.(ByteFetcher); ok {
		err = br.WithBytesAt(e.pos, uint(e.size-e.pos), func(buf []byte) error {
			var n int
			for len(buf) > 0 {
				n, err = w.Write(buf)
				e.pos += int64(n)
				written += int64(n)
				buf = buf[n:]
				if err != nil {
					return err
				}
			}
			return nil
		})
		return
	}
	if rf, ok := w.(io.ReaderFrom); ok {
		written, err = rf.ReadFrom(e)
		return
	}
	buf := getBuf()
	defer putBuf(buf)
	written, err = io.CopyBuffer(w, e, buf)
	return
}

// NoData is a basic Src used when a File doesn't have data backing it.
// All file-ish functions will return the specified error instead.
type NoData struct{ Err error }

func (n NoData) Seek(int64, int) (int64, error)    { return 0, n.Err }
func (n NoData) Read([]byte) (int, error)          { return 0, n.Err }
func (n NoData) ReadAt([]byte, int64) (int, error) { return 0, n.Err }
func (n NoData) WriteTo(io.Writer) (int64, error)  { return 0, n.Err }

// Reader implements Src methods for the *File structs returned from this package.
// As a bonus, it also implements io.WriterTo.
type Reader struct {
	ex       GetExtents
	pos, siz int64
}

// ReadAt implements io.ReaderAt for a Reader.  It is responsible for handling the case
// where a single ReadAt call may cross multiple Extents.
func (e *Reader) ReadAt(buf []byte, offset int64) (readsize int, err error) {
	extents := e.ex(offset, int64(len(buf)))
	defer releaseExtents(extents)
	var n int
	for _, ex := range extents {
		n, err = ex.ReadAt(buf[readsize:], 0)
		readsize += n
		if err == io.EOF {
			err = nil
		}
	}
	if readsize < len(buf) && err == nil {
		err = io.EOF
	}
	return
}

// Seek implements io.Seeker for a Reader
func (e *Reader) Seek(offset int64, whence int) (int64, error) {
	switch whence {
	default:
		return 0, fs.ErrInvalid
	case io.SeekStart:
		e.pos = offset
	case io.SeekCurrent:
		e.pos += offset
	case io.SeekEnd:
		e.pos = e.siz - offset
	}
	return e.pos, nil
}

// Read implements io.Reader for a Reader.
func (e *Reader) Read(buf []byte) (int, error) {
	if e.pos < 0 || e.pos >= e.siz {
		return 0, io.EOF
	}
	rd := len(buf)
	if overflow := e.siz - (e.pos + int64(rd)); overflow < 0 {
		rd += int(overflow)
	}
	res, err := e.ReadAt(buf[:rd], e.pos)
	e.pos += int64(res)
	return res, err
}

// WriteTo implements io.WriterTo for a Reader.
func (e *Reader) WriteTo(w io.Writer) (written int64, err error) {
	seeker, canSeek := w.(io.Seeker)
	rdr := &extentReader{}
	var n int64
	extents := e.ex(e.pos, e.siz-e.pos)
	defer releaseExtents(extents)
	for _, ex := range extents {
		rdr.extent = ex
		rdr.pos = 0
		if rdr.src == Zeroes && canSeek {
			var pos, newPos int64
			pos, err = seeker.Seek(0, io.SeekCurrent)
			if err != nil {
				canSeek = false
			} else {
				newPos, err = seeker.Seek(rdr.size, io.SeekCurrent)
				seeked := newPos - pos
				e.pos += seeked
				written += seeked
				if seeked == rdr.size {
					continue
				}
				canSeek = false
				rdr.pos += newPos - pos
			}
		}
		n, err = rdr.writeTo(w)
		written += n
		e.pos += n
		if err != nil {
			break
		}
	}
	return
}
