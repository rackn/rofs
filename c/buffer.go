package c

import (
	"io"
	"io/fs"
	"syscall"
)

// SimpleBuffer is a very basic wrapper around a mostly static byte buffer
type SimpleBuffer []byte

func (s SimpleBuffer) closed() error {
	if s == nil {
		return fs.ErrClosed
	}
	return nil
}

// ReaAt implements io.ReaderAt
func (s SimpleBuffer) ReadAt(buf []byte, offset int64) (n int, err error) {
	if err = s.closed(); err != nil {
		return
	}
	err = io.EOF
	if offset < 0 || offset >= int64(len(s)) {
		return
	}
	n = copy(buf, s[int(offset):])
	if n == len(buf) {
		err = nil
	}
	return
}

// BytesAt returns a slice covering the requested byte range in the buffer.
func (s SimpleBuffer) BytesAt(offset int64, size uint) (buf []byte, err error) {
	if err = s.closed(); err != nil {
		return
	}
	err = io.EOF
	if offset < 0 || offset >= int64(len(s)) {
		return
	}
	size += uint(offset)
	if size > uint(len(s)) {
		return
	}
	err = nil
	buf = s[int(offset):int(size)]
	return
}

// WithBytesAt call f with a slice covering the requested byte range.
func (s SimpleBuffer) WithBytesAt(offset int64, size uint, f func([]byte) error) error {
	buf, err := s.BytesAt(offset, size)
	if err == nil {
		err = f(buf)
	}
	return err
}

// Size returns the size of the buffer.
func (s SimpleBuffer) Size() int64 {
	return int64(len(s))
}

// Clear zeros out the buffer.
func (s SimpleBuffer) Clear() {
	for i := range s {
		s[i] = 0
	}
}

var _ ByteFetcher = SimpleBuffer{}

// Buffer is an exact sized buffer.  Unlike a bytes.Buffer, it never
// grows beyond the capacity of the slice.  it is used where we know there
// is a fixed upper bound to the amount of data we are going to have it hold.
type Buffer struct {
	SimpleBuffer
	pos int
}

// NewBuffer creates a new Buffer.  The total size is fixed
// to the size of the byte slice that is passed in.
func NewBuffer(b []byte) *Buffer { return &Buffer{b, 0} }

// Read implements io.Reader
func (b *Buffer) Read(buf []byte) (int, error) {
	if err := b.closed(); err != nil {
		return 0, err
	}
	if b.pos == len(b.SimpleBuffer) {
		return 0, io.EOF
	}
	n := copy(buf, b.SimpleBuffer[b.pos:])
	b.pos += n
	return n, nil
}

// ReadByte returns the next byte in the Buffer.
func (b *Buffer) ReadByte() (byte, error) {
	if err := b.closed(); err != nil {
		return 0, err
	}
	if b.pos == len(b.SimpleBuffer) {
		return 0, io.EOF
	}
	res := b.SimpleBuffer[b.pos]
	b.pos++
	return res, nil
}

// WriteTo implements io.WriterTo.
func (b *Buffer) WriteTo(w io.Writer) (int64, error) {
	if err := b.closed(); err != nil {
		return 0, err
	}
	n, err := w.Write(b.SimpleBuffer[b.pos:])
	b.pos += n
	return int64(n), err
}

// Len returns the length of the unread part of the Buffer.
func (b *Buffer) Len() int {
	return len(b.SimpleBuffer[b.pos:])
}

// Seek changes the read and write offset in the Buffer.  It implements io.Seeker
func (b *Buffer) Seek(offset int64, whence int) (n int64, err error) {
	if err = b.closed(); err != nil {
		return
	}
	switch whence {
	default:
		err = fs.ErrInvalid
		return
	case io.SeekStart:
		n = offset
	case io.SeekCurrent:
		n = int64(b.pos) + offset
	case io.SeekEnd:
		n = int64(len(b.SimpleBuffer)) + offset
	}
	if n < 0 || n > int64(len(b.SimpleBuffer)) {
		n = 0
		err = fs.ErrInvalid
		return
	}
	b.pos = int(n)
	return
}

type WriteBuffer struct {
	Buffer
}

// Write implements io.Writer
func (w *WriteBuffer) Write(buf []byte) (int, error) {
	if err := w.closed(); err != nil {
		return 0, err
	}
	if w.pos == len(w.SimpleBuffer) {
		return 0, syscall.ENOSPC
	}
	n := copy(w.SimpleBuffer[w.pos:], buf)
	w.pos += n
	if len(buf[n:]) == 0 {
		return n, nil
	}
	return n, io.ErrShortWrite
}

// Bytes returns the written bytes in the buffer.
func (w *WriteBuffer) Bytes() []byte {
	return w.SimpleBuffer[:w.pos]
}

// NewBuffer creates a new Buffer.  The total size is fixed
// to the size of the byte slice that is passed in.
func NewWriteBuffer(b []byte) *WriteBuffer {
	return &WriteBuffer{Buffer{b, 0}}
}
