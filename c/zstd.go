package c

import "github.com/klauspost/compress/zstd"

// Zstd is our preallocated zstd decoder.  It is intended to be used for zstd.DecodeAll operations.
var Zstd *zstd.Decoder

func init() {
	Zstd, _ = zstd.NewReader(nil,
		zstd.WithDecoderLowmem(true),
		zstd.WithDecoderMaxWindow(128<<20),
		zstd.WithDecoderConcurrency(0))
}
