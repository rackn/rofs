// Package filesystem provides interfaces and constants required for filesystem implementations.
// All interesting implementations are in subpackages, e.g. gitlab.com/rackn/rofs/iso
package rofs

import (
	"gitlab.com/rackn/rofs/c"
	"gitlab.com/rackn/rofs/iso"
	"gitlab.com/rackn/rofs/squash"
	"gitlab.com/rackn/rofs/tar"
	"io/fs"
)

// Open attempts to open file as a read-only filesystem.
// It will attempt to try reading file as an ISO first,
// a tarfile second, and a squashfs file third.  If no
// suitable filesystems are found, the final error is returned.
func Open(file c.Src) (res fs.FS, err error) {
	defer func() {
		if err != nil {
			res = nil
		}
	}()
	if res, err = iso.Open(file); err == nil {
		return
	}
	if res, err = tar.Open(file); err == nil {
		return
	}
	res, err = squash.Open(file)
	return
}
