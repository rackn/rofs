package main

import (
	"gitlab.com/rackn/rofs/c"
	"io"
	"io/fs"
	"log"
	"os"
	"path"
	"path/filepath"

	rofs "gitlab.com/rackn/rofs"
)

func main() {
	if len(os.Args) != 3 {
		log.Fatalf("Need ISO filename and location to extract to")
	}
	fi, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatalf("Error opening %s: %v", os.Args[1], err)
	}
	defer fi.Close()
	fsys, err := rofs.Open(fi)
	if err != nil {
		log.Fatalf("Error reading %s: %v", os.Args[1], err)
	}
	os.RemoveAll(os.Args[2])
	if err := os.MkdirAll(os.Args[2], 0755); err != nil {
		log.Fatalf("Error creating %s: %v", os.Args[2], err)
	}
	err = fs.WalkDir(fsys, ".", func(p string, ent fs.DirEntry, e2 error) error {
		if e2 != nil {
			return e2
		}
		finalPath := path.Join(os.Args[2], p)
		log.Printf("Extracting %s", finalPath)
		mode, _ := ent.Info()
		if ent.IsDir() {
			return os.MkdirAll(finalPath, mode.Mode().Perm()|0600)
		}
		if mode.Mode().Type()&fs.ModeSymlink > 0 {
			ln, err := fsys.(c.Filesystem).ReadLink(p)
			if err != nil {
				log.Printf("Error reading link %s: %v", p, err)
				return err
			}
			pwd := path.Dir(finalPath)
			if ln[0] == '/' {
				ln, err = filepath.Rel(pwd, path.Join(os.Args[2], ln[1:]))
			} else {
				ln, err = filepath.Rel(pwd, path.Join(pwd, ln))
			}
			if err != nil {
				log.Printf("Link %s cannot be fixed up: %v", ln, err)
				return err
			}
			return os.Symlink(ln, finalPath)
		}
		if !mode.Mode().IsRegular() {
			log.Printf("Ignoring non-standard path %s with mode %s", p, mode.Mode())
			return nil
		}
		src, err := fsys.Open(p)
		if err != nil {
			log.Printf("Error opening source %s: %v", p, err)
			return err
		}
		defer src.Close()
		tgt, err := os.OpenFile(finalPath, os.O_CREATE|os.O_TRUNC|os.O_RDWR, mode.Mode()|0600)
		if err != nil {
			log.Printf("Error creating %s: %v", finalPath, err)
			return err
		}
		defer tgt.Close()

		if _, err = io.Copy(tgt, src); err != nil {
			log.Printf("Error copying %s: %v", p, err)
			return err
		}
		return nil
	})
	if err != nil {
		log.Fatalf("error %v", err)
	}

}
