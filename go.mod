module gitlab.com/rackn/rofs

go 1.22.0

toolchain go1.23.5

require (
	github.com/klauspost/compress v1.17.11
	github.com/pierrec/lz4/v4 v4.1.22
	github.com/pkg/xattr v0.4.10
	github.com/ulikunitz/xz v0.5.12
	github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8
	gitlab.com/rackn/seekable-zstd v0.8.3
	gitlab.com/rackn/simplecache v0.0.0-20230324193231-44368de53d93
	golang.org/x/exp v0.0.0-20250128182459-e0ece0dbea4c
)

require (
	github.com/cespare/xxhash/v2 v2.3.0 // indirect
	golang.org/x/sys v0.29.0 // indirect
)
