package iso

import (
	"encoding/binary"
	"fmt"
	"os"
	"path"
	"time"
)

const (
	rockRidgeSignaturePosixAttributes    = "PX"
	rockRidgeSignaturePosixDeviceNumber  = "PN"
	rockRidgeSignatureSymbolicLink       = "SL"
	rockRidgeSignatureName               = "NM"
	rockRidgeSignatureChild              = "CL"
	rockRidgeSignatureParent             = "PL"
	rockRidgeSignatureRelocatedDirectory = "RE"
	rockRidgeSignatureTimestamps         = "TF"
	rockRidgeSignatureSparseFile         = "SF"
	rockRidge110                         = "RRIP_1991A"
	rockRidge112                         = "IEEE_P1282"
	px110                                = 36
	px112                                = 44
	sf110                                = 12
	sf112                                = 21
)

// rockRidgeExtension implements suspExtension interface
type rockRidgeExtension struct {
	version    string
	id         string
	descriptor string
	source     string
}

func (r rockRidgeExtension) ID() string {
	return r.id
}

func (r rockRidgeExtension) Version() uint8 {
	return 1
}

func (r rockRidgeExtension) Process(signature string, b []byte) (directoryEntrySystemUseExtension, error) {
	// if we have a parser, use it, else use the raw parser
	var (
		entry directoryEntrySystemUseExtension
		err   error
	)
	switch signature {
	case rockRidgeSignaturePosixAttributes:
		entry, err = r.parsePosixAttributes(b)
	case rockRidgeSignaturePosixDeviceNumber:
		entry, err = r.parsePosixDeviceNumber(b)
	case rockRidgeSignatureSymbolicLink:
		entry, err = r.parseSymlink(b)
	case rockRidgeSignatureName:
		entry, err = r.parseName(b)
	case rockRidgeSignatureChild:
		entry, err = r.parseChildDirectory(b)
	case rockRidgeSignatureParent:
		entry, err = r.parseParentDirectory(b)
	case rockRidgeSignatureRelocatedDirectory:
		entry, err = r.parseRelocatedDirectory(b)
	case rockRidgeSignatureTimestamps:
		entry, err = r.parseTimestamps(b)
	case rockRidgeSignatureSparseFile:
		entry, err = r.parseSparseFile(b)
	default:
		return nil, ErrSuspNoHandler
	}
	if err != nil {
		return nil, fmt.Errorf("Error parsing %s extension by Rock Ridge : %v", signature, err)
	}
	return entry, nil
}

// get the rock ridge fname for a directory entry
func (r rockRidgeExtension) GetFilename(de *inode) (string, error) {
	found := false
	name := ""
	for _, e := range de.extensions {
		if nm, ok := e.(rockRidgeName); ok {
			found = true
			name = nm.name
			break
		}
	}
	if !found {
		return "", fmt.Errorf("Could not find Rock Ridge fname property")
	}
	return name, nil
}

// determine if a directory entry was relocated
func (r rockRidgeExtension) Relocated(de *inode) bool {
	relocated := false
	for _, e := range de.extensions {
		if _, ok := e.(rockRidgeRelocatedDirectory); ok {
			relocated = true
			break
		}
	}
	return relocated
}

// find the directory location
func (r rockRidgeExtension) GetDirectoryLocation(de *inode) int64 {
	for _, e := range de.extensions {
		if child, ok := e.(rockRidgeChildDirectory); ok {
			return int64(child.location) * de.fs.blocksize
		}
	}
	return 0
}

func getRockRidgeExtension(id string) *rockRidgeExtension {
	var ret *rockRidgeExtension // defaults to nil
	switch id {
	case rockRidge110:
		ret = &rockRidgeExtension{
			id:         id,
			version:    "1.10",
			descriptor: "THE ROCK RIDGE INTERCHANGE PROTOCOL PROVIDES SUPPORT FOR POSIX FILE SYSTEM SEMANTICS",
			source:     "PLEASE CONTACT DISC PUBLISHER FOR SPECIFICATION SOURCE. SEE PUBLISHER IDENTIFIER IN PRIMARY VOLUME DESCRIPTOR FOR CONTACT INFORMATION.",
		}
	case rockRidge112:
		ret = &rockRidgeExtension{
			id:         id,
			version:    "1.12",
			descriptor: "THE IEEE P1282 PROTOCOL PROVIDES SUPPORT FOR POSIX FILE SYSTEM SEMANTICS.",
			source:     "PLEASE CONTACT THE IEEE STANDARDS DEPARTMENT, PISCATAWAY, NJ, USA FOR THE P1282 SPECIFICATION.",
		}
	}
	return ret
}

// rockRidgePosixAttributes
type rockRidgePosixAttributes struct {
	mode         os.FileMode
	saveSwapText bool
	length       int

	linkCount uint32
	uid       uint32
	gid       uint32
	serial    uint32
}

func (d rockRidgePosixAttributes) Signature() string {
	return rockRidgeSignaturePosixAttributes
}
func (d rockRidgePosixAttributes) Length() int {
	return d.length
}
func (d rockRidgePosixAttributes) Version() uint8 {
	return 1
}
func (d rockRidgePosixAttributes) Data() []byte {
	ret := make([]byte, d.length-4)
	modes := uint32(0)
	regular := true
	m := d.mode
	// get Unix permission bits - golang and Rock Ridge use the same ones
	modes = modes | uint32(m&0777)
	// get setuid and setgid
	modes = modes | uint32(m&os.ModeSetuid)
	modes = modes | uint32(m&os.ModeSetgid)
	// save swapped text mode seems to have no parallel
	if d.saveSwapText {
		modes = modes | 01000
	}
	// the rest of the modes do not use the same bits on Rock Ridge and on golang
	if m&os.ModeSocket == os.ModeSocket {
		modes = modes | 0140000
		regular = false
	}
	if m&os.ModeSymlink == os.ModeSymlink {
		modes = modes | 0120000
		regular = false
	}
	if m&os.ModeDevice == os.ModeDevice {
		regular = false
		if m&os.ModeCharDevice == os.ModeCharDevice {
			modes = modes | 020000
		} else {
			modes = modes | 060000
		}
	}
	if m&os.ModeDir == os.ModeDir {
		modes = modes | 040000
		regular = false
	}
	if m&os.ModeNamedPipe == os.ModeNamedPipe {
		modes = modes | 010000
		regular = false
	}
	if regular {
		modes = modes | 0100000
	}

	binary.LittleEndian.PutUint32(ret[0:4], modes)
	binary.BigEndian.PutUint32(ret[4:8], modes)
	binary.LittleEndian.PutUint32(ret[8:12], d.linkCount)
	binary.BigEndian.PutUint32(ret[12:16], d.linkCount)
	binary.LittleEndian.PutUint32(ret[16:20], d.uid)
	binary.BigEndian.PutUint32(ret[20:24], d.uid)
	binary.LittleEndian.PutUint32(ret[24:28], d.gid)
	binary.BigEndian.PutUint32(ret[28:32], d.gid)
	if d.length == 44 {
		binary.LittleEndian.PutUint32(ret[32:36], d.serial)
		binary.BigEndian.PutUint32(ret[36:40], d.serial)
	}
	return ret
}

func (d rockRidgePosixAttributes) Continuable() bool {
	return false
}
func (d rockRidgePosixAttributes) Merge([]directoryEntrySystemUseExtension) directoryEntrySystemUseExtension {
	return nil
}

func (r rockRidgeExtension) parsePosixAttributes(b []byte) (directoryEntrySystemUseExtension, error) {
	if !(len(b) == px110 || len(b) == px112) {
		return nil, fmt.Errorf("Rock Ridge PX extension must be %d or %d bytes, but received %d", px110, px112, len(b))
	}
	size := b[2]
	if size != uint8(len(b)) {
		return nil, fmt.Errorf("Rock Ridge PX extension must be %d bytes, but byte 2 indicated %d", len(b), size)
	}
	version := b[3]
	if version != 1 {
		return nil, fmt.Errorf("Rock Ridge PX extension must be version 1, was %d", version)
	}
	// file mode
	modes := binary.LittleEndian.Uint32(b[4:8])
	var m uint32
	// get Unix permission bits - golang and Rock Ridge use the same ones
	m = m | (modes & 0777)
	// get setuid and setgid
	m = m | (modes & uint32(os.ModeSetuid))
	m = m | (modes & uint32(os.ModeSetgid))
	// save swapped text mode seems to have no parallel
	var saveSwapText bool
	if modes&01000 != 0 {
		saveSwapText = true
	}
	// the rest of the modes do not use the same bits on Rock Ridge and on golang, and are exclusive
	switch {
	case modes&0140000 == 0140000:
		m = m | uint32(os.ModeSocket)
	case modes&0120000 == 0120000:
		m = m | uint32(os.ModeSymlink)
	case modes&020000 == 020000:
		m = m | uint32(os.ModeCharDevice|os.ModeDevice)
	case modes&060000 == 060000:
		m = m | uint32(os.ModeDevice)
	case modes&040000 == 040000:
		m = m | uint32(os.ModeDir)
	case modes&010000 == 010000:
		m = m | uint32(os.ModeNamedPipe)
	}

	var serial uint32
	if len(b) == px112 {
		serial = binary.LittleEndian.Uint32(b[36:40])
	}
	return rockRidgePosixAttributes{
		mode:         os.FileMode(m),
		saveSwapText: saveSwapText,
		linkCount:    binary.LittleEndian.Uint32(b[12:16]),
		uid:          binary.LittleEndian.Uint32(b[20:24]),
		gid:          binary.LittleEndian.Uint32(b[28:32]),
		serial:       serial,
		length:       len(b),
	}, nil
}

// rockRidgePosixDeviceNumber
type rockRidgePosixDeviceNumber struct {
	high uint32
	low  uint32
}

func (d rockRidgePosixDeviceNumber) Signature() string {
	return rockRidgeSignaturePosixDeviceNumber
}
func (d rockRidgePosixDeviceNumber) Length() int {
	return 20
}
func (d rockRidgePosixDeviceNumber) Version() uint8 {
	return 1
}
func (d rockRidgePosixDeviceNumber) Data() []byte {
	ret := make([]byte, 16)

	binary.LittleEndian.PutUint32(ret[0:4], d.high)
	binary.BigEndian.PutUint32(ret[4:8], d.high)
	binary.LittleEndian.PutUint32(ret[8:12], d.low)
	binary.BigEndian.PutUint32(ret[12:16], d.low)
	return ret
}

func (d rockRidgePosixDeviceNumber) Continuable() bool {
	return false
}
func (d rockRidgePosixDeviceNumber) Merge([]directoryEntrySystemUseExtension) directoryEntrySystemUseExtension {
	return nil
}

func (r rockRidgeExtension) parsePosixDeviceNumber(b []byte) (directoryEntrySystemUseExtension, error) {
	targetSize := 20
	if len(b) != targetSize {
		return nil, fmt.Errorf("Rock Ridge PN extension must be %d bytes, but received %d", targetSize, len(b))
	}
	size := b[2]
	if size != uint8(targetSize) {
		return nil, fmt.Errorf("Rock Ridge PN extension must be %d bytes, but byte 2 indicated %d", targetSize, size)
	}
	version := b[3]
	if version != 1 {
		return nil, fmt.Errorf("Rock Ridge PN extension must be version 1, was %d", version)
	}
	return rockRidgePosixDeviceNumber{
		high: binary.LittleEndian.Uint32(b[4:8]),
		low:  binary.LittleEndian.Uint32(b[12:16]),
	}, nil
}

// rockRidgeSymlink
// a symlink can be greater than the 254 max size of a SUSP extension, so it may continue across multiple extension entries
// a rockRidgeSymlink can represent the individual components, or an entire set merged together
// Bytes(), when called, will provide as many consecutive symlink bytes as needed
type rockRidgeSymlink struct {
	name      string
	continued bool
}

func (d rockRidgeSymlink) Signature() string {
	return rockRidgeSignatureSymbolicLink
}
func (d rockRidgeSymlink) Length() int {
	// basic 4 bytes for all SUSP entries, 1 byte for flags, 1 for Component Flags, 1 for Component Len, name
	return 4 + 1 + 2 + len(d.name)
}
func (d rockRidgeSymlink) Version() uint8 {
	return 1
}
func (d rockRidgeSymlink) Data() []byte {
	return []byte{}
}

func (d rockRidgeSymlink) Continuable() bool {
	return d.continued
}
func (d rockRidgeSymlink) Merge(links []directoryEntrySystemUseExtension) directoryEntrySystemUseExtension {
	for _, e := range links {
		if l, ok := e.(rockRidgeSymlink); ok {
			d.name = d.name + l.name
		}
	}
	d.continued = false
	return d
}

func (r rockRidgeExtension) parseSymlink(buf []byte) (directoryEntrySystemUseExtension, error) {
	if int(buf[2]) != len(buf) {
		return nil, fmt.Errorf("Rock Ridge SL extension received %d bytes, but byte 2 indicated %d", len(buf), buf[2])
	}
	if buf[3] != 1 {
		return nil, fmt.Errorf("Rock Ridge SL extension must be version 1, was %d", buf[3])
	}
	continued := buf[4] == 1
	var name []string
	namePart := []byte{}
	toProcess := buf[5:]
	for len(toProcess) > 0 {
		flags, size := toProcess[0], toProcess[1]
		toProcess = toProcess[2:]
		join := true
		switch {
		case flags == 0:
			namePart = append(namePart, toProcess[:size]...)
			toProcess = toProcess[size:]
		case flags&0x1 == 0x1:
			join = false
		case flags&0x4 == 0x4:
			namePart = append(namePart, byte('.'))
			fallthrough
		case flags&0x2 == 0x2:
			namePart = append(namePart, byte('.'))
		case flags&0x8 == 0x8:
			name = append(name, "/")
			namePart = namePart[:0]
			join = false
		}
		if join {
			name = append(name, string(namePart))
			namePart = namePart[:0]
		}
	}
	return rockRidgeSymlink{
		continued: continued,
		name:      path.Join(name...),
	}, nil
}

// rockRidgeName
// a name can be greater than the 254 max size of a SUSP extension, so it may continue across multiple extension entries
// a rockRidgeName can represent the individual components, or an entire set merged together
// Bytes(), when called, will provide as many consecutive name bytes as needed
type rockRidgeName struct {
	name      string
	continued bool
	current   bool
	parent    bool
}

func (d rockRidgeName) Signature() string {
	return rockRidgeSignatureName
}
func (d rockRidgeName) Length() int {
	// basic 4 bytes for all SUSP entries, 1 byte for flags, name
	return 4 + 1 + len(d.name)
}
func (d rockRidgeName) Version() uint8 {
	return 1
}
func (d rockRidgeName) Data() []byte {
	return []byte{}
}

func (d rockRidgeName) Continuable() bool {
	return d.continued
}
func (d rockRidgeName) Merge(names []directoryEntrySystemUseExtension) directoryEntrySystemUseExtension {
	for _, e := range names {
		if n, ok := e.(rockRidgeName); ok {
			d.name = d.name + n.name
		}
	}
	d.continued = false
	return d
}

func (r rockRidgeExtension) parseName(b []byte) (directoryEntrySystemUseExtension, error) {
	size := int(b[2])
	if size != len(b) {
		return nil, fmt.Errorf("Rock Ridge NM extension received %d bytes, but byte 2 indicated %d", len(b), size)
	}
	version := b[3]
	if version != 1 {
		return nil, fmt.Errorf("Rock Ridge NM extension must be version 1, was %d", version)
	}
	continued := b[4]&1 != 0
	current := b[4]&2 != 0
	parent := b[4]&4 != 0
	name := ""
	if size > 5 {
		name = string(b[5:])
	}
	return rockRidgeName{
		continued: continued,
		name:      name,
		current:   current,
		parent:    parent,
	}, nil
}

// rockRidgeTimestamp constants - these are the bitmasks for the flag field
const (
	rockRidgeTimestampCreation   uint8 = 1
	rockRidgeTimestampModify     uint8 = 2
	rockRidgeTimestampAccess     uint8 = 4
	rockRidgeTimestampAttribute  uint8 = 8
	rockRidgeTimestampBackup     uint8 = 16
	rockRidgeTimestampExpiration uint8 = 32
	rockRidgeTimestampEffective  uint8 = 64
	rockRidgeTimestampLongForm   uint8 = 128
)

// rockRidgeTimestamp
type rockRidgeTimestamp struct {
	time          time.Time
	timestampType uint8
}

type rockRidgeTimestamps struct {
	stamps   []rockRidgeTimestamp
	longForm bool
}
type rockRidgeTimestampByBitOrder []rockRidgeTimestamp

func (s rockRidgeTimestampByBitOrder) Len() int {
	return len(s)
}
func (s rockRidgeTimestampByBitOrder) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s rockRidgeTimestampByBitOrder) Less(i, j int) bool {
	return s[i].timestampType < s[j].timestampType
}

func (d rockRidgeTimestamps) Signature() string {
	return rockRidgeSignatureTimestamps
}
func (d rockRidgeTimestamps) Length() int {
	entryLength := 7
	if d.longForm {
		entryLength = 17
	}
	return 5 + entryLength*len(d.stamps)
}
func (d rockRidgeTimestamps) Version() uint8 {
	return 1
}
func (d rockRidgeTimestamps) Data() []byte {
	return []byte{}
}

func (d rockRidgeTimestamps) Continuable() bool {
	return false
}
func (d rockRidgeTimestamps) Merge([]directoryEntrySystemUseExtension) directoryEntrySystemUseExtension {
	return nil
}

func (r rockRidgeExtension) parseTimestamps(b []byte) (directoryEntrySystemUseExtension, error) {
	size := b[2]
	if int(size) != len(b) {
		return nil, fmt.Errorf("Rock Ridge TF extension has %d bytes, but byte 2 indicated %d", len(b), size)
	}
	version := b[3]
	if version != 1 {
		return nil, fmt.Errorf("Rock Ridge TF extension must be version 1, was %d", version)
	}
	// what timestamps are recorded?
	flags := b[4]
	// go through them one by one in order
	entryLength := 7
	longForm := false
	if flags&rockRidgeTimestampLongForm != 0 {
		entryLength = 17
		longForm = true
	}

	entries := make([]rockRidgeTimestamp, 0)
	tfBytes := b[5:]
	tfTypes := []uint8{rockRidgeTimestampCreation, rockRidgeTimestampModify, rockRidgeTimestampAccess, rockRidgeTimestampAttribute,
		rockRidgeTimestampBackup, rockRidgeTimestampExpiration, rockRidgeTimestampEffective}
	for _, tf := range tfTypes {
		if flags&tf != 0 {
			timeBytes := tfBytes[:entryLength]
			tfBytes = tfBytes[entryLength:]
			var (
				t   time.Time
				err error
			)
			if longForm {
				t, err = decBytesToTime(timeBytes)
				if err != nil {
					return nil, fmt.Errorf("Could not process timestamp %d bytes to long form bytes: %v % x", tf, err, timeBytes)
				}
			} else {
				t = bytesToTime(timeBytes)
			}
			entry := rockRidgeTimestamp{
				time:          t,
				timestampType: tf,
			}
			entries = append(entries, entry)
		}
	}

	return rockRidgeTimestamps{
		stamps:   entries,
		longForm: longForm,
	}, nil
}

// rockRidgeSparseFile
type rockRidgeSparseFile struct {
	length     int
	high       uint32
	low        uint32
	tableDepth uint8
}

func (d rockRidgeSparseFile) Signature() string {
	return rockRidgeSignatureSparseFile
}
func (d rockRidgeSparseFile) Length() int {
	return d.length
}
func (d rockRidgeSparseFile) Version() uint8 {
	return 1
}
func (d rockRidgeSparseFile) Data() []byte {
	return []byte{}
}

func (d rockRidgeSparseFile) Continuable() bool {
	return false
}
func (d rockRidgeSparseFile) Merge([]directoryEntrySystemUseExtension) directoryEntrySystemUseExtension {
	return nil
}

func (r rockRidgeExtension) parseSparseFile(b []byte) (directoryEntrySystemUseExtension, error) {
	if !(len(b) == sf110 || len(b) == sf112) {
		return nil, fmt.Errorf("Rock Ridge SF extension must be %d or %d bytes, but received %d", sf110, sf112, len(b))
	}
	size := b[2]
	if size != uint8(len(b)) {
		return nil, fmt.Errorf("Rock Ridge SF extension must be %d bytes, but byte 2 indicated %d", len(b), size)
	}
	version := b[3]
	if version != 1 {
		return nil, fmt.Errorf("Rock Ridge SF extension must be version 1, was %d", version)
	}
	sf := &rockRidgeSparseFile{
		high:   binary.LittleEndian.Uint32(b[4:8]),
		length: len(b),
	}
	if len(b) == sf112 {
		sf.low = binary.LittleEndian.Uint32(b[12:16])
		sf.tableDepth = b[20]
	}
	return sf, nil
}

// rockRidgeChildDirectory
type rockRidgeChildDirectory struct {
	location uint32
}

func (d rockRidgeChildDirectory) Signature() string {
	return rockRidgeSignatureChild
}
func (d rockRidgeChildDirectory) Length() int {
	return 12
}
func (d rockRidgeChildDirectory) Version() uint8 {
	return 1
}
func (d rockRidgeChildDirectory) Data() []byte {
	return []byte{}
}

func (d rockRidgeChildDirectory) Continuable() bool {
	return false
}
func (d rockRidgeChildDirectory) Merge([]directoryEntrySystemUseExtension) directoryEntrySystemUseExtension {
	return nil
}

func (r rockRidgeExtension) parseChildDirectory(b []byte) (directoryEntrySystemUseExtension, error) {
	targetSize := 12
	if len(b) != targetSize {
		return nil, fmt.Errorf("Rock Ridge CL extension must be %d bytes, but received %d", targetSize, len(b))
	}
	size := b[2]
	if size != uint8(targetSize) {
		return nil, fmt.Errorf("Rock Ridge CL extension must be %d bytes, but byte 2 indicated %d", targetSize, size)
	}
	version := b[3]
	if version != 1 {
		return nil, fmt.Errorf("Rock Ridge CL extension must be version 1, was %d", version)
	}
	return rockRidgeChildDirectory{
		location: binary.LittleEndian.Uint32(b[4:8]),
	}, nil
}

// rockRidgeParentDirectory
type rockRidgeParentDirectory struct {
	location uint32
}

func (d rockRidgeParentDirectory) Signature() string {
	return rockRidgeSignatureParent
}
func (d rockRidgeParentDirectory) Length() int {
	return 12
}
func (d rockRidgeParentDirectory) Version() uint8 {
	return 1
}
func (d rockRidgeParentDirectory) Data() []byte {
	return []byte{}
}

func (d rockRidgeParentDirectory) Continuable() bool {
	return false
}
func (d rockRidgeParentDirectory) Merge([]directoryEntrySystemUseExtension) directoryEntrySystemUseExtension {
	return nil
}

func (r rockRidgeExtension) parseParentDirectory(b []byte) (directoryEntrySystemUseExtension, error) {
	targetSize := 12
	if len(b) != targetSize {
		return nil, fmt.Errorf("Rock Ridge PL extension must be %d bytes, but received %d", targetSize, len(b))
	}
	size := b[2]
	if size != uint8(targetSize) {
		return nil, fmt.Errorf("Rock Ridge PL extension must be %d bytes, but byte 2 indicated %d", targetSize, size)
	}
	version := b[3]
	if version != 1 {
		return nil, fmt.Errorf("Rock Ridge PL extension must be version 1, was %d", version)
	}
	return rockRidgeParentDirectory{
		location: binary.LittleEndian.Uint32(b[4:8]),
	}, nil
}

// rockRidgeRelocatedDirectory
type rockRidgeRelocatedDirectory struct {
}

func (d rockRidgeRelocatedDirectory) Signature() string {
	return rockRidgeSignatureRelocatedDirectory
}
func (d rockRidgeRelocatedDirectory) Length() int {
	return 8
}
func (d rockRidgeRelocatedDirectory) Version() uint8 {
	return 1
}
func (d rockRidgeRelocatedDirectory) Data() []byte {
	return []byte{}
}

func (d rockRidgeRelocatedDirectory) Continuable() bool {
	return false
}
func (d rockRidgeRelocatedDirectory) Merge([]directoryEntrySystemUseExtension) directoryEntrySystemUseExtension {
	return nil
}

func (r rockRidgeExtension) parseRelocatedDirectory(b []byte) (directoryEntrySystemUseExtension, error) {
	targetSize := 4
	if len(b) != targetSize {
		return nil, fmt.Errorf("Rock Ridge RE extension must be %d bytes, but received %d", targetSize, len(b))
	}
	size := b[2]
	if size != uint8(targetSize) {
		return nil, fmt.Errorf("Rock Ridge RE extension must be %d bytes, but byte 2 indicated %d", targetSize, size)
	}
	version := b[3]
	if version != 1 {
		return nil, fmt.Errorf("Rock Ridge RE extension must be version 1, was %d", version)
	}
	return rockRidgeRelocatedDirectory{}, nil
}
