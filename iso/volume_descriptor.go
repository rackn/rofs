package iso

import (
	"encoding/binary"
	"fmt"
	"io"
	"time"
)

type volumeDescriptorType uint8

const (
	volumeDescriptorBoot          volumeDescriptorType = 0x00
	volumeDescriptorPrimary       volumeDescriptorType = 0x01
	volumeDescriptorSupplementary volumeDescriptorType = 0x02
	volumeDescriptorPartition     volumeDescriptorType = 0x03
	volumeDescriptorTerminator    volumeDescriptorType = 0xff
)

const (
	isoIdentifier        uint64 = 0x4344303031 // string "CD001"
	isoVersion           uint8  = 0x01
	bootSystemIdentifier        = "EL TORITO SPECIFICATION"
)

// volumeDescriptor interface for any given type of volume descriptor
type volumeDescriptor interface {
	Type() volumeDescriptorType
}

type primaryVolumeDescriptor struct {
	creation                   time.Time
	effective                  time.Time
	expiration                 time.Time
	modification               time.Time
	rootDirectoryEntry         *inode
	bibliographicFile          string
	preparerIdentifier         string
	abstractFile               string
	copyrightFile              string
	applicationIdentifier      string
	systemIdentifier           string
	volumeIdentifier           string
	volumeSetIdentifier        string
	publisherIdentifier        string
	pathTableMOptionalLocation uint32
	pathTableMLocation         uint32
	pathTableLOptionalLocation uint32
	pathTableLLocation         uint32
	pathTableSize              uint32
	volumeSize                 uint32
	blocksize                  uint16
	sequenceNumber             uint16
	setSize                    uint16
}

type bootVolumeDescriptor struct {
	location uint32 // length 1977 bytes; trailing 0x00 are stripped off
}
type terminatorVolumeDescriptor struct {
}
type supplementaryVolumeDescriptor struct {
	effective                  time.Time
	creation                   time.Time
	modification               time.Time
	expiration                 time.Time
	rootDirectoryEntry         *inode
	applicationIdentifier      string
	volumeIdentifier           string
	bibliographicFile          string
	abstractFile               string
	copyrightFile              string
	preparerIdentifier         string
	systemIdentifier           string
	volumeSetIdentifier        string
	publisherIdentifier        string
	escapeSequences            []byte
	volumeSize                 uint64
	pathTableMOptionalLocation uint32
	pathTableMLocation         uint32
	pathTableLOptionalLocation uint32
	pathTableLLocation         uint32
	pathTableSize              uint32
	blocksize                  uint16
	sequenceNumber             uint16
	setSize                    uint16
	volumeFlags                uint8
}
type partitionVolumeDescriptor struct {
	data []byte // length 2048 bytes; trailing 0x00 are stripped off
}

type volumeDescriptors struct {
	primary     *primaryVolumeDescriptor
	descriptors []volumeDescriptor
}

// primaryVolumeDescriptor
func (v *primaryVolumeDescriptor) Type() volumeDescriptorType {
	return volumeDescriptorPrimary
}

// volumeDescriptorFromBytes create a volumeDescriptor struct from bytes
func volumeDescriptorFromBytes(b []byte, src io.ReaderAt) (volumeDescriptor, error) {
	if len(b) != int(volumeDescriptorSize) {
		return nil, fmt.Errorf("Cannot read volume descriptor from bytes of length %d, must be %d", len(b), volumeDescriptorSize)
	}
	// validate the signature
	tmpb := make([]byte, 8, 8)
	copy(tmpb[3:8], b[1:6])
	signature := binary.BigEndian.Uint64(tmpb)
	if signature != isoIdentifier {
		return nil, fmt.Errorf("Mismatched ISO identifier in Volume Descriptor. Found %x expected %x", signature, isoIdentifier)
	}
	// validate the version
	version := b[6]
	if version != isoVersion {
		return nil, fmt.Errorf("Mismatched ISO version in Volume Descriptor. Found %x expected %x", version, isoVersion)
	}
	// get the type and data - later we will be more intelligent about this and read actual primary volume info
	vdType := volumeDescriptorType(b[0])
	var vd volumeDescriptor
	var err error

	switch vdType {
	case volumeDescriptorPrimary:
		vd, err = parsePrimaryVolumeDescriptor(b, src)
		if err != nil {
			return nil, fmt.Errorf("Unable to parse primary volume descriptor bytes: %v", err)
		}
	case volumeDescriptorBoot:
		vd, err = parseBootVolumeDescriptor(b)
		if err != nil {
			return nil, fmt.Errorf("Unable to parse primary volume descriptor bytes: %v", err)
		}
	case volumeDescriptorTerminator:
		vd = &terminatorVolumeDescriptor{}
	case volumeDescriptorPartition:
		vd = &partitionVolumeDescriptor{
			data: b[8:volumeDescriptorSize],
		}
	case volumeDescriptorSupplementary:
		vd, err = parseSupplementaryVolumeDescriptor(b, src)
		if err != nil {
			return nil, fmt.Errorf("Unable to parse primary volume descriptor bytes: %v", err)
		}
	default:
		return nil, fmt.Errorf("Unknown volume descriptor type %d", vdType)
	}
	return vd, nil
}

func parsePrimaryVolumeDescriptor(b []byte, src io.ReaderAt) (*primaryVolumeDescriptor, error) {
	blocksize := binary.LittleEndian.Uint16(b[128:130])

	creation, err := decBytesToTime(b[813 : 813+17])
	if err != nil {
		return nil, fmt.Errorf("Unable to convert ctime date/time from bytes: %v", err)
	}

	var expiration, effective, modification time.Time
	expirationBytes := b[847 : 847+17]
	effectiveBytes := b[864 : 864+17]
	modification, err = decBytesToTime(b[830 : 830+17])
	if err != nil {
		return nil, fmt.Errorf("Unable to convert modification date/time from bytes: %v", err)
	}
	expiration, err = decBytesToTime(expirationBytes)
	if err != nil {
		return nil, fmt.Errorf("Unable to convert expiration date/time from bytes: %v", err)
	}
	effective, err = decBytesToTime(effectiveBytes)
	if err != nil {
		return nil, fmt.Errorf("Unable to convert effective date/time from bytes: %v", err)
	}

	rootDirEntry, err := dirEntryFromBytes(b[156:156+34], nil, int64(blocksize), src)
	if err != nil {
		return nil, fmt.Errorf("Unable to read root directory entry: %v", err)
	}
	return &primaryVolumeDescriptor{
		systemIdentifier:           string(b[8:40]),
		volumeIdentifier:           string(b[40:72]),
		volumeSize:                 binary.LittleEndian.Uint32(b[80:84]),
		setSize:                    binary.LittleEndian.Uint16(b[120:122]),
		sequenceNumber:             binary.LittleEndian.Uint16(b[124:126]),
		blocksize:                  blocksize,
		pathTableSize:              binary.LittleEndian.Uint32(b[132:136]),
		pathTableLLocation:         binary.LittleEndian.Uint32(b[140:144]),
		pathTableLOptionalLocation: binary.LittleEndian.Uint32(b[144:148]),
		pathTableMLocation:         binary.BigEndian.Uint32(b[148:152]),
		pathTableMOptionalLocation: binary.BigEndian.Uint32(b[152:156]),
		volumeSetIdentifier:        string(b[190 : 190+128]),
		publisherIdentifier:        string(b[318 : 318+128]),
		preparerIdentifier:         string(b[446 : 446+128]),
		applicationIdentifier:      string(b[574 : 574+128]),
		copyrightFile:              string(b[702 : 702+37]),
		abstractFile:               string(b[739 : 739+37]),
		bibliographicFile:          string(b[776 : 776+37]),
		creation:                   creation,
		modification:               modification,
		expiration:                 expiration,
		effective:                  effective,
		rootDirectoryEntry:         rootDirEntry,
	}, nil
}

// terminatorVolumeDescriptor
func (v *terminatorVolumeDescriptor) Type() volumeDescriptorType {
	return volumeDescriptorTerminator
}

// bootVolumeDescriptor
func (v *bootVolumeDescriptor) Type() volumeDescriptorType {
	return volumeDescriptorBoot
}

// parseBootVolumeDescriptor
func parseBootVolumeDescriptor(b []byte) (*bootVolumeDescriptor, error) {
	systemIdentifier := string(b[0x7 : 0x7+len(bootSystemIdentifier)])
	if systemIdentifier != bootSystemIdentifier {
		return nil, fmt.Errorf("Incorrect specification, actual '%s' expected '%s'", systemIdentifier, bootSystemIdentifier)
	}
	location := binary.LittleEndian.Uint32(b[0x47:0x4b])
	return &bootVolumeDescriptor{location: location}, nil
}

// supplementaryVolumeDescriptor
func parseSupplementaryVolumeDescriptor(b []byte, src io.ReaderAt) (*supplementaryVolumeDescriptor, error) {
	blocksize := binary.LittleEndian.Uint16(b[128:130])
	volumesize := binary.LittleEndian.Uint32(b[80:84])
	volumesizeBytes := uint64(blocksize) * uint64(volumesize)

	creation, err := decBytesToTime(b[813 : 813+17])
	if err != nil {
		return nil, fmt.Errorf("Unable to convert ctime date/time from bytes: %v", err)
	}
	modification, err := decBytesToTime(b[830 : 830+17])
	if err != nil {
		return nil, fmt.Errorf("Unable to convert modification date/time from bytes: %v", err)
	}
	// expiration can be never
	var expiration, effective time.Time
	expirationBytes := b[847 : 847+17]
	effectiveBytes := b[864 : 864+17]
	expiration, err = decBytesToTime(expirationBytes)
	if err != nil {
		return nil, fmt.Errorf("Unable to convert expiration date/time from bytes: %v", err)
	}
	effective, err = decBytesToTime(effectiveBytes)
	if err != nil {
		return nil, fmt.Errorf("Unable to convert effective date/time from bytes: %v", err)
	}

	// no susp extensions for the dir entry in the volume descriptor
	rootDirEntry, err := dirEntryFromBytes(b[156:156+34], nil, int64(blocksize), src)
	if err != nil {
		return nil, fmt.Errorf("Unable to read root directory entry: %v", err)
	}

	return &supplementaryVolumeDescriptor{
		systemIdentifier:           string(b[8:40]),
		volumeIdentifier:           string(b[40:72]),
		volumeSize:                 volumesizeBytes,
		setSize:                    binary.LittleEndian.Uint16(b[120:122]),
		sequenceNumber:             binary.LittleEndian.Uint16(b[124:126]),
		blocksize:                  blocksize,
		pathTableSize:              binary.LittleEndian.Uint32(b[132:136]),
		pathTableLLocation:         binary.LittleEndian.Uint32(b[140:144]),
		pathTableLOptionalLocation: binary.LittleEndian.Uint32(b[144:148]),
		pathTableMLocation:         binary.BigEndian.Uint32(b[148:152]),
		pathTableMOptionalLocation: binary.BigEndian.Uint32(b[152:156]),
		volumeSetIdentifier:        bytesToUCS2String(b[190 : 190+128]),
		publisherIdentifier:        bytesToUCS2String(b[318 : 318+128]),
		preparerIdentifier:         bytesToUCS2String(b[446 : 446+128]),
		applicationIdentifier:      bytesToUCS2String(b[574 : 574+128]),
		copyrightFile:              bytesToUCS2String(b[702 : 702+37]),
		abstractFile:               bytesToUCS2String(b[739 : 739+37]),
		bibliographicFile:          bytesToUCS2String(b[776 : 776+37]),
		creation:                   creation,
		modification:               modification,
		expiration:                 expiration,
		effective:                  effective,
		rootDirectoryEntry:         rootDirEntry,
	}, nil
}
func (v *supplementaryVolumeDescriptor) Type() volumeDescriptorType {
	return volumeDescriptorSupplementary
}

// partitionVolumeDescriptor
func (v *partitionVolumeDescriptor) Type() volumeDescriptorType {
	return volumeDescriptorPartition
}
