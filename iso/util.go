package iso

import (
	"bytes"
	"fmt"
	"io"
	"strconv"
	"time"
)

const (
	// KB represents one KB
	KB = 1024
	// MB represents one MB
	MB = 1024 * KB
	// GB represents one GB
	GB = 1024 * MB
	// TB represents one TB
	TB = 1024 * GB
)

var nullBytes = []byte{48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 0}
var zeros = make([]byte, 2048)

func nullTime(t []byte) bool {
	return bytes.Equal(t[:len(nullBytes)], nullBytes) || bytes.Equal(t, zeros[:len(t)])
}

// bytesToUCS2String convert bytes to UCS-2. We aren't 100% sure that this is right,
// as it is possible to pass it an odd number of characters. But good enough for now.
func bytesToUCS2String(b []byte) string {
	r := make([]rune, 0, 30)
	// now we can iterate - be careful in case we were given an odd number of bytes
	for i := 0; i < len(b); {
		// little endian
		var val uint16
		if i >= len(b)-1 {
			val = uint16(b[i])
		} else {
			val = uint16(b[i])<<8 + uint16(b[i+1])
		}
		r = append(r, rune(val))
		i += 2
	}
	return string(r)
}

var isoNameSuffix = []byte(`.;1`)

// If the name was not overridden by an extension, clean it up.
// Matches Linux kernel semantics in fs/isofs/dir.c:isofs_name_translate
func cleanName(name string) string {
	nb := []byte(name)
	for i := range nb {
		if nb[i] >= byte('A') && nb[i] <= byte('Z') {
			nb[i] |= 0x20
		}
		if (len(nb[i:]) == len(isoNameSuffix) && bytes.Equal(nb[i:], isoNameSuffix)) ||
			(len(nb[i:]) == len(isoNameSuffix[1:]) && bytes.Equal(nb[i:], isoNameSuffix[1:])) {
			return string(nb[:i])
		}
		if nb[i] == byte(';') || nb[i] == byte('/') {
			nb[i] = byte('.')
		}
	}
	return string(nb)
}

func readBytes(r io.ReaderAt, offset int64, sz int) ([]byte, error) {
	buf := make([]byte, sz)
	_, err := r.ReadAt(buf, offset)
	return buf, err
}

func bytesToTime(b []byte) time.Time {
	year := int(b[0])
	month := time.Month(b[1])
	date := int(b[2])
	hour := int(b[3])
	minute := int(b[4])
	second := int(b[5])
	offset := int(int8(b[6]))
	location := time.FixedZone("iso", offset*15*60)
	return time.Date(year+1900, month, date, hour, minute, second, 0, location)
}

func decBytesToTime(b []byte) (time.Time, error) {
	if nullTime(b) {
		return time.Time{}, nil
	}
	year := string(b[0:4])
	month := string(b[4:6])
	date := string(b[6:8])
	hour := string(b[8:10])
	minute := string(b[10:12])
	second := string(b[12:14])
	csec := string(b[14:16])
	offset := int(int8(b[16]))
	location := offset * 15
	format := "2006-01-02T15:04:05-07:00"
	offsetHr := location / 60
	offsetMin := location % 60
	offsetString := ""
	// if negative offset, show it just on the hour part, not twice, so we end up with "-06:30" and not "-06:-30"
	switch {
	case offset == 0:
		offsetString = "+00:00"
	case offset < 0:
		offsetString = fmt.Sprintf("-%02d:%02d", -offsetHr, -offsetMin)
	case offset > 0:
		offsetString = fmt.Sprintf("+%02d:%02d", offsetHr, offsetMin)
	}
	return time.Parse(format, fmt.Sprintf("%s-%s-%sT%s:%s:%s.%s%s", year, month, date, hour, minute, second, csec, offsetString))
}

func timeToBytes(t time.Time) []byte {
	year := t.Year()
	month := t.Month()
	date := t.Day()
	second := t.Second()
	minute := t.Minute()
	hour := t.Hour()
	_, offset := t.Zone()
	b := make([]byte, 7, 7)
	b[0] = byte(year - 1900)
	b[1] = byte(month)
	b[2] = byte(date)
	b[3] = byte(hour)
	b[4] = byte(minute)
	b[5] = byte(second)
	b[6] = byte(int8(offset / 60 / 15))
	return b
}

func timeToDecBytes(t time.Time) []byte {
	year := strconv.Itoa(t.Year())
	month := strconv.Itoa(int(t.Month()))
	date := strconv.Itoa(t.Day())
	hour := strconv.Itoa(t.Hour())
	minute := strconv.Itoa(t.Minute())
	second := strconv.Itoa(t.Second())
	csec := strconv.Itoa(t.Nanosecond() / 1e+7)
	_, offset := t.Zone()
	b := make([]byte, 17, 17)
	copy(b[0:4], []byte(fmt.Sprintf("%04s", year)))
	copy(b[4:6], []byte(fmt.Sprintf("%02s", month)))
	copy(b[6:8], []byte(fmt.Sprintf("%02s", date)))
	copy(b[8:10], []byte(fmt.Sprintf("%02s", hour)))
	copy(b[10:12], []byte(fmt.Sprintf("%02s", minute)))
	copy(b[12:14], []byte(fmt.Sprintf("%02s", second)))
	copy(b[14:16], []byte(fmt.Sprintf("%02s", csec)))
	b[16] = byte(offset / 60 / 15)
	return b
}
