package iso

import (
	"fmt"
	"gitlab.com/rackn/rofs/c"
	"io"
)

const (
	volumeDescriptorSize = 2 * KB  // each volume descriptor is 2KB
	systemAreaSize       = 32 * KB // 32KB system area size
	defaultSectorSize    = 2 * KB
)

// isoFs gives access to the files in an ISO.
// It satisfies the fs.isoFs, fs.ReadDirFS, and fs.StatFS
// interfaces.
type isoFs struct {
	volumes        volumeDescriptors
	file           c.Src
	rootDir        *inode
	suspExtensions []suspExtension
	size           int64
	blocksize      int64
	suspEnabled    bool
	suspSkip       uint8
}

// Open returns a new c.Filesystem for src, assuming it is an ISO image we can support.
func Open(src c.Src) (res c.Filesystem, err error) {
	size := int64(0)
	size, err = src.Seek(0, io.SeekEnd)
	if err != nil {
		return
	}
	src.Seek(0, io.SeekStart)
	// at bare minimum, it must have enough space for the system area, one volume descriptor, one volume decriptor set terminator, and one block of data
	if size < systemAreaSize+2*volumeDescriptorSize+defaultSectorSize {
		err = fmt.Errorf("requested size is too small to allow for system area (%d), one volume descriptor (%d), one volume descriptor set terminator (%d), and one block (%d)", systemAreaSize, volumeDescriptorSize, volumeDescriptorSize, defaultSectorSize)
		return
	}

	// next read the volume descriptors, one at a time, until we hit the terminator
	var (
		pvd *primaryVolumeDescriptor
		vd  volumeDescriptor
		vds []volumeDescriptor
	)
done:
	for i := 0; ; i++ {
		var vdBytes []byte
		if vdBytes, err = readBytes(src, systemAreaSize+int64(i)*volumeDescriptorSize, volumeDescriptorSize); err != nil {
			err = fmt.Errorf("Unable to read bytes for volume descriptor %d: %v", i, err)
			return
		} else if vd, err = volumeDescriptorFromBytes(vdBytes, src); err != nil {
			err = fmt.Errorf("Error reading Volume Descriptor: %v", err)
			return
		}
		// is this a terminator?
		switch vd.Type() {
		case volumeDescriptorTerminator:
			break done
		case volumeDescriptorPrimary:
			pvd = vd.(*primaryVolumeDescriptor)
		}
		vds = append(vds, vd)
	}

	// load up our path table and root directory entry
	var (
		rootDirEntry *inode
	)
	blocksize := int64(pvd.blocksize)
	if pvd != nil {
		rootDirEntry = pvd.rootDirectoryEntry
	}

	var (
		suspEnabled  bool
		skipBytes    uint8
		suspHandlers []suspExtension
		b            []byte
		de           *inode
	)
	// is system use enabled?
	location := rootDirEntry.extents[0]
	b = make([]byte, 1)
	if _, err = src.ReadAt(b, location.backingFileOffset); err != nil {
		err = fmt.Errorf("Unable to read root directory size at location %d: %v", location, err)
		return
	}
	b = make([]byte, int(b[0]))
	if _, err = src.ReadAt(b, location.backingFileOffset); err != nil {
		err = fmt.Errorf("Unable to read root directory entry at location %d: %v", location, err)
		return
	}
	if de, err = parseDirEntry(b, &isoFs{suspEnabled: true, file: src, blocksize: blocksize}); err != nil {
		err = fmt.Errorf("Error parsing root entry from bytes: %v", err)
		return
	}
	// is the SUSP in use?
	for _, ext := range de.extensions {
		if s, ok := ext.(directoryEntrySystemUseExtensionSharingProtocolIndicator); ok {
			suspEnabled = true
			skipBytes = s.SkipBytes()
		}
		// register any extension handlers
		if s, ok := ext.(directoryEntrySystemUseExtensionReference); suspEnabled && ok {
			extHandler := getRockRidgeExtension(s.ExtensionID())
			if extHandler != nil {
				suspHandlers = append(suspHandlers, extHandler)
			}
		}
	}
	iso := &isoFs{
		size: size,
		file: src,
		volumes: volumeDescriptors{
			descriptors: vds,
			primary:     pvd,
		},
		blocksize:      blocksize,
		rootDir:        rootDirEntry,
		suspEnabled:    suspEnabled,
		suspSkip:       skipBytes,
		suspExtensions: suspHandlers,
	}
	rootDirEntry.fs = iso
	res.InodeProvider = iso
	err = iso.rootDir.readDir()
	return
}

// Label returns the label assigned to the fs
func (iso *isoFs) Label() string {
	if iso.volumes.primary == nil {
		return ""
	}
	return iso.volumes.primary.volumeIdentifier
}

func (iso *isoFs) Root() c.Inode {
	return iso.rootDir
}

func (iso *isoFs) Close() error {
	if cl, ok := iso.file.(io.Closer); ok {
		return cl.Close()
	}
	return nil
}
