package iso

import (
	"encoding/binary"
	"fmt"
	"gitlab.com/rackn/rofs/c"
	"io"
	"io/fs"
	"os"
	"sort"
	"strings"
	"syscall"
	"time"
)

const (
	directoryEntryMinSize uint8 = 34  // min size is all the required fields (33 bytes) plus 1 byte for the fname
	directoryEntryMaxSize int   = 254 // max size allowed
)

func toExtents(loc, siz, blksz int64) []extent {
	return []extent{{backingFileOffset: loc * blksz, size: siz}}
}

type extent struct {
	offset            int64 // offset from the start of the file data for the current inode
	backingFileOffset int64 // offset from the start of the backing file for the current inode
	size              int64 // size of the extent.
}

// inode is a single directory entry
// also fulfills fs.FileInfo, os.FileInfo, and fs.DirEntry
//
//	Name() string       // base name of the file
//	Size() int64        // length in bytes for regular files; system-dependent for others
//	Mode() FileMode     // file mode bits
//	ModTime() time.Time // modification time
//	IsDir() bool        // abbreviation for Mode().IsDir()
//	Sys() interface{}   // underlying data source (can return nil)
type inode struct {
	fs             *isoFs
	ctime          time.Time
	fname          string
	dirEntries     []fs.DirEntry
	extensions     []directoryEntrySystemUseExtension
	extents        []extent
	volumeSequence uint16
	flags          uint16
	extAttrSize    uint8
}

func (i *inode) hasOwnerGroupPermissions() bool {
	return i.flags&hasOwnerGroupPermissions > 0
}

func (i *inode) hasExtendedAttrs() bool {
	return i.flags&hasExtendedAttrs > 0
}

func (i *inode) isAssociated() bool {
	return i.flags&isAssociated > 0
}

func (i *inode) isSubdirectory() bool {
	return i.flags&isSubdirectory > 0
}

func (i *inode) isHidden() bool {
	return i.flags&isHidden > 0
}

func (i *inode) hasMoreEntries() bool {
	return i.flags&hasMoreEntries > 0
}

func (i *inode) isSelf() bool {
	return i.flags&isSelf > 0
}

func (i *inode) isParent() bool {
	return i.flags&isParent > 0
}

var (
	_ = fs.FileInfo(&inode{})
	_ = os.FileInfo(&inode{})
	_ = fs.DirEntry(&inode{})
)

const (
	isHidden                 = 0x0001
	isSubdirectory           = 0x0002
	isAssociated             = 0x0004
	hasExtendedAttrs         = 0x0008
	hasOwnerGroupPermissions = 0x0010
	hasMoreEntries           = 0x0080
	isSelf                   = 0x0100
	isParent                 = 0x0200
)

func dirEntryFromBytes(b []byte, ext []suspExtension, blksz int64, src io.ReaderAt) (*inode, error) {
	// has to be at least 34 bytes
	if len(b) < int(directoryEntryMinSize) {
		return nil, fmt.Errorf("Cannot read inode from %d bytes, fewer than minimum of %d bytes", len(b), directoryEntryMinSize)
	}
	recordSize := b[0]
	// what if it is not the right size?
	if len(b) != int(recordSize) {
		return nil, fmt.Errorf("inode should be size %d bytes according to first byte, but have %d bytes", recordSize, len(b))
	}
	extAttrSize := b[1]
	location := binary.LittleEndian.Uint32(b[2:6])
	size := binary.LittleEndian.Uint32(b[10:14])
	creation := bytesToTime(b[18:25])
	flags := uint16(b[25])

	volumeSequence := binary.LittleEndian.Uint16(b[28:30])

	// size includes the ";1" at the end as two bytes and any padding
	namelen := b[32]
	nameLenWithPadding := namelen

	// get the fname itself
	nameBytes := b[33 : 33+namelen]
	if namelen > 1 && namelen%2 == 0 {
		nameLenWithPadding++
	}
	var filename string

	switch {
	case namelen == 1 && nameBytes[0] == 0x00:
		filename = ""
		flags |= isSelf
	case namelen == 1 && nameBytes[0] == 0x01:
		filename = ""
		flags |= isParent
	default:
		filename = string(nameBytes)
	}

	// and now for extensions in the system use area
	suspFields := make([]directoryEntrySystemUseExtension, 0)
	if len(b) > 33+int(nameLenWithPadding) {
		var err error
		suspFields, err = parseDirectoryEntryExtensions(b[33+nameLenWithPadding:], ext)
		if err != nil {
			return nil, fmt.Errorf("Unable to parse directory entry extensions: %v", err)
		}
	}

	return &inode{
		extAttrSize:    extAttrSize,
		extents:        toExtents(int64(location), int64(size), blksz),
		ctime:          creation,
		flags:          flags,
		volumeSequence: volumeSequence,
		fname:          filename,
		extensions:     suspFields,
	}, nil
}

// parseDirEntry takes the bytes of a single directory entry
// and parses it, including pulling in continuation entry bytes
func parseDirEntry(b []byte, f *isoFs) (*inode, error) {
	// empty entry means nothing more to read - this might not actually be accurate, but work with it for now
	entryLen := int(b[0])
	if entryLen == 0 {
		return nil, nil
	}
	// get the bytes
	de, err := dirEntryFromBytes(b[:entryLen], f.suspExtensions, f.blocksize, f.file)
	if err != nil {
		return nil, fmt.Errorf("Invalid directory entry : %v", err)
	}
	de.fs = f

	if f.suspEnabled && len(de.extensions) > 0 {
		// if the last entry is a continuation SUSP entry and SUSP is enabled, we need to follow and parse them
		// because the extensions can be a linked list directory -> CE area -> CE area ...
		//   we need to loop until it is no more
		for {
			if ce, ok := de.extensions[len(de.extensions)-1].(directoryEntrySystemUseContinuation); ok {
				continuationBytes := make([]byte, ce.ContinuationLength())
				location := int64(ce.Location())*f.blocksize + int64(ce.Offset())
				// read it from disk
				if _, err := f.file.ReadAt(continuationBytes, location); err != nil {
					return nil, fmt.Errorf("Error reading continuation entry data at %d: %v", location, err)
				} else if entries, err := parseDirectoryEntryExtensions(continuationBytes, f.suspExtensions); err != nil {
					return nil, fmt.Errorf("Error parsing continuation entry data at %d: %v", location, err)
				} else {
					// remove the CE one from the extensions array and append our new ones
					de.extensions = append(de.extensions[:len(de.extensions)-1], entries...)
				}
			} else {
				break
			}
		}
	}
	return de, nil

}

// parseDirEntries takes all of the bytes in a special file (i.e. a directory)
// and gets all of the DirectoryEntry for that directory
// this is, essentially, the equivalent of `ls -l` or if you prefer `dir`
func parseDirEntries(b []byte, f *isoFs) ([]*inode, error) {
	dirEntries := make([]*inode, 0, 20)
	count := 0
	var working *inode
	for i := 0; i < len(b); count++ {
		// empty entry means nothing more to read - this might not actually be accurate, but work with it for now
		entryLen := int(b[i+0])
		if entryLen == 0 {
			i += (int(f.blocksize) - i%int(f.blocksize))
			continue
		}
		de, err := parseDirEntry(b[i+0:i+entryLen], f)
		if err != nil {
			return nil, fmt.Errorf("Invalid directory entry %d at byte %d: %v", count, i, err)
		}
		// some extensions to directory relocation, so check if we should ignore it
		if f.suspEnabled {
			for _, e := range f.suspExtensions {
				if e.Relocated(de) {
					de = nil
					break
				}
			}
		}

		if de != nil {
			if working == nil {
				working = de
			} else {
				working.extents = append(working.extents, de.extents...)
			}
			if !de.hasMoreEntries() {
				runningOffset := int64(0)
				for idx := range working.extents {
					working.extents[idx].offset = runningOffset
					runningOffset += working.extents[idx].size
				}
				working.flags &^= hasMoreEntries
				dirEntries = append(dirEntries, working)
				working = nil
			}
		}
		i += entryLen
	}
	return dirEntries, nil
}

func (ino *inode) readDir() error {
	var dirEntries []*inode
	dirb := make([]byte, ino.extents[0].size)
	if _, err := ino.fs.file.ReadAt(dirb, ino.extents[0].backingFileOffset); err != nil {
		return fmt.Errorf("Could not read directory: %v", err)
	} else if dirEntries, err = parseDirEntries(dirb, ino.fs); err != nil {
		return fmt.Errorf("Could not parse directory: %v", err)
	}
	for _, entry := range dirEntries {
		if !entry.isSubdirectory() && ino.fs.suspEnabled && !entry.isSelf() && !entry.isParent() {
			// It looks like a file, but it might actually be a relocated subdirectory.
			for _, e := range ino.fs.suspExtensions {
				location2 := e.GetDirectoryLocation(entry)
				if location2 == 0 {
					continue
				}
				dirb := make([]byte, ino.fs.blocksize)
				if _, err := ino.fs.file.ReadAt(dirb, location2); err != nil {
					return fmt.Errorf("Could not read bytes of relocated directory %s from block %d: %v", entry.Name(), location2, err)
				} else if entry2, err := parseDirEntry(dirb[:int(dirb[0])], ino.fs); err != nil {
					return fmt.Errorf("Error converting bytes to a directory entry for relocated directory %s from block %d: %v", entry.Name(), location2, err)
				} else {
					entry.extents = entry2.extents
					entry.flags |= isSubdirectory
					break
				}
			}
		}
		if entry.isSelf() || entry.isParent() {
			continue
		}
		ino.dirEntries = append(ino.dirEntries, entry)
	}
	sort.Slice(ino.dirEntries, func(i, j int) bool { return ino.dirEntries[i].Name() < ino.dirEntries[j].Name() })
	for i := range ino.dirEntries {
		if ino.dirEntries[i].IsDir() {
			if err := ino.dirEntries[i].(*inode).readDir(); err != nil {
				return err
			}
		}
	}
	return nil
}

func (ino *inode) getDent(parts []string, offset int) (*inode, int) {
	if offset < len(parts) && ino.isSubdirectory() {
		i, found := sort.Find(len(ino.dirEntries), func(i int) int {
			return ino.dirEntries[i].(*inode).nameCmp(parts[offset])
		})
		if found {
			return ino.dirEntries[i].(*inode).getDent(parts, offset+1)
		}
	}
	return ino, offset
}

func (ino *inode) name() (res string, cleaned bool) {
	if ino.fs.suspEnabled && !ino.isSelf() && !ino.isParent() {
		for _, e := range ino.fs.suspExtensions {
			if filename, err := e.GetFilename(ino); err == nil {
				return filename, false
			}
		}

	}
	return cleanName(ino.fname), true
}

func (ino *inode) nameCmp(target string) int {
	probe, cleaned := ino.name()
	if cleaned {
		return strings.Compare(cleanName(target), probe)
	}
	return strings.Compare(target, probe)
}

func (ino *inode) getExtents(offset, size int64) (res c.Extents) {
	if offset < 0 || offset >= ino.Size() {
		return
	}
	runningOffset := int64(0)
	res = c.AllocExtents(len(ino.extents))
	for _, ex := range ino.extents {
		if size <= 0 {
			break
		}
		if ex.offset+ex.size < offset {
			continue
		}
		offsetInExtent := offset - ex.offset
		dataSize := ex.size - offsetInExtent
		if dataSize > size {
			dataSize = size
		}
		res = res.Append(ino.fs.file, ex.backingFileOffset+offsetInExtent, dataSize)
		size -= dataSize
		offset += dataSize
		runningOffset += dataSize
	}
	return
}

func (ino *inode) Open() *c.Reader {
	return c.GetExtents(ino.getExtents).Reader()
}

// Name of the file
func (ino *inode) Name() string {
	name, _ := ino.name()
	return name
}

// Size of the file.
func (ino *inode) Size() (size int64) {
	for i := range ino.extents {
		size += ino.extents[i].size
	}
	return
}

// Mode bits for the file.
func (ino *inode) Mode() (mode fs.FileMode) {
	mode = 0755
	for _, e := range ino.extensions {
		if e.Signature() == rockRidgeSignaturePosixAttributes {
			mode = e.(rockRidgePosixAttributes).mode
		}
	}
	if ino.isSubdirectory() {
		mode = fs.ModeDir | mode
	}
	return
}

// ModTime of the file
func (ino *inode) ModTime() time.Time {
	return ino.ctime
}

// IsDir returns true if the file is a directory.
func (ino *inode) IsDir() bool {
	return ino.Mode()&fs.ModeDir > 0
}

// Sys returns the underlying data for the info.  We return nil, since
// we are information hiders.
func (ino *inode) Sys() interface{} {
	return nil
}

// Type of the file.
func (ino *inode) Type() fs.FileMode {
	return fs.ModeType & ino.Mode()
}

// Info about the file.  We just return ourself.
func (ino *inode) Info() (fs.FileInfo, error) {
	return ino, nil
}

func (ino *inode) IsSymlink() bool {
	return ino.Mode()&fs.ModeSymlink > 0
}

func (ino *inode) symLink() string {
	for _, e := range ino.extensions {
		if e.Signature() == rockRidgeSignatureSymbolicLink {
			return e.(rockRidgeSymlink).name
		}
	}
	return ""
}

func (ino *inode) ReadLink() (sl string, err error) {
	if sl = ino.symLink(); sl == "" {
		err = fs.ErrInvalid
	}
	return
}

func (ino *inode) ReadDir() ([]fs.DirEntry, error) {
	if ino.IsDir() {
		return ino.dirEntries, nil
	}
	return nil, syscall.ENOTDIR
}

func (ino *inode) ReadXattrs() map[string]string {
	return nil
}
