package iso

import (
	"gitlab.com/rackn/rofs/c"
	"io/fs"
	"os"
	"testing"
)

func TestIso9660ReadDirectory(t *testing.T) {
	// will use the file.iso fixture to test an actual directory
	// \ (root directory) should be in one block
	// \FOO should be in multiple blocks
	file, err := os.Open(ISO9660File)
	defer file.Close()
	if err != nil {
		t.Fatalf("Could not open file %s to read: %v", ISO9660File, err)
	}
	isoFs := &isoFs{
		size:      ISO9660Size,
		file:      file,
		blocksize: 2048,
	}
	validDe, _, _, _, err := get9660DirectoryEntries(isoFs)
	if err != nil {
		t.Fatalf("Unable to read valid directory entries: %v", err)
	}
	validDeExtended, _, _, err := getValidDirectoryEntriesExtended(isoFs)
	if err != nil {
		t.Fatalf("Unable to read valid directory entries extended: %v", err)
	}
	isoFs.rootDir = validDe[0] // validDe contains root directory entries, first one is the root itself
	if err = isoFs.rootDir.readDir(); err != nil {
		t.Fatalf("Error processing directory tree: %v", err)
	}
	iso := c.Filesystem{isoFs}
	tests := []struct {
		path    string
		entries []*inode
	}{
		{".", validDe},
		{`foo`, validDeExtended},
	}
	for _, tt := range tests {
		var entries []fs.DirEntry
		ino, err := iso.Lookup(tt.path, true)
		if err == nil {
			entries, err = ino.ReadDir()
		}
		switch {
		case err != nil:
			t.Errorf("isoFs.ReadDir(%s): unexpected nil error: %v", tt.path, err)
		case len(entries) != len(tt.entries)-2:
			t.Errorf("isoFs.ReadDir(%s): number of entries do not match, actual %d expected %d", tt.path, len(entries), len(tt.entries))
		default:
			for i, entry := range entries {
				if !compareDirectoryEntries(entry.(*inode), tt.entries[i+2], false, false) {
					t.Errorf("isoFs.ReadDir(%s) %d: entries do not match, actual then expected", tt.path, i)
					t.Logf("%#v\n", entry)
					t.Logf("%#v\n", tt.entries[i+2])
				}
			}
		}
	}
}

func TestRockRidgeReadDirectory(t *testing.T) {
	// will use the file.iso fixture to test an actual directory
	// \ (root directory) should be in one block
	// \FOO should be in multiple blocks
	file, err := os.Open(RockRidgeFile)
	defer file.Close()
	if err != nil {
		t.Fatalf("Could not open file %s to read: %v", RockRidgeFile, err)
	}
	isoFs := &isoFs{
		size:           ISO9660Size,
		file:           file,
		blocksize:      2048,
		suspEnabled:    true,
		suspExtensions: []suspExtension{getRockRidgeExtension("RRIP_1991A")},
	}
	validDe, _, _, _, err := getRockRidgeDirectoryEntries(isoFs, false)
	if err != nil {
		t.Fatalf("Unable to read valid directory entries: %v", err)
	}
	isoFs.rootDir = validDe[0] // validDe contains root directory entries, first one is the root itself
	if err = isoFs.rootDir.readDir(); err != nil {
		t.Fatalf("Error filling directory tree: %v", err)
	}
	iso := c.Filesystem{isoFs}
	tests := []struct {
		path    string
		entries []*inode
	}{
		{`.`, validDe},
	}
	for _, tt := range tests {
		var entries []fs.DirEntry
		ino, err := iso.Lookup(tt.path, true)
		if err == nil {
			entries, err = ino.ReadDir()
		}
		switch {
		case err != nil:
			t.Errorf("fs.ReadDir(%s): unexpected nil error: %v", tt.path, err)
		case len(entries) != len(tt.entries)-2:
			t.Errorf("fs.ReadDir(%s): number of entries do not match, actual %d expected %d", tt.path, len(entries), len(tt.entries))
		default:
			for i, entry := range entries {
				if !compareDirectoryEntries(entry.(*inode), tt.entries[i+2], false, false) {
					t.Errorf("fs.ReadDir(%s) %d %s: entries do not match, actual then expected", tt.path, i, entry.(*inode).fname)
					t.Logf("%#v\n", entry)
					t.Logf("%#v\n", tt.entries[i+2])
				}
			}
		}
	}
}

func TestLabel(t *testing.T) {
	t.Run("no primary volume descriptor", func(t *testing.T) {
		expected := ""
		fs := isoFs{}
		label := fs.Label()
		if label != expected {
			t.Errorf("mismatched labels, actual '%s' expected '%s'", label, expected)
		}
	})
	t.Run("primary volume descriptor no label", func(t *testing.T) {
		expected := ""
		fs := isoFs{
			volumes: volumeDescriptors{
				primary: &primaryVolumeDescriptor{},
			},
		}
		label := fs.Label()
		if label != expected {
			t.Errorf("mismatched labels, actual '%s' expected '%s'", label, expected)
		}
	})
	t.Run("primary volume descriptor with label", func(t *testing.T) {
		expected := "myisolabel"
		fs := isoFs{
			volumes: volumeDescriptors{
				primary: &primaryVolumeDescriptor{
					volumeIdentifier: expected,
				},
			},
		}
		label := fs.Label()
		if label != expected {
			t.Errorf("mismatched labels, actual '%s' expected '%s'", label, expected)
		}
	})
}
