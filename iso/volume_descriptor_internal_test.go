package iso

import (
	"fmt"
	"io/ioutil"
	"reflect"
	"testing"
	"time"
)

const (
	volRecordsFile = "./testdata/volrecords.iso"
)

var (
	timeDecBytesTests = []struct {
		b   []byte
		rfc string
	}{
		// see reference at https://wiki.osdev.org/ISO_9660#Volume_Descriptors
		{append([]byte("1980010214353600"), 0), "1980-01-02T14:35:36Z"},
		{append([]byte("1995112500160700"), 8), "1995-11-25T00:16:07+02:00"},
		{append([]byte("2101063012000000"), 0xe6), "2101-06-30T12:00:00-06:30"},
	}
)

func comparePrimaryVolumeDescriptorsIgnoreDates(a, b *primaryVolumeDescriptor) bool {
	now := time.Now()
	// copy values so we do not mess up the originals
	c := &primaryVolumeDescriptor{}
	d := &primaryVolumeDescriptor{}
	*c = *a
	*d = *b

	// unify fields we let be equal
	c.creation = now
	d.creation = now
	c.effective = now
	d.effective = now
	c.modification = now
	d.modification = now
	c.expiration = now
	d.expiration = now

	// cannot actually compare root directory entry since can be pointers to different things
	// so we compare them separately, and then compare the rest
	if !reflect.DeepEqual(*c.rootDirectoryEntry, *c.rootDirectoryEntry) {
		return false
	}
	c.rootDirectoryEntry = nil
	d.rootDirectoryEntry = nil
	return *c == *d
}

func get9660PrimaryVolumeDescriptor() (*primaryVolumeDescriptor, []byte, error) {
	// these are taken from the file ./testdata/fat32.img, see ./testdata/README.md
	blocksize := 2048
	pvdSector := 16
	t1, _ := time.Parse(time.RFC3339, "2017-11-26T07:53:16Z")

	// read correct bytes off of disk
	input, err := ioutil.ReadFile(ISO9660File)
	if err != nil {
		return nil, nil, fmt.Errorf("Error reading data from iso9660 test fixture %s: %v", ISO9660File, err)
	}

	start := pvdSector * blocksize // PVD sector

	// five blocks, since we know it is five blocks
	allBytes := input[start : start+blocksize]

	pvd := &primaryVolumeDescriptor{
		systemIdentifier:           fmt.Sprintf("%32v", ""),
		volumeIdentifier:           "ISOIMAGE                        ",
		volumeSize:                 5386, // in bytes
		setSize:                    1,
		sequenceNumber:             1,
		blocksize:                  2048,
		pathTableSize:              168,
		pathTableLLocation:         35,
		pathTableLOptionalLocation: 0,
		pathTableMLocation:         36,
		pathTableMOptionalLocation: 0,
		rootDirectoryEntry:         &inode{},
		volumeSetIdentifier:        fmt.Sprintf("%128v", ""),
		publisherIdentifier:        fmt.Sprintf("%128v", ""),
		preparerIdentifier:         fmt.Sprintf("%-128v", "XORRISO-1.4.8 2017.09.12.143001, LIBISOBURN-1.4.8, LIBISOFS-1.4.8, LIBBURN-1.4.8"),
		applicationIdentifier:      fmt.Sprintf("%128v", ""),
		copyrightFile:              fmt.Sprintf("%37v", ""),
		abstractFile:               fmt.Sprintf("%37v", ""),
		bibliographicFile:          fmt.Sprintf("%37v", ""),
		creation:                   t1,
		modification:               t1,
		expiration:                 t1,
		effective:                  t1,
	}
	// we need the root inode
	rootDirEntry := &inode{
		extAttrSize:    0,
		extents:        toExtents(0x12, 0x800, 2048),
		ctime:          t1,
		flags:          isSubdirectory | isSelf,
		volumeSequence: 1,
		fname:          "",
		fs:             nil,
	}
	pvd.rootDirectoryEntry = rootDirEntry
	return pvd, allBytes, nil
}

func TestDecBytesToTime(t *testing.T) {
	for _, tt := range timeDecBytesTests {
		output, err := decBytesToTime(tt.b)
		if err != nil {
			t.Fatalf("Error parsing actual date: %v", err)
		}
		expected, err := time.Parse(time.RFC3339, tt.rfc)
		if err != nil {
			t.Fatalf("Error parsing expected date: %v", err)
		}
		if !expected.Equal(output) {
			t.Errorf("decBytesToTime(%d) expected output %v, actual %v", tt.b, expected, output)
		}
	}
}

func TestParsePrimaryVolumeDescriptor(t *testing.T) {
	validPvd, validBytes, err := get9660PrimaryVolumeDescriptor()
	if err != nil {
		t.Fatal(err)
	}
	pvd, err := parsePrimaryVolumeDescriptor(validBytes, nil)
	if err != nil {
		t.Fatalf("Error parsing primary volume descriptor: %v", err)
	}
	if !comparePrimaryVolumeDescriptorsIgnoreDates(pvd, validPvd) {
		t.Errorf("Mismatched primary volume descriptor, actual vs expected")
		t.Logf("%#v\n", pvd)
		t.Logf("%#v\n", validPvd)
	}
}
func TestPrimaryVolumeDescriptorType(t *testing.T) {
	pvd := &primaryVolumeDescriptor{}
	if pvd.Type() != volumeDescriptorPrimary {
		t.Errorf("Primary Volume Descriptor type was %v instead of expected %v", pvd.Type(), volumeDescriptorPrimary)
	}
}
