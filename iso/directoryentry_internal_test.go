package iso

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"reflect"
	"sort"
	"testing"
	"time"
)

var (
	timeBytesTests = []struct {
		b   []byte
		rfc string
	}{
		// see reference at https://wiki.osdev.org/ISO_9660#Directories
		{[]byte{80, 1, 2, 14, 35, 36, 0}, "1980-01-02T14:35:36+00:00"},
		{[]byte{95, 11, 25, 0, 16, 7, 8}, "1995-11-25T00:16:07+02:00"},
		{[]byte{101, 6, 30, 12, 0, 0, 0xe6}, "2001-06-30T12:00:00-06:30"},
	}
)

func compareDirectoryEntries(a, b *inode, compareDates, compareExtensions bool) bool {
	now := time.Now()
	// copy values so we do not mess up the originals
	c := &inode{}
	d := &inode{}
	*c = *a
	*d = *b
	c.dirEntries = nil
	d.dirEntries = nil

	if !compareDates {
		// unify fields we let be equal
		c.ctime = now
		d.ctime = now
	}

	cExt := c.extensions
	dExt := d.extensions
	c.extensions = nil
	d.extensions = nil

	shallowMatch := reflect.DeepEqual(*c, *d)
	extMatch := true
	switch {
	case !compareExtensions:
		extMatch = true
	case len(cExt) != len(dExt):
		extMatch = false
	default:
		// compare them
		for i, e := range cExt {
			if e.Signature() != dExt[i].Signature() || e.Length() != dExt[i].Length() || e.Version() != dExt[i].Version() || bytes.Compare(e.Data(), dExt[i].Data()) != 0 {
				extMatch = false
				break
			}
		}
	}
	return shallowMatch && extMatch
}

// get9660DirectoryEntries get a list of valid directory entries for path /
// returns:
// slice of entries, slice of byte slices for each, entire bytes for all, map of location to byte slice
func get9660DirectoryEntries(f *isoFs) ([]*inode, [][][]byte, []byte, map[int64][]byte, error) {
	blocksize := int64(2048)
	rootSector := int64(18)
	ceSector := int64(19)
	// read correct bytes off of disk
	input, err := ioutil.ReadFile(ISO9660File)
	if err != nil {
		return nil, nil, nil, nil, fmt.Errorf("Error reading data from iso9660 test fixture %s: %v", ISO9660File, err)
	}

	// start of root directory in file.iso - sector 18
	// sector 0-15 - system area
	// sector 16 - Primary Volume Descriptor
	// sector 17 - Volume Descriptor Set Terimnator
	// sector 18 - / (root) directory
	// sector 19 - Continuation Area for root directory first entry
	// sector 20 - /abc directory
	// sector 21 - /bar directory
	// sector 22 - /foo directory
	// sector 23 - /foo directory
	// sector 24 - /foo directory
	// sector 25 - /foo directory
	// sector 26 - /foo directory
	// sector 27 - L path table
	// sector 28 - M path table
	// sector 33-2592 - /ABC/LARGEFILE
	// sector 2593-5152 - /BAR/LARGEFILE
	// sector 5153 - /FOO/FILENA01
	//  ..
	// sector 5228 - /FOO/FILENA75
	// sector 5229 - /README.MD
	startRoot := rootSector * blocksize // start of root directory in file.iso

	// one block, since we know it is just one block
	allBytes := input[startRoot : startRoot+blocksize]

	startCe := ceSector * blocksize               // start of ce area in file.iso
	ceBytes := input[startCe : startCe+blocksize] // CE block
	// cut the CE block down to just the CE bytes
	ceBytes = ceBytes[:237]

	t1 := time.Now()
	entries := []*inode{
		{
			extAttrSize:    0,
			extents:        toExtents(0x12, 0x800, blocksize),
			ctime:          t1,
			flags:          isSelf | isSubdirectory,
			volumeSequence: 1,
			fname:          "",
			fs:             f,
		},
		{
			extAttrSize:    0,
			extents:        toExtents(0x12, 0x800, blocksize),
			ctime:          t1,
			flags:          isSubdirectory | isParent,
			volumeSequence: 1,
			fname:          "",
			fs:             f,
		},
		{
			extAttrSize:    0,
			extents:        toExtents(0x13, 0x800, blocksize),
			ctime:          t1,
			flags:          isSubdirectory,
			volumeSequence: 1,
			fname:          "ABC",
			fs:             f,
		},
		{
			extAttrSize:    0,
			extents:        toExtents(0x14, 0x800, blocksize),
			ctime:          t1,
			flags:          isSubdirectory,
			volumeSequence: 1,
			fname:          "BAR",
			fs:             f,
		},
		{
			extAttrSize:    0,
			extents:        toExtents(0x15, 0x800, blocksize),
			ctime:          t1,
			flags:          isSubdirectory,
			volumeSequence: 1,
			fname:          "DEEP",
			fs:             f,
		},
		{
			extAttrSize:    0,
			extents:        toExtents(0x21, 0x1000, blocksize),
			ctime:          t1,
			flags:          isSubdirectory,
			volumeSequence: 1,
			fname:          "FOO",
			fs:             f,
		},
		{
			extAttrSize:    0,
			extents:        toExtents(0x1473, 0x7, blocksize),
			ctime:          t1,
			volumeSequence: 1,
			fname:          "README.MD;1",
			fs:             f,
		},
	}

	// b is a slice of byte slices; each one represents the raw directory entry bytes for a inode
	//  parallels the directory Entries above
	// each entry is a slice of slices: the first is the raw data for the entry
	//  the second and any subsequent are continuation areas
	b := make([][][]byte, 0, 8)
	read := int64(0)
	for range entries {
		recordSize := int64(allBytes[int(read)])
		// do we have a 0 point? if so, move ahead until we pass it at the end of the block
		if recordSize == 0x00 {
			read += (blocksize - read%blocksize)
		}
		b2 := make([][]byte, 0)
		b2 = append(b2, allBytes[read:read+recordSize])
		b = append(b, b2)
		read += recordSize
	}
	// the first one has a continuation entry
	b[0] = append(b[0], ceBytes)

	byteMap := map[int64][]byte{
		rootSector: allBytes, ceSector: ceBytes,
	}
	sort.Slice(entries[2:], func(i, j int) bool {
		return entries[i+2].Name() < entries[j+2].Name()
	})
	return entries, b, allBytes, byteMap, nil
}

// getRockRidgeDirectoryEntries get a list of valid directory entries for path /
// returns:
// slice of entries, slice of byte slices for each, entire bytes for all, map of location to byte slice
func getRockRidgeDirectoryEntries(f *isoFs, includeRelocated bool) ([]*inode, [][][]byte, []byte, map[int64][]byte, error) {
	blocksize := int64(2048)
	rootSector := int64(18)
	ceSector := int64(19)
	// read correct bytes off of disk
	input, err := ioutil.ReadFile(RockRidgeFile)
	if err != nil {
		return nil, nil, nil, nil, fmt.Errorf("Error reading data from iso9660 test fixture %s: %v", ISO9660File, err)
	}

	// start of root directory in file.iso - sector 18
	// sector 0-15 - system area
	// sector 16 - Primary Volume Descriptor
	// sector 17 - Volume Descriptor Set Terimnator
	// sector 18 - / (root) directory
	// sector 19 - Continuation Area for root directory first entry
	// sector 20 - /abc directory
	// sector 21 - /bar directory
	// sector 22 - /foo directory
	// sector 23 - /foo directory
	// sector 24 - /foo directory
	// sector 25 - /foo directory
	// sector 26 - /foo directory
	// sector 27 - L path table
	// sector 28 - M path table
	// sector 33-2592 - /ABC/LARGEFILE
	// sector 2593-5152 - /BAR/LARGEFILE
	// sector 5153 - /FOO/FILENA01
	//  ..
	// sector 5228 - /FOO/FILENA75
	// sector 5229 - /README.MD
	startRoot := rootSector * blocksize // start of root directory in file.iso

	// one block, since we know it is just one block
	allBytes := input[startRoot : startRoot+blocksize]

	startCe := ceSector * blocksize               // start of ce area in file.iso
	ceBytes := input[startCe : startCe+blocksize] // CE block
	// cut the CE block down to just the CE bytes
	ceBytes = ceBytes[:237]

	t1 := time.Now()
	entries := []*inode{
		&inode{
			extAttrSize:    0,
			extents:        toExtents(0x12, 0x800, blocksize),
			ctime:          t1,
			flags:          isSubdirectory | isSelf,
			volumeSequence: 1,
			fname:          "",
			fs:             f,
			extensions: []directoryEntrySystemUseExtension{
				directoryEntrySystemUseExtensionSharingProtocolIndicator{0},
				rockRidgePosixAttributes{mode: 0755 | os.ModeDir, linkCount: 1, uid: 0, gid: 0, length: 36},
				rockRidgeTimestamps{longForm: false, stamps: []rockRidgeTimestamp{
					{timestampType: rockRidgeTimestampModify, time: bytesToTime([]byte{0x76, 0x9, 0x1b, 0xb, 0x2f, 0x15, 0x0})},
					{timestampType: rockRidgeTimestampAccess, time: bytesToTime([]byte{0x76, 0x9, 0x1b, 0xb, 0x2f, 0x29, 0x0})},
					{timestampType: rockRidgeTimestampAttribute, time: bytesToTime([]byte{0x76, 0x9, 0x1b, 0xb, 0x2f, 0x15, 0x0})},
				},
				},
				directoryEntrySystemUseExtensionReference{extensionVersion: 1, id: "RRIP_1991A", descriptor: "THE ROCK RIDGE INTERCHANGE PROTOCOL PROVIDES SUPPORT FOR POSIX FILE SYSTEM SEMANTICS", source: "PLEASE CONTACT DISC PUBLISHER FOR SPECIFICATION SOURCE.  SEE PUBLISHER IDENTIFIER IN PRIMARY VOLUME DESCRIPTOR FOR CONTACT INFORMATION."},
			},
		},
		&inode{
			extAttrSize:    0,
			extents:        toExtents(0x12, 0x800, blocksize),
			ctime:          t1,
			flags:          isSubdirectory | isParent,
			volumeSequence: 1,
			fname:          "",
			fs:             f,
			extensions: []directoryEntrySystemUseExtension{
				rockRidgePosixAttributes{mode: 0755 | os.ModeDir, linkCount: 1, uid: 0, gid: 0, length: 36},
				rockRidgeTimestamps{longForm: false, stamps: []rockRidgeTimestamp{
					{timestampType: rockRidgeTimestampModify, time: bytesToTime([]byte{0x76, 0x9, 0x1b, 0xb, 0x2f, 0x15, 0x0})},
					{timestampType: rockRidgeTimestampAccess, time: bytesToTime([]byte{0x76, 0x9, 0x1b, 0xb, 0x2f, 0x29, 0x0})},
					{timestampType: rockRidgeTimestampAttribute, time: bytesToTime([]byte{0x76, 0x9, 0x1b, 0xb, 0x2f, 0x15, 0x0})},
				},
				},
			},
		},
		&inode{
			extAttrSize:    0,
			extents:        toExtents(0x14, 0x800, blocksize),
			ctime:          t1,
			flags:          isSubdirectory,
			volumeSequence: 1,
			fname:          "ABC",
			fs:             f,
			extensions: []directoryEntrySystemUseExtension{
				rockRidgePosixAttributes{mode: 0755 | os.ModeDir, linkCount: 1, uid: 0, gid: 0, length: 36},
				rockRidgeTimestamps{longForm: false, stamps: []rockRidgeTimestamp{
					{timestampType: rockRidgeTimestampModify, time: bytesToTime([]byte{0x76, 0x9, 0x1b, 0xb, 0x2f, 0x1e, 0x0})},
					{timestampType: rockRidgeTimestampAccess, time: bytesToTime([]byte{0x76, 0x9, 0x1b, 0xb, 0x2f, 0x29, 0x0})},
					{timestampType: rockRidgeTimestampAttribute, time: bytesToTime([]byte{0x76, 0x9, 0x1b, 0xb, 0x2f, 0x1e, 0x0})},
				},
				},
				rockRidgeName{name: "abc"},
			},
		},
		&inode{
			extAttrSize:    0,
			extents:        toExtents(0x15, 0x800, blocksize),
			ctime:          t1,
			flags:          isSubdirectory,
			volumeSequence: 1,
			fname:          "BAR",
			fs:             f,
			extensions: []directoryEntrySystemUseExtension{
				rockRidgePosixAttributes{mode: 0755 | os.ModeDir, linkCount: 1, uid: 0, gid: 0, length: 36},
				rockRidgeTimestamps{longForm: false, stamps: []rockRidgeTimestamp{
					{timestampType: rockRidgeTimestampModify, time: bytesToTime([]byte{0x76, 0x9, 0x1b, 0xb, 0x2f, 0x1a, 0x0})},
					{timestampType: rockRidgeTimestampAccess, time: bytesToTime([]byte{0x76, 0x9, 0x1b, 0xb, 0x2f, 0x29, 0x0})},
					{timestampType: rockRidgeTimestampAttribute, time: bytesToTime([]byte{0x76, 0x9, 0x1b, 0xb, 0x2f, 0x1a, 0x0})},
				},
				},
				rockRidgeName{name: "bar"},
			},
		},
		&inode{
			extAttrSize:    0x0,
			extents:        toExtents(0x16, 0x800, blocksize),
			ctime:          t1,
			flags:          isSubdirectory,
			volumeSequence: 0x1,
			fs:             f,
			fname:          "DEEP",
			extensions: []directoryEntrySystemUseExtension{
				rockRidgePosixAttributes{mode: 0755 | os.ModeDir, linkCount: 1, uid: 0, gid: 0, length: 36},
				rockRidgeTimestamps{longForm: false, stamps: []rockRidgeTimestamp{
					{timestampType: rockRidgeTimestampModify, time: bytesToTime([]byte{0x76, 0x0A, 0x13, 0x08, 0x0D, 0x32, 0x00})},
					{timestampType: rockRidgeTimestampAccess, time: bytesToTime([]byte{0x76, 0x0A, 0x13, 0x08, 0x0D, 0x32, 0x00})},
					{timestampType: rockRidgeTimestampAttribute, time: bytesToTime([]byte{0x76, 0x0A, 0x13, 0x08, 0x0D, 0x32, 0x00})},
				}},
				rockRidgeName{name: "deep"},
			},
		},
		&inode{
			extAttrSize:    0,
			extents:        toExtents(0x1d, 0x2800, blocksize),
			ctime:          t1,
			flags:          isSubdirectory,
			volumeSequence: 1,
			fname:          "FOO",
			fs:             f,
			extensions: []directoryEntrySystemUseExtension{
				rockRidgePosixAttributes{mode: 0755 | os.ModeDir, linkCount: 1, uid: 0, gid: 0, length: 36},
				rockRidgeTimestamps{longForm: false, stamps: []rockRidgeTimestamp{
					{timestampType: rockRidgeTimestampModify, time: bytesToTime([]byte{0x76, 0x9, 0x1b, 0xb, 0x2f, 0x28, 0x0})},
					{timestampType: rockRidgeTimestampAccess, time: bytesToTime([]byte{0x76, 0x9, 0x1b, 0xb, 0x2f, 0x29, 0x0})},
					{timestampType: rockRidgeTimestampAttribute, time: bytesToTime([]byte{0x76, 0x9, 0x1b, 0xb, 0x2f, 0x28, 0x0})},
				},
				},
				rockRidgeName{name: "foo"},
			},
		},
		&inode{
			extAttrSize:    0,
			extents:        toExtents(0x29, 0x0, blocksize),
			ctime:          t1,
			volumeSequence: 1,
			fname:          "LINK.;1",
			fs:             f,
			extensions: []directoryEntrySystemUseExtension{
				rockRidgePosixAttributes{mode: 0777 | os.ModeSymlink, linkCount: 1, uid: 0, gid: 0, length: 36},
				rockRidgeTimestamps{longForm: false, stamps: []rockRidgeTimestamp{
					{timestampType: rockRidgeTimestampModify, time: bytesToTime([]byte{0x76, 0xa, 0x13, 0x8, 0xd, 0x32, 0x0})},
					{timestampType: rockRidgeTimestampAccess, time: bytesToTime([]byte{0x76, 0xa, 0x13, 0x8, 0xd, 0x32, 0x0})},
					{timestampType: rockRidgeTimestampAttribute, time: bytesToTime([]byte{0x76, 0xa, 0x13, 0x8, 0xd, 0x32, 0x0})},
				},
				},
				rockRidgeName{name: "link"},
				rockRidgeSymlink{name: "a/b/c/d/ef/g/h"},
			},
		},
		&inode{
			extAttrSize:    0,
			extents:        toExtents(0x1476, 0x7, blocksize),
			ctime:          t1,
			volumeSequence: 1,
			fname:          "README.MD;1",
			fs:             f,
			extensions: []directoryEntrySystemUseExtension{
				rockRidgePosixAttributes{mode: 0644, linkCount: 1, uid: 0, gid: 0, length: 36},
				rockRidgeTimestamps{longForm: false, stamps: []rockRidgeTimestamp{
					{timestampType: rockRidgeTimestampModify, time: bytesToTime([]byte{0x76, 0xa, 0x13, 0x8, 0xd, 0x32, 0x0})},
					{timestampType: rockRidgeTimestampAccess, time: bytesToTime([]byte{0x76, 0xa, 0x13, 0x8, 0xd, 0x33, 0x0})},
					{timestampType: rockRidgeTimestampAttribute, time: bytesToTime([]byte{0x76, 0xa, 0x13, 0x8, 0xd, 0x32, 0x0})},
				},
				},
				rockRidgeName{name: "README.md"},
			},
		},
	}
	if includeRelocated {
		entries = append(entries,
			&inode{
				extAttrSize:    0,
				extents:        toExtents(0x22, 0x800, blocksize),
				ctime:          t1,
				flags:          isSubdirectory,
				volumeSequence: 1,
				fname:          "G",
				fs:             f,
				extensions: []directoryEntrySystemUseExtension{
					rockRidgePosixAttributes{mode: 0755 | os.ModeDir, linkCount: 1, uid: 0, gid: 0, length: 36},
					rockRidgeTimestamps{longForm: false, stamps: []rockRidgeTimestamp{
						{timestampType: rockRidgeTimestampModify, time: bytesToTime([]byte{0x76, 0xa, 0x13, 0x8, 0xd, 0x32, 0x0})},
						{timestampType: rockRidgeTimestampAccess, time: bytesToTime([]byte{0x76, 0xa, 0x13, 0x8, 0xd, 0x32, 0x0})},
						{timestampType: rockRidgeTimestampAttribute, time: bytesToTime([]byte{0x76, 0xa, 0x13, 0x8, 0xd, 0x32, 0x0})},
					},
					},
					rockRidgeRelocatedDirectory{},
					rockRidgeName{name: "g"},
				},
			})
	}

	// b is a slice of byte slices; each one represents the raw directory entry bytes for a inode
	//  parallels the directory Entries above
	// each entry is a slice of slices: the first is the raw data for the entry
	//  the second and any subsequent are continuation areas
	b := make([][][]byte, 0, 8)
	read := int64(0)
	for range entries {
		recordSize := int64(allBytes[int(read)])
		// do we have a 0 point? if so, move ahead until we pass it at the end of the block
		if recordSize == 0x00 {
			read += (blocksize - read%blocksize)
		}
		b2 := make([][]byte, 0)
		b2 = append(b2, allBytes[read:read+recordSize])
		b = append(b, b2)
		read += recordSize
	}
	// the first one has a continuation entry
	b[0] = append(b[0], ceBytes)

	byteMap := map[int64][]byte{
		rootSector: allBytes, ceSector: ceBytes,
	}
	sort.Slice(entries[2:], func(i, j int) bool {
		return entries[i+2].Name() < entries[j+2].Name()
	})
	return entries, b, allBytes, byteMap, nil
}

func getValidDirectoryEntriesExtended(fs *isoFs) ([]*inode, [][]byte, []byte, error) {
	// these are taken from the file ./testdata/9660.iso, see ./testdata/README.md
	blocksize := 2048
	fooSector := 0x21
	t1, _ := time.Parse(time.RFC3339, "2017-11-26T07:53:16Z")
	sizes := []int{0x34, 0x34, 0x2c}
	entries := []*inode{
		{extAttrSize: 0x0, extents: toExtents(0x21, 0x1000, 2048), ctime: t1, flags: isSubdirectory | isSelf, volumeSequence: 0x1, fname: ""},
		{extAttrSize: 0x0, extents: toExtents(0x12, 0x800, 2048), ctime: t1, flags: isSubdirectory | isParent, volumeSequence: 0x1, fname: ""},
		{extAttrSize: 0x0, extents: toExtents(0x1427, 0xb, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA00.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1428, 0xb, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA01.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1429, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA02.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x142a, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA03.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x142b, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA04.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x142c, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA05.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x142d, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA06.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x142e, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA07.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x142f, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA08.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1430, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA09.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1431, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA10.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1432, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA11.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1433, 0xb, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA12.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1434, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA13.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1435, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA14.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1436, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA15.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1437, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA16.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1438, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA17.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1439, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA18.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x143a, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA19.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x143b, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA20.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x143c, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA21.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x143d, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA22.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x143e, 0xb, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA23.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x143f, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA24.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1440, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA25.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1441, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA26.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1442, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA27.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1443, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA28.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1444, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA29.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1445, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA30.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1446, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA31.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1447, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA32.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1448, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA33.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1449, 0xb, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA34.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x144a, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA35.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x144b, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA36.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x144c, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA37.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x144d, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA38.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x144e, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA39.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x144f, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA40.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1450, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA41.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1451, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA42.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1452, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA43.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1453, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA44.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1454, 0xb, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA45.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1455, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA46.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1456, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA47.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1457, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA48.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1458, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA49.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1459, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA50.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x145a, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA51.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x145b, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA52.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x145c, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA53.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x145d, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA54.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x145e, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA55.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x145f, 0xb, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA56.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1460, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA57.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1461, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA58.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1462, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA59.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1463, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA60.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1464, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA61.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1465, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA62.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1466, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA63.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1467, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA64.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1468, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA65.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1469, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA66.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x146a, 0xb, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA67.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x146b, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA68.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x146c, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA69.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x146d, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA70.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x146e, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA71.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x146f, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA72.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1470, 0xc, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA73.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1471, 0xb, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA74.;1"},
		{extAttrSize: 0x0, extents: toExtents(0x1472, 0xb, 2048), ctime: t1, volumeSequence: 0x1, fname: "FILENA75.;1"},
	}

	for _, e := range entries {
		e.fs = fs
	}
	// read correct bytes off of disk
	input, err := ioutil.ReadFile(ISO9660File)
	if err != nil {
		return nil, nil, nil, fmt.Errorf("Error reading data from iso9660 test fixture %s: %v", ISO9660File, err)
	}

	start := fooSector * blocksize // start of /foo directory in file.iso

	// five blocks, since we know it is five blocks
	allBytes := input[start : start+2*blocksize]
	b := make([][]byte, 0, len(entries))
	read := 0
	for i := range entries {
		var recordSize int
		if i < len(sizes) {
			recordSize = sizes[1]
		} else {
			recordSize = sizes[len(sizes)-1]
		}
		// do we have a 0 point? if so, move ahead until we pass it at the end of the block
		if allBytes[read] == 0x00 {
			read += (blocksize - read%blocksize)
		}
		b = append(b, allBytes[read:read+recordSize])
		read += recordSize
	}
	return entries, b, allBytes, nil
}

func TestBytesToTime(t *testing.T) {
	for _, tt := range timeBytesTests {
		output := bytesToTime(tt.b)
		expected, err := time.Parse(time.RFC3339, tt.rfc)
		if err != nil {
			t.Fatalf("Error parsing expected date: %v", err)
		}
		if !expected.Equal(output) {
			t.Errorf("bytesToTime(%d) expected output %v, actual %v", tt.b, expected, output)
		}
	}
}
