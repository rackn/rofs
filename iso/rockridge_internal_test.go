package iso

import (
	"fmt"
	"sort"
	"testing"
)

func TestRockRidgeID(t *testing.T) {
	id := "abc"
	rr := &rockRidgeExtension{id: id}
	if rr.ID() != id {
		t.Errorf("Mismatched signature, actual '%s' expected '%s'", rr.ID(), id)
	}
}

func TestRockRidgeGetFilename(t *testing.T) {
	tests := []struct {
		dirEntry *inode
		filename string
		err      error
	}{
		{&inode{fname: "ABC"}, "", fmt.Errorf("Could not find Rock Ridge fname property")},
		{&inode{fname: "ABC", extensions: []directoryEntrySystemUseExtension{rockRidgeName{name: "abc"}}}, "abc", nil},
	}
	rr := &rockRidgeExtension{}
	for _, tt := range tests {
		name, err := rr.GetFilename(tt.dirEntry)
		if (err != nil && tt.err == nil) || (err == nil && tt.err != nil) {
			t.Errorf("Mismatched errors, actual then expected")
			t.Log(err)
			t.Log(tt.err)
		} else if name != tt.filename {
			t.Errorf("Mismatched fname actual %s expected %s", name, tt.filename)
		}
	}
}

func TestRockRidgeRelocated(t *testing.T) {
	tests := []struct {
		dirEntry  *inode
		relocated bool
	}{
		{&inode{fname: "ABC"}, false},
		{&inode{fname: "ABC", extensions: []directoryEntrySystemUseExtension{rockRidgeRelocatedDirectory{}}}, true},
	}
	rr := &rockRidgeExtension{}
	for _, tt := range tests {
		reloc := rr.Relocated(tt.dirEntry)
		if reloc != tt.relocated {
			t.Errorf("Mismatched relocated actual %v expected %v", reloc, tt.relocated)
		}
	}
}

func TestRockRidgeSymlinkMerge(t *testing.T) {
	tests := []struct {
		first        rockRidgeSymlink
		continuation []directoryEntrySystemUseExtension
		result       rockRidgeSymlink
	}{
		{rockRidgeSymlink{name: "/a/b", continued: true}, []directoryEntrySystemUseExtension{rockRidgeSymlink{name: "/c/d", continued: true}, rockRidgeSymlink{name: "/e/f", continued: false}}, rockRidgeSymlink{name: "/a/b/c/d/e/f", continued: false}},
		{rockRidgeSymlink{name: "/a/b", continued: true}, []directoryEntrySystemUseExtension{rockRidgeSymlink{name: "/c/d", continued: false}}, rockRidgeSymlink{name: "/a/b/c/d", continued: false}},
		{rockRidgeSymlink{name: "/a/b", continued: false}, nil, rockRidgeSymlink{name: "/a/b", continued: false}},
	}
	for _, tt := range tests {
		symlink := tt.first.Merge(tt.continuation)
		if symlink != tt.result {
			t.Errorf("Mismatched merge result actual %v expected %v", symlink, tt.result)
		}
	}
}

func TestRockRidgeNameMerge(t *testing.T) {
	tests := []struct {
		first        rockRidgeName
		continuation []directoryEntrySystemUseExtension
		result       rockRidgeName
	}{
		{rockRidgeName{name: "/a/b", continued: true}, []directoryEntrySystemUseExtension{rockRidgeName{name: "/c/d", continued: true}, rockRidgeName{name: "/e/f", continued: false}}, rockRidgeName{name: "/a/b/c/d/e/f", continued: false}},
		{rockRidgeName{name: "/a/b", continued: true}, []directoryEntrySystemUseExtension{rockRidgeName{name: "/c/d", continued: false}}, rockRidgeName{name: "/a/b/c/d", continued: false}},
		{rockRidgeName{name: "/a/b", continued: false}, nil, rockRidgeName{name: "/a/b", continued: false}},
	}
	for _, tt := range tests {
		name := tt.first.Merge(tt.continuation)
		if name != tt.result {
			t.Errorf("Mismatched merge result actual %v expected %v", name, tt.result)
		}
	}
}

func TestRockRidgeSortTimestamp(t *testing.T) {
	// these are ust sorted randomly
	tests := []rockRidgeTimestamp{
		{timestampType: rockRidgeTimestampExpiration},
		{timestampType: rockRidgeTimestampModify},
		{timestampType: rockRidgeTimestampEffective},
		{timestampType: rockRidgeTimestampAttribute},
		{timestampType: rockRidgeTimestampCreation},
		{timestampType: rockRidgeTimestampAccess},
		{timestampType: rockRidgeTimestampBackup},
	}
	expected := []uint8{rockRidgeTimestampCreation, rockRidgeTimestampModify, rockRidgeTimestampAccess,
		rockRidgeTimestampAttribute, rockRidgeTimestampBackup, rockRidgeTimestampExpiration, rockRidgeTimestampEffective}
	sort.Sort(rockRidgeTimestampByBitOrder(tests))
	for i, e := range tests {
		if e.timestampType != expected[i] {
			t.Errorf("At position %d, got %v instead of %v", i, e.timestampType, expected[i])
		}
	}
}
