package iso

/*
 These tests the exported functions
 We want to do full-in tests with files
*/

import (
	"bytes"
	"fmt"
	"gitlab.com/rackn/rofs/c"
	"io"
	"io/fs"
	"io/ioutil"
	"os"
	"strings"
	"syscall"
	"testing"
)

func getValidIso9660FSReadOnly() (c.Filesystem, error) {
	var f c.Src
	f, err := os.Open(ISO9660File)
	if err != nil {
		return c.Filesystem{}, fmt.Errorf("Failed to read iso9660 testfile %s: %v", ISO9660File, err)
	}
	f = c.Mmap(f)
	return Open(f)
}
func getValidRockRidgeFSReadOnly() (c.Filesystem, error) {
	var f c.Src
	f, err := os.Open(RockRidgeFile)
	if err != nil {
		return c.Filesystem{}, fmt.Errorf("Failed to read iso9660 testfile %s: %v", RockRidgeFile, err)
	}
	f = c.Mmap(f)
	return Open(f)
}

func TestIso9660ReadDir(t *testing.T) {
	type testList struct {
		fs    c.Filesystem
		path  string
		count int
		first string
		last  string
		err   error
	}
	runTests := func(t *testing.T, tests []testList) {
		for _, tt := range tests {
			var fi []fs.DirEntry
			tgt, err := tt.fs.Lookup(tt.path, true)
			if err == nil {
				fi, err = tgt.ReadDir()
			}
			switch {
			case (err == nil && tt.err != nil) || (err != nil && tt.err == nil):
				t.Errorf("fs.ReadDir(%s): mismatched errors, actual %v expected %v", tt.path, err, tt.err)
			case len(fi) != tt.count:
				t.Errorf("fs.ReadDir(%s): mismatched directory received %d entries, expected %d", tt.path, len(fi), tt.count)
			case fi != nil && tt.count > 2 && fi[0].Name() != tt.first:
				t.Errorf("fs.ReadDir(%s): mismatched first non-self or parent entry, actual then expected", tt.path)
				t.Logf("%s", fi[0].Name())
				t.Logf("%s", tt.first)
			case fi != nil && tt.count > 0 && fi[len(fi)-1].Name() != tt.last:
				t.Errorf("fs.ReadDir(%s): mismatched last entry, actual then expected", tt.path)
				t.Logf("%s", fi[len(fi)-1].Name())
				t.Logf("%s", tt.last)
			}
		}
	}
	t.Run("read-only 9660", func(t *testing.T) {
		iso, err := getValidIso9660FSReadOnly()
		if err != nil {
			t.Errorf("Failed to get read-only ISO9660 fs: %v", err)
		}
		runTests(t, []testList{
			{iso, "abcdef", 0, "", "", fmt.Errorf("directory does not exist")}, // does not exist
			// root should have 4 entries (since we do not pass back . and ..):
			// .
			// ..
			// /ABC
			// /BAR
			// /FOO
			// /README.MD;1
			{iso, ".", 5, "abc", "readme.md", nil}, // exists
			{iso, "abc", 1, "", "largefil", nil},   // exists
		},
		)
	})
	t.Run("read-only rock ridge", func(t *testing.T) {
		iso, err := getValidRockRidgeFSReadOnly()
		if err != nil {
			t.Errorf("Failed to get read-only Rock Ridge fs: %v", err)
		}
		runTests(t, []testList{
			{iso, "abcdef", 0, "", "", fmt.Errorf("directory does not exist")}, // does not exist
			// root should have 4 entries (since we do not pass back . and ..):
			{iso, ".", 6, "README.md", "link", nil},                                 // exists
			{iso, "ABC", 0, "", "LARGEFIL", fmt.Errorf("directory does not exist")}, // should not find 8.3 name
			{iso, "abc", 1, "", "largefile", nil},                                   // should find rock ridge name
			{iso, "deep/a/b/c/d/e/f/g/h/i/j/k", 1, "file", "file", nil},             // should find a deep directory
			{iso, "G", 0, "", "H", fmt.Errorf("directory does not exist")},          // relocated directory
			{iso, "g", 0, "", "h", fmt.Errorf("directory does not exist")},          // relocated directory
		},
		)
	})
}

func TestIso9660OpenFile(t *testing.T) {
	// opening directories and files for reading
	type testStruct struct {
		path     string
		expected string
		err      error
	}

	t.Run("Read", func(t *testing.T) {
		runTests := func(t *testing.T, fs c.Filesystem, tests []testStruct) {
			for _, tt := range tests {
				header := fmt.Sprintf("open(%s)", tt.path)
				var b []byte
				reader, err := fs.Open(tt.path)
				if err == nil {
					b, err = ioutil.ReadAll(reader)
				}
				if err == nil {
					reader.Close()
					reader, err = fs.Open(tt.path)
				}
				b2 := &bytes.Buffer{}
				if err == nil {
					_, err = io.Copy(b2, reader)
				}
				switch {
				case (err == nil && tt.err != nil) || (err != nil && tt.err == nil) || (err != nil && tt.err != nil && !strings.HasPrefix(err.Error(), tt.err.Error())):
					t.Errorf("%s: mismatched errors, actual: %v , expected: %v", header, err, tt.err)
				case reader == nil && (tt.err == nil || tt.expected != ""):
					t.Errorf("%s: Unexpected nil output", header)
				case reader != nil:
					if err != nil && tt.err == nil {
						t.Errorf("%s: ioutil.ReadAll(reader) unexpected error: %v", header, err)
					}
					if string(b) != tt.expected {
						t.Errorf("%s: mismatched contents, actual then expected", header)
						t.Log(string(b))
						t.Log(tt.expected)
					}
					if b2.String() != tt.expected {
						t.Errorf("%s: mismatched contents, actual then expected", header)
						t.Log(b2.String())
						t.Log(tt.expected)
					}
				}
			}
		}
		t.Run("read-only 9660", func(t *testing.T) {
			isoFs, err := getValidIso9660FSReadOnly()
			if err != nil {
				t.Errorf("Failed to get read-only ISO9660 fs: %v", err)
			}
			tests := []testStruct{
				// error opening a directory
				{".", "", syscall.EISDIR},
				// open non-existent file for read or read write
				{"abcdefg", "", fmt.Errorf("open %s: file does not exist", "abcdefg")},
				// open file for read or read write and check contents
				{"foo/filena01", "filename_1\n", nil},
				{"foo/filena75", "filename_9\n", nil},
				// rock ridge versions should not exist
				{"readme.md", "README\n", nil},
			}
			runTests(t, isoFs, tests)
		})
		t.Run("read-only rock ridge", func(t *testing.T) {
			iso, err := getValidRockRidgeFSReadOnly()
			if err != nil {
				t.Errorf("Failed to get read-only Rock Ridge fs: %v", err)
			}
			tests := []testStruct{
				// error opening a directory
				{".", "", syscall.EISDIR},
				// open non-existent file for read or read write
				{"abcdefg", "", fmt.Errorf("open %s: file does not exist", "abcdefg")},
				// open file for read or read write and check contents
				{"foo/filename_1", "filename_1\n", nil},
				{"foo/filename_75", "filename_75\n", nil},
				// only rock ridge versions should exist
				{"readme.md", "", fmt.Errorf("open %s: file does not exist", "readme.md")},
				{"README.md", "README\n", nil},
			}
			runTests(t, iso, tests)
		})
	})
}
