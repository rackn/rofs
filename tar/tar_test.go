package tar

import (
	t2 "archive/tar"
	"bytes"
	"gitlab.com/rackn/rofs/c"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"reflect"
	"testing"
	"time"
)

func TestTarOpen(t *testing.T) {
	tars, err := filepath.Glob("testdata/*.tar")
	if err != nil {
		t.Fatalf("Error getting tar list: %v", err)
	}
	expectErrs := map[string]error{
		"testdata/issue10968.tar":         ErrHeader,
		"testdata/issue11169.tar":         ErrHeader,
		"testdata/issue12435.tar":         ErrHeader,
		"testdata/neg-size.tar":           ErrHeader,
		"testdata/pax-bad-hdr-file.tar":   ErrHeader,
		"testdata/pax-bad-mtime-file.tar": ErrHeader,
		"testdata/pax-nul-path.tar":       ErrHeader,
		"testdata/pax-nul-xattrs.tar":     ErrHeader,
		"testdata/writer-big-long.tar":    io.ErrUnexpectedEOF,
		"testdata/writer-big.tar":         io.ErrUnexpectedEOF,
	}
	for _, i := range tars {
		fi, err := os.Open(i)
		if err != nil {
			t.Fatalf("Error opening %s: %v", i, err)
		}
		f2 := c.Mmap(fi)
		_, err = Open(f2)
		if err != nil {
			if expect := expectErrs[i]; expect == nil {
				t.Errorf("Error opening tar %s: %v", i, err)
			} else if err != expect {
				t.Errorf("Expected err %v opening %s, not %v", expect, i, err)
			} else {
				t.Logf("Got expected error opening %s", i)
			}
			continue
		}
		if expect := expectErrs[i]; expect != nil {
			t.Errorf("No error opening %s, but expected %v", i, expect)
		} else {
			t.Logf("Opened tar %s", i)
		}
	}
}

type toAdd struct {
	hdr  t2.Header
	data []byte
}

func TestTarRead(t *testing.T) {
	tarBuf := &bytes.Buffer{}
	tw := t2.NewWriter(tarBuf)
	mtime := time.Date(2022, 1, 1, 1, 1, 1, 1, time.UTC)
	data := "some stuff"
	contents := []toAdd{
		{
			hdr: t2.Header{
				Typeflag:   TypeReg,
				Name:       "foo/a/b/c/d/stuff.txt",
				Linkname:   "",
				Size:       int64(len(data)),
				Mode:       0644,
				Uid:        0,
				Gid:        0,
				Uname:      "",
				Gname:      "",
				ModTime:    mtime,
				AccessTime: mtime,
				ChangeTime: mtime,
			},
			data: []byte(data),
		},
		{
			hdr: t2.Header{
				Typeflag: TypeLink,
				Name:     "bar/stuff.txt",
				Linkname: "foo/a/b/c/d/stuff.txt",
			},
		},
		{
			hdr: t2.Header{
				Typeflag:   TypeSymlink,
				Name:       "stuff.lnk",
				Linkname:   "bar/stuff.txt",
				Mode:       0644,
				ModTime:    mtime,
				AccessTime: mtime,
				ChangeTime: mtime,
			},
		},
		{
			hdr: t2.Header{
				Typeflag:   TypeDir,
				Name:       "foo/b",
				Mode:       0755,
				ModTime:    mtime,
				AccessTime: mtime,
				ChangeTime: mtime,
			},
		},
		{
			hdr: t2.Header{
				Typeflag:   TypeReg,
				Name:       "foo/b/stuff.txt",
				Linkname:   "",
				Size:       int64(len(data)),
				Mode:       0644,
				ModTime:    mtime,
				AccessTime: mtime,
				ChangeTime: mtime,
			},
			data: []byte(data),
		},
	}
	for _, v := range contents {
		if err := tw.WriteHeader(&v.hdr); err != nil {
			t.Fatalf("Error writing header %s: %v", v.hdr.Name, err)
		}
		if v.data != nil {
			if _, err := tw.Write(v.data); err != nil {
				t.Fatalf("Error writing data for %s: %v", v.hdr.Name, err)
			}
		}
		if err := tw.Flush(); err != nil {
			t.Fatalf("Error flushing entry for %s: %v", v.hdr.Name, err)
		}
	}
	if err := tw.Close(); err != nil {
		t.Fatalf("Error closing: %v", err)
	}
	tarSrc := bytes.NewReader(tarBuf.Bytes())
	tfs, err := Open(tarSrc)
	if err != nil {
		t.Fatalf("Error opening tar in fs: %v", err)
	}
	var ents []string
	expectEnts := []string{".",
		"bar", "bar/stuff.txt",
		"foo",
		"foo/a", "foo/a/b", "foo/a/b/c", "foo/a/b/c/d", "foo/a/b/c/d/stuff.txt",
		"foo/b",
		"foo/b/stuff.txt",
		"stuff.lnk",
	}
	err = fs.WalkDir(tfs, ".", func(path string, ent fs.DirEntry, e2 error) error {
		if e2 != nil {
			return e2
		}
		ents = append(ents, path)
		return nil
	})
	if err != nil {
		t.Fatalf("Error walking: %v", err)
	}
	if !reflect.DeepEqual(ents, expectEnts) {
		t.Fatalf("Did not get expected entries")
	}
}
