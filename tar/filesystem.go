package tar

import (
	"fmt"
	"gitlab.com/rackn/rofs/c"
	"gitlab.com/rackn/seekable-zstd"
	"io"
	"io/fs"
	"path"
	"strings"
	"time"
)

// tarFS provides access to the contents of an uncompressed tar archive as
// an fs.tarFS.  Only tar files that do not contain sparse files are supported.
type tarFS struct {
	r    io.ReadSeeker
	root *inode
	pad  int64
}

func (fs *tarFS) Close() error {
	if cl, ok := fs.r.(io.Closer); ok {
		return cl.Close()
	}
	return nil
}

func (fs *tarFS) Root() c.Inode {
	return fs.root
}

func (tf *tarFS) insert(ino *inode, ra io.ReaderAt) {
	parts := strings.Split(ino.name, "/")
	parent, offset := tf.root.lookup(parts[:len(parts)-1], 0)
	for offset < len(parts)-1 {
		child := &inode{
			name:     path.Join(parts[:offset+1]...),
			fi:       ra,
			mode:     int64(0755 | fs.ModeDir),
			typeflag: TypeDir,
			mtime:    time.Now(),
			format:   FormatUnknown,
		}
		parent.insert(child)
		parent = child
		offset++
	}
	if child, _ := parent.lookup(parts, offset); child != parent {
		if child.typeflag == ino.typeflag && child.typeflag == TypeDir {
			ino.subs = child.subs
		}
	}
	parent.insert(ino)
}

// Open creates a new tarFS reading from r.
func Open(r c.Src) (res c.Filesystem, err error) {
	var sz int64
	if sz, err = r.Seek(0, io.SeekEnd); err != nil {
		return
	}
	if _, err = r.Seek(0, io.SeekStart); err != nil {
		return
	}
	defer r.Seek(0, io.SeekStart)
	ra := r.(io.ReaderAt)
	rdr := r.(io.ReadSeeker)
	if zs, zsErr := seekable.NewDecoder(r, sz, c.Zstd); zsErr == nil {
		rdr = zs.ReadSeeker()
		ra = zs
		sz = zs.Size()
	}
	tfs := &tarFS{
		r: rdr,
		root: &inode{
			fi:       ra,
			name:     ".",
			mode:     int64(0755 | fs.ModeDir),
			typeflag: TypeDir,
			mtime:    time.Now(),
			format:   FormatUnknown,
		},
	}
	var blk block
	curr := &regFileReader{rdr, 0}
	res.InodeProvider = tfs
	for {
		var ino *inode
		ino, err = tfs.next(&blk, curr)
		if err == io.EOF {
			break
		}
		if err != nil {
			return
		}
		switch ino.typeflag {
		case TypeChar, TypeBlock, TypeFifo, TypeXGlobalHeader:
			// hahaha, nope.
			continue
		case TypeReg, TypeGNUSparse:
			ino.fi = ra
			ino.offset, _ = rdr.Seek(0, io.SeekCurrent)
			if ino.offset+ino.physicalSize() > sz {
				err = io.ErrUnexpectedEOF
				return
			}
		case TypeLink:
			// This is a hard link.  The file to which it refers should already exist.
			ino.linkname = normalizeName(ino.linkname)
			if ri, re := res.Lookup(ino.linkname, true); !(re == nil && ri.Mode().IsRegular()) {
				err = io.ErrUnexpectedEOF
				return
			} else {
				old := ri.(*inode)
				ino.fi = old.fi
				ino.size = old.size
				ino.offset = old.offset
				ino.mode = old.mode
				ino.mtime = old.mtime
				ino.typeflag = TypeReg
				ino.linkname = ""
				ino.spds = old.spds
			}
		case TypeSymlink:
			ino.mode = ino.mode | int64(fs.ModeSymlink)
		case 'D', TypeDir:
			ino.typeflag = TypeDir
			ino.mode = ino.mode | int64(fs.ModeDir)
		default:
			err = fmt.Errorf("Cannot handle inode type %c in %s", ino.typeflag, ino.name)
			return
		}
		ino.name = normalizeName(ino.name)
		tfs.insert(ino, ra)
	}
	err = nil
	return
}

func (tf *tarFS) next(rawHdr *block, curr *regFileReader) (*inode, error) {
	var paxHdrs map[string]string
	var gnuLongName, gnuLongLink string

	// Externally, Next iterates through the tar archive as if it is a series of
	// files. Internally, the tar hdrFormat often uses fake "files" to add meta
	// data that describes the next file. These meta data "files" should not
	// normally be visible to the outside. As such, this loop iterates through
	// one or more "header files" until it finds a "normal file".
	hdrFormat := FormatUSTAR | FormatPAX | FormatGNU
	for {
		// Discard the remainder of the file and any padding.
		if _, err := tf.r.Seek(curr.physicalRemaining()+tf.pad, io.SeekCurrent); err != nil {
			return nil, err
		}
		tf.pad = 0

		hdr, err := tf.readHeader(rawHdr)
		if err != nil {
			return nil, err
		}
		if err := tf.handleRegularFile(hdr, curr); err != nil {
			return nil, err
		}
		hdrFormat.mayOnlyBe(hdr.format)

		// Check for PAX/GNU special headers and files.
		switch hdr.typeflag {
		case TypeXHeader, TypeXGlobalHeader:
			hdrFormat.mayOnlyBe(FormatPAX)
			paxHdrs, err = parsePAX(curr)
			if err != nil {
				return nil, err
			}
			if hdr.typeflag == TypeXGlobalHeader {
				mergePAX(hdr, paxHdrs)
				return &inode{
					name:       hdr.name,
					typeflag:   hdr.typeflag,
					xattrs:     hdr.xattrs,
					paxrecords: hdr.paxrecords,
					format:     hdrFormat,
				}, nil
			}
			continue // This is a meta header affecting the next header
		case TypeGNULongName, TypeGNULongLink:
			hdrFormat.mayOnlyBe(FormatGNU)
			realname, err := io.ReadAll(curr)
			if err != nil {
				return nil, err
			}

			var p parser
			switch hdr.typeflag {
			case TypeGNULongName:
				gnuLongName = p.parseString(realname)
			case TypeGNULongLink:
				gnuLongLink = p.parseString(realname)
			}
			continue // This is a meta header affecting the next header
		default:
			// The old GNU sparse hdrFormat is handled here since it is technically
			// just a regular file with additional attributes.

			if err := mergePAX(hdr, paxHdrs); err != nil {
				return nil, err
			}
			if gnuLongName != "" {
				hdr.name = gnuLongName
			}
			if gnuLongLink != "" {
				hdr.linkname = gnuLongLink
			}
			if hdr.typeflag == TypeRegA {
				if strings.HasSuffix(hdr.name, "/") {
					hdr.typeflag = TypeDir // Legacy archives use trailing slash for directories
				} else {
					hdr.typeflag = TypeReg
				}
			}

			// The extended headers may have updated the size.
			// Thus, setup the regFileReader again after merging PAX headers.
			if err := tf.handleRegularFile(hdr, curr); err != nil {
				return nil, err
			}

			// Sparse formats rely on being able to read from the logical data
			// section; there must be a preceding call to handleRegularFile.
			if err := tf.handleSparseFile(hdr, rawHdr, curr); err != nil {
				return nil, err
			}

			// Set the final guess at the hdrFormat.
			if hdrFormat.has(FormatUSTAR) && hdrFormat.has(FormatPAX) {
				hdrFormat.mayOnlyBe(FormatUSTAR)
			}
			hdr.format = hdrFormat
			return hdr, nil // This is a file, so stop
		}
	}
}
