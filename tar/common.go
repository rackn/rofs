// Copyright 2022, Rackn, Inc.
//
// Copyright 2009 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package tar implements c.InodeProvider for uncompressed tarfiles.
//
// Tape archives (tar) are a file format for storing a sequence of files that
// can be read and written in a streaming manner.
// This package aims to cover most variations of the format,
// including those produced by GNU and BSD tar tools.  Tar files
// with sparse entries are not supported yet.
package tar

import (
	"errors"
	"gitlab.com/rackn/rofs/c"
	"golang.org/x/exp/constraints"
	"io"
	"io/fs"
	"math"
	"path"
	"sort"
	"strings"
	"syscall"
	"time"
)

// BUG: Use of the uid and gid fields in inode could overflow on 32-bit
// architectures. If a large value is encountered when decoding, the result
// stored in inode will be the truncated version.

var (
	ErrHeader               = errors.New("tarfs: invalid tar header")
	ErrFieldTooLong         = errors.New("tarfs: header field too long")
	ErrSparseNotImplemented = errors.New("tarfs: cannot handle sparse files")
	errMissData             = errors.New("tarfs: sparse file references non-existent data")
	errUnrefData            = errors.New("tarfs: sparse file contains unreferenced data")
)

// Type flags for inode.typeflag.
const (
	// Type '0' indicates a regular file.
	TypeReg  = '0'
	TypeRegA = '\x00' // Deprecated: Use TypeReg instead.

	// Type '1' to '6' are header-only flags and may not have a data body.
	TypeLink    = '1' // Hard link
	TypeSymlink = '2' // Symbolic link
	TypeChar    = '3' // Character device node
	TypeBlock   = '4' // Block device node
	TypeDir     = '5' // Directory
	TypeFifo    = '6' // FIFO node

	// Type '7' is reserved.
	TypeCont = '7'

	// Type 'x' is used by the PAX format to store key-value records that
	// are only relevant to the next file.
	// This package transparently handles these types.
	TypeXHeader = 'x'

	// Type 'g' is used by the PAX format to store key-value records that
	// are relevant to all subsequent files.
	// This package only supports parsing and composing such headers,
	// but does not currently support persisting the global state across files.
	TypeXGlobalHeader = 'g'

	// Type 'S' indicates a sparse file in the GNU format.
	TypeGNUSparse = 'S'

	// Types 'L' and 'K' are used by the GNU format for a meta file
	// used to store the path or link name for the next file.
	// This package transparently handles these types.
	TypeGNULongName = 'L'
	TypeGNULongLink = 'K'
)

// Keywords for PAX extended header records.
const (
	paxNone     = "" // Indicates that no PAX key is suitable
	paxPath     = "path"
	paxLinkpath = "linkpath"
	paxSize     = "size"
	paxUid      = "uid"
	paxGid      = "gid"
	paxUname    = "uname"
	paxGname    = "gname"
	paxMtime    = "mtime"
	paxAtime    = "atime"
	paxCtime    = "ctime"   // Removed from later revision of PAX spec, but was valid
	paxCharset  = "charset" // Currently unused
	paxComment  = "comment" // Currently unused

	paxSchilyXattr = "SCHILY.xattr."

	// Keywords for GNU sparse files in a PAX extended header.
	paxGNUSparse          = "GNU.sparse."
	paxGNUSparseNumBlocks = "GNU.sparse.numblocks"
	paxGNUSparseOffset    = "GNU.sparse.offset"
	paxGNUSparseNumBytes  = "GNU.sparse.numbytes"
	paxGNUSparseMap       = "GNU.sparse.map"
	paxGNUSparseName      = "GNU.sparse.name"
	paxGNUSparseMajor     = "GNU.sparse.major"
	paxGNUSparseMinor     = "GNU.sparse.minor"
	paxGNUSparseSize      = "GNU.sparse.size"
	paxGNUSparseRealSize  = "GNU.sparse.realsize"
)

// inode represents a single header in a tar archive.
// Some fields may not be populated.
type inode struct {
	mtime      time.Time
	fi         io.ReaderAt
	xattrs     map[string]string
	paxrecords map[string]string
	name       string
	linkname   string
	subs       []fs.DirEntry
	spds       sparseDatas
	offset     int64
	size       int64
	mode       int64
	format     format
	typeflag   byte
}

func (i *inode) physicalSize() int64 {
	if len(i.spds) == 0 {
		return i.size
	}
	var res int64
	for j := range i.spds {
		if i.spds[j].hole() {
			continue
		}
		res += i.spds[j].length
	}
	return res
}

func (ino *inode) extents(offset, size int64) (res c.Extents) {
	if offset < 0 || offset >= ino.size {
		return
	}
	if len(ino.spds) == 0 {
		if offset+size > ino.size {
			size = ino.size - offset
		}
		res = c.AllocExtents(1).Append(ino.fi, ino.offset+offset, size)
		return
	}
	var runningOffset int64
	res = c.AllocExtents(len(ino.spds))
	for _, ex := range ino.spds {
		if size <= 0 {
			break
		}
		if ex.offs()+ex.length < offset {
			continue
		}
		offsetInExtent := offset - ex.offs()
		dataSize := ex.length - offsetInExtent
		if dataSize > size {
			dataSize = size
		}
		if !ex.hole() {
			res = res.Append(ino.fi, ino.offset+offsetInExtent, dataSize)
		} else {
			res = res.Append(nil, ino.offset+offsetInExtent, dataSize)
		}
		size -= dataSize
		offset += dataSize
		runningOffset += dataSize
	}
	return
}

func (ino *inode) ReadDir() ([]fs.DirEntry, error) {
	if ino.typeflag == TypeDir {
		return ino.subs, nil
	}
	return nil, syscall.ENOTDIR
}

func (ino *inode) ReadLink() (string, error) {
	if ino.typeflag == TypeSymlink {
		return ino.linkname, nil
	}
	return "", fs.ErrInvalid
}

func (ino *inode) ReadXattrs() map[string]string {
	return ino.xattrs
}

func (ino *inode) Open() *c.Reader {
	return c.GetExtents(ino.extents).Reader()
}

func (ino *inode) lookup(parts []string, offset int) (*inode, int) {
	if offset < len(parts) && ino.typeflag == TypeDir {
		i, found := sort.Find(len(ino.subs), func(i int) int {
			return strings.Compare(parts[offset], ino.subs[i].Name())
		})
		if found {
			return ino.subs[i].(*inode).lookup(parts, offset+1)
		}
	}
	return ino, offset
}

func (ino *inode) insert(child *inode) {
	if ino.typeflag != TypeDir {
		panic("not a dir")
	}
	child.name = path.Base(child.name)
	if len(ino.subs) == 0 {
		ino.subs = []fs.DirEntry{child}
		return
	}
	i, found := sort.Find(len(ino.subs), func(i int) int {
		return strings.Compare(child.name, ino.subs[i].Name())
	})
	if !found {
		ino.subs = append(ino.subs, nil)
		copy(ino.subs[i+1:], ino.subs[i:])
	}
	ino.subs[i] = child
}

var (
	_ = c.Inode(&inode{})
)

func (ino *inode) Name() string {
	return path.Base(ino.name)
}

func (ino *inode) Size() int64 {
	return ino.size
}

func (ino *inode) Mode() fs.FileMode {
	return fs.FileMode(ino.mode)
}

func (ino *inode) ModTime() time.Time {
	return ino.mtime
}

func (ino *inode) IsDir() bool {
	return ino.typeflag == TypeDir
}

func (ino *inode) IsSymlink() bool {
	return ino.typeflag == TypeSymlink
}

func (ino *inode) Type() fs.FileMode {
	return fs.ModeType & ino.Mode()
}

func (ino *inode) Info() (fs.FileInfo, error) {
	return ino, nil
}

func (ino *inode) Sys() interface{} {
	return nil
}

// sparseEntry represents a Length-sized fragment at Offset in the file.
type sparseEntry struct {
	offset uint64
	length int64
}

func (s sparseEntry) endOffset() int64 { return int64(s.offset>>1) + s.length }
func (s sparseEntry) offs() int64      { return int64(s.offset >> 1) }
func (s sparseEntry) hole() bool       { return s.offset&1 > 0 }

// A sparse file can be represented as either a sparseDatas or a sparseHoles.
// As long as the total size is known, they are equivalent and one can be
// converted to the other form and back. The various tar formats with sparse
// file support represent sparse files in the sparseDatas form. That is, they
// specify the fragments in the file that has data, and treat everything else as
// having zero bytes. As such, the encoding and decoding logic in this package
// deals with sparseDatas.
//
// However, the external API uses sparseHoles instead of sparseDatas because the
// zero value of sparseHoles logically represents a normal file (i.e., there are
// no holes in it). On the other hand, the zero value of sparseDatas implies
// that the file has no data in it, which is rather odd.
//
// As an example, if the underlying raw file contains the 10-byte data:
//
//	var compactFile = "abcdefgh"
//
// And the sparse map has the following entries:
//
//	var spd sparseDatas = []sparseEntry{
//		{Offset: 2,  Length: 5},  // Data fragment for 2..6
//		{Offset: 18, Length: 3},  // Data fragment for 18..20
//	}
//	var sph sparseHoles = []sparseEntry{
//		{Offset: 0,  Length: 2},  // Hole fragment for 0..1
//		{Offset: 7,  Length: 11}, // Hole fragment for 7..17
//		{Offset: 21, Length: 4},  // Hole fragment for 21..24
//	}
//
// Then the content of the resulting sparse file with a inode.size of 25 is:
//
//	var sparseFile = "\x00"*2 + "abcde" + "\x00"*11 + "fgh" + "\x00"*4
type (
	sparseDatas []sparseEntry
	sparseHoles []sparseEntry
)

// validateSparseEntries reports whether sp is a valid sparse map.
// It does not matter whether sp represents data fragments or hole fragments.
func validateSparseEntries(sp []sparseEntry, size int64) bool {
	// Validate all sparse entries. These are the same checks as performed by
	// the BSD tar utility.
	if size < 0 {
		return false
	}
	var pre sparseEntry
	for _, cur := range sp {
		switch {
		case cur.length < 0:
			return false // Negative values are never okay
		case int64(cur.offset>>1) > math.MaxInt64-cur.length:
			return false // Integer overflow with large length
		case cur.endOffset() > size:
			return false // Region extends beyond the actual size
		case pre.endOffset() > cur.offs():
			return false // Regions cannot overlap and must be in order
		}
		pre = cur
	}
	return true
}

// fileState tracks the number of logical (includes sparse holes) and physical
// (actual in tar archive) bytes remaining for the current file.
//
// Invariant: logicalRemaining >= physicalRemaining
type fileState interface {
	logicalRemaining() int64
	physicalRemaining() int64
}

// splitUSTARPath splits a path according to USTAR prefix and suffix rules.
// If the path is not splittable, then it will return ("", "", false).
func splitUSTARPath(name string) (prefix, suffix string, ok bool) {
	length := len(name)
	if length <= nameSize || !isASCII(name) {
		return "", "", false
	} else if length > prefixSize+1 {
		length = prefixSize + 1
	} else if name[length-1] == '/' {
		length--
	}

	i := strings.LastIndex(name[:length], "/")
	nlen := len(name) - i - 1 // nlen is length of suffix
	plen := i                 // plen is length of prefix
	if i <= 0 || nlen > nameSize || nlen == 0 || plen > prefixSize {
		return "", "", false
	}
	return name[:i], name[i+1:], true
}

// isHeaderOnlyType checks if the given type flag is of the type that has no
// data section even if a size is specified.
func isHeaderOnlyType(flag byte) bool {
	switch flag {
	case TypeLink, TypeSymlink, TypeChar, TypeBlock, TypeDir, TypeFifo:
		return true
	default:
		return false
	}
}

func min[T constraints.Ordered](a, b T) T {
	if a < b {
		return a
	}
	return b
}
func normalizeName(p string) string {
	p = path.Clean(p)
	if p == "/" {
		return "."
	}
	if len(p) > 0 && p[0] == '/' {
		return p[1:]
	}
	return p
}
