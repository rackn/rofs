package squash

type tableReader struct {
	sb          *squashFs
	buf         []byte
	offset      uint64
	shiftedBase uint64 // position of table block list (when liveBlocks aren't one after another)
}

func (sq *squashFs) inodeReader(ino inodeRef) (*tableReader, error) {
	return sq.table(sq.inodeTableStart+uint64(ino.idx()), int(ino.offs()))
}

func (sq *squashFs) table(base uint64, start int) (*tableReader, error) {
	ir := &tableReader{
		sb:     sq,
		offset: base,
	}

	err := ir.readBlock()
	if err != nil {
		return nil, err
	}

	if start != 0 {
		// need to cut blockOffset
		ir.buf = ir.buf[start:]
	}

	return ir, nil
}

func (sq *squashFs) indirectTable(base uint64, start int) (*tableReader, error) {
	ir := &tableReader{
		sb:          sq,
		shiftedBase: base,
	}

	err := ir.readBlock()
	if err != nil {
		return nil, err
	}

	if start != 0 {
		// need to cut blockOffset
		ir.buf = ir.buf[start:]
	}

	return ir, nil
}

func (i *tableReader) readBlock() error {
	do := [8]byte{}
	if i.shiftedBase != 0 {
		// shiftedBase mode
		_, err := i.sb.src.ReadAt(do[:], int64(i.shiftedBase))
		if err != nil {
			return err
		}
		i.offset = order.Uint64(do[:])
	}
	_, err := i.sb.src.ReadAt(do[:2], int64(i.offset))
	if err != nil {
		return err
	}
	lenN := int(order.Uint16(do[:2]))
	nocompressFlag := false
	lenN, nocompressFlag = lenN&0x7fff, lenN&0x8000 == 0
	buf, err := i.sb.readMetaBlock(i.offset+2, lenN, nocompressFlag)
	if err == nil {
		i.buf = buf
		i.offset += uint64(lenN) + 2
	}
	return err
}

func (i *tableReader) Read(p []byte) (int, error) {
	// read from buf, if empty call readBlock()
	if i.buf == nil {
		err := i.readBlock()
		if err != nil {
			return 0, err
		}
	}

	n := copy(p, i.buf)
	if n == len(i.buf) {
		i.buf = nil
	} else {
		i.buf = i.buf[n:]
	}

	return n, nil
}
