package squash

import (
	"encoding/binary"
	"fmt"
	"gitlab.com/rackn/rofs/c"
	sc "gitlab.com/rackn/simplecache"
	"io"
	"io/fs"
	"sync"
)

const superblockSize = 96
const metaBlockSize = 8192

var order = binary.LittleEndian

type superBlock struct {
	magic            uint32
	inodeCnt         uint32
	modTime          uint32
	blockSize        uint32
	fragCount        uint32
	comp             Compression
	blockLog         uint16
	flags            Flags
	idCount          uint16
	vMajor           uint16
	vMinor           uint16
	rootInode        inodeRef
	bytesUsed        uint64
	idTableStart     uint64
	xattrTableStart  uint64
	inodeTableStart  uint64
	dirTableStart    uint64
	fragTableStart   uint64
	exportTableStart uint64
}

// squashFs implements squashfs support.
type squashFs struct {
	src io.ReaderAt
	superBlock
	rootIno              *namedInode
	rootInoN             uint64
	inoIdx               map[uint32]inodeRef // inode refs dataCache (see export table)
	inoIdxL              sync.RWMutex
	inoOfft              uint64
	idTable              []uint32
	decompress           Decompressor
	dataCache, metaCache *sc.Cache[uint64, []byte]
	dataPool, metaPool   *sync.Pool
}

func (s *squashFs) Close() error {
	s.dataCache.Clear()
	s.metaCache.Clear()
	if cl, ok := s.src.(io.Closer); ok {
		return cl.Close()
	}
	return nil
}

func (s *squashFs) getDataBlock() []byte {
	res := s.dataPool.Get().([]byte)
	if cap(res) < int(s.blockSize) {
		panic("bad cached buf")
	}
	res = res[:cap(res)]
	for i := range res {
		res[i] = 0
	}
	return res[:s.blockSize]
}

func (s *squashFs) getMetaBlock() []byte {
	res := s.metaPool.Get().([]byte)
	if cap(res) < metaBlockSize {
		panic("bad cached buf")
	}
	res = res[:cap(res)]
	for i := range res {
		res[i] = 0
	}
	return res[:metaBlockSize]
}

func (sb *squashFs) readMetaBlock(offset uint64, size int, compressed bool) ([]byte, error) {
	return sb.metaCache.Acquire(offset,
		func(offset uint64) ([]byte, error) {
			in := sb.getMetaBlock()[:size]
			if _, err := sb.src.ReadAt(in, int64(offset)); err != nil {
				return nil, err
			}
			if !compressed {
				return in, nil
			}
			defer sb.metaPool.Put(in)
			out := sb.getMetaBlock()[:metaBlockSize]
			var err error
			out, err = sb.decompress(in, out)
			if err != nil || len(out) > metaBlockSize {
				sb.metaPool.Put(out)
				return nil, fs.ErrInvalid
			}
			return out, err
		})
}

var _ = c.InodeProvider(&squashFs{})

// Open returns a new instance of squashFs for a given io.ReaderAt that can
// be used to access files inside squashfs.
func Open(fs io.ReaderAt) (res c.Filesystem, err error) {
	sb := &squashFs{src: fs,
		inoIdx: make(map[uint32]inodeRef),
	}
	head := make([]byte, superblockSize)

	_, err = fs.ReadAt(head, 0)
	if err != nil {
		return
	}
	err = (&sb.superBlock).UnmarshalBinary(head)
	if err != nil {
		return
	}
	decompressMux.Lock()
	sb.decompress = decompressHandlers[sb.comp]
	decompressMux.Unlock()
	if sb.decompress == nil {
		err = fmt.Errorf("unsupported compression format %s", sb.comp)
		return
	}

	sb.dataPool = &sync.Pool{New: func() any { return make([]byte, sb.blockSize) }}
	sb.metaPool = &sync.Pool{New: func() any { return make([]byte, metaBlockSize) }}
	sb.dataCache = sc.New[uint64, []byte](16, func(b []byte) { sb.dataPool.Put(b[:cap(b)]) })
	sb.metaCache = sc.New[uint64, []byte](32, func(b []byte) { sb.metaPool.Put(b[:cap(b)]) })
	// get root inode
	sb.rootIno = &namedInode{name: "."}
	sb.rootIno.ino, err = sb.getInodeRef(sb.rootInode)
	if err != nil {
		return
	}

	sb.rootInoN = uint64(sb.rootIno.ino.ino)
	if err = sb.rootIno.ino.fillSubs(); err != nil {
		return
	}

	if err = sb.readIdTable(); err == nil {
		res.InodeProvider = sb
	}

	return
}

func (sq *squashFs) readIdTable() error {
	// read id table
	idtable, err := sq.indirectTable(sq.idTableStart, 0)
	if err != nil {
		return err
	}
	var id uint32
	sq.idTable = make([]uint32, sq.idCount)
	for i := range sq.idTable {
		err := binary.Read(idtable, order, &id)
		if err != nil {
			return err
		}
		sq.idTable[i] = id
	}
	return nil
}

func (sb *superBlock) UnmarshalBinary(data []byte) error {
	if len(data) != superblockSize {
		return ErrInvalidSuper
	}
	switch string(data[:4]) {
	case "hsqs":
	default:
		return ErrInvalidFile
	}

	sb.magic = order.Uint32(data[0:4])
	sb.inodeCnt = order.Uint32(data[4:8])
	sb.modTime = order.Uint32(data[8:12])
	sb.blockSize = order.Uint32(data[12:16])
	sb.fragCount = order.Uint32(data[16:20])
	sb.comp = Compression(order.Uint16(data[20:22]))
	sb.blockLog = order.Uint16(data[22:24])
	sb.flags = Flags(order.Uint16(data[24:26]))
	sb.idCount = order.Uint16(data[26:28])
	sb.vMajor = order.Uint16(data[28:30])
	sb.vMinor = order.Uint16(data[30:32])
	sb.rootInode = inodeRef(order.Uint64(data[32:40]))
	sb.bytesUsed = order.Uint64(data[40:48])
	sb.idTableStart = order.Uint64(data[48:56])
	sb.xattrTableStart = order.Uint64(data[56:64])
	sb.inodeTableStart = order.Uint64(data[64:72])
	sb.dirTableStart = order.Uint64(data[72:80])
	sb.fragTableStart = order.Uint64(data[80:88])
	sb.exportTableStart = order.Uint64(data[88:96])
	if sb.magic != 0x73717368 {
		return ErrInvalidFile
	}
	if uint32(1)<<sb.blockLog != uint32(sb.blockSize) {
		return ErrInvalidSuper
	}
	if sb.vMajor != 4 || sb.vMinor != 0 {
		return ErrInvalidVersion
	}
	return nil
}

func (s *squashFs) Root() c.Inode {
	return s.rootIno
}
