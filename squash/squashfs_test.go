package squash

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"gitlab.com/rackn/rofs/c"
	"io"
	"io/fs"
	"log"
	"os"
	"testing"
)

// testdata/zlib-dev.squashfs

func s256(buf []byte) string {
	hash := sha256.Sum256(buf)
	return hex.EncodeToString(hash[:])
}

type listing struct {
	f string
	l []string
}

func (l listing) examine(t *testing.T) {
	fi, err := os.Open(l.f)
	if err != nil {
		t.Fatalf("Error opening %s: %v", l.f, err)
	}
	defer fi.Close()
	f2 := c.Mmap(fi)
	sq, err := Open(f2)
	if err != nil {
		t.Fatalf("Error opening %s: %v", l.f, err)
	}
	var res []string
	err = fs.WalkDir(sq, ".", func(p string, d fs.DirEntry, e2 error) error {
		if e2 != nil {
			return e2
		}
		res = append(res, p)
		return nil
	})
	if err != nil {
		t.Errorf("List err in %s : %v", l.l, err)
		return
	}
	i, j := 0, 0
	for {
		er, el := i == len(res), j == len(l.l)
		if er && el {
			break
		}
		if el {
			t.Errorf("Extra res entry %s", res[i])
			i++
			continue
		}
		if er {
			t.Errorf("Expected list entry %s", l.l[j])
			j++
			continue
		}
		if res[i] == l.l[j] {
			i++
			j++
		} else if res[i] < l.l[j] {
			t.Errorf("Extra result %s", res[i])
			i++
		} else {
			t.Errorf("Missing result %s", l.l[j])
			j++
		}
	}
	expectedContents, err := os.ReadFile("testdata/README.md")
	if err != nil {
		t.Fatalf("Failed to open expected contents: %v", err)
	}
	buf, err := fs.ReadFile(sq, "README.md")
	if err != nil {
		t.Errorf("Error reading README: %v", err)
	}
	if !bytes.Equal(expectedContents, buf) {
		t.Errorf("Want %s, have %s", expectedContents, buf)
	}
	tf, err := sq.Open("README.md")
	if err != nil {
		t.Fatalf("Error opening README: %v", err)
	}
	dst := &bytes.Buffer{}
	if _, err := io.Copy(dst, tf); err != nil {
		t.Fatalf("Error copying README: %v", err)
	}
	buf = dst.Bytes()
	if !bytes.Equal(expectedContents, buf) {
		t.Errorf("Want %s, have %s", expectedContents, buf)
	}
}

func TestOpenSquashes(t *testing.T) {
	res := []string{
		".",
		"LICENSE",
		"README.md",
		"a",
		"a/..README.md",
		"a/b",
		"a/b/c",
		"a/b/c/README.md",
		"a/b/c/d",
		"a/b/c/d/e",
		"a/b/c/d/e/f",
		"a/b/c/d/e/f/g",
		"a/b/c/d/e/f/g/LICENSE",
	}
	listings := []listing{
		{f: "testdata/zstd.squashfs", l: res},
		{f: "testdata/zlib.squashfs", l: res},
	}
	for _, l := range listings {
		l.examine(t)
	}
}

func TestBigDirs(t *testing.T) {
	fi, err := os.Open("testdata/bigdir.squashfs")
	if err != nil {
		t.Fatalf("Error opening bigdir: %v", err)
	}
	defer fi.Close()
	f2 := c.Mmap(fi)
	sq, err := Open(f2)
	if err != nil {
		t.Fatalf("Error opening bigdir: %v", err)
	}
	for _, sub := range []string{"a", "b", "c"} {
		ents, err := fs.ReadDir(sq, sub)
		if err != nil {
			t.Errorf("Error walking %s: %v", sub, err)
		}
		switch sub {
		case "a":
			if len(ents) != 511 {
				t.Errorf("Wanted 511 entries, not %d", len(ents))
			}
		case "b":
			if len(ents) != 2047 {
				t.Errorf("Wanted 2047 entries, not %d", len(ents))
			}
		case "c":
			if len(ents) != 4095 {
				t.Errorf("Wanted 4095 entries, not %d", len(ents))
			}
		}
	}
}

func TestSquashfs(t *testing.T) {
	type ts struct {
		name string
		fail bool
	}
	tests := []ts{
		{name: "testdata/zlib-dev.gzip.squashfs", fail: false},
		{name: "testdata/zlib-dev.lz4.squashfs", fail: false},
		{name: "testdata/zlib-dev.lzma.squashfs", fail: false},
		{name: "testdata/zlib-dev.lzo.squashfs", fail: true},
		{name: "testdata/zlib-dev.xz.squashfs", fail: false},
		{name: "testdata/zlib-dev.zstd.squashfs", fail: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			fi, err := os.Open(tt.name)
			if err != nil {
				t.Fatalf("Failed to open archive file %s: %v", tt.name, err)
			}
			defer fi.Close()
			f2 := c.Mmap(fi)
			sqfs, err := Open(f2)
			if err != nil {
				if !tt.fail {
					t.Errorf("failed to open %s: %v", tt.name, err)
				} else {
					t.Logf("Failed to open %s as expected: %v", tt.name, err)
				}
				return
			} else {
				if tt.fail {
					t.Errorf("Opened %s, but did not expect to", tt.name)
				} else {
					t.Logf("Opened %s", tt.name)
				}
			}

			data, err := fs.ReadFile(sqfs, "pkgconfig/zlib.pc")
			if err != nil {
				t.Errorf("failed to read pkgconfig/zlib.pc: %v", err)
			} else {
				//log.Printf("zlib.pc = %s", s256(data))
				if s256(data) != "2bbfca2364630d3ad2bbc9d44f45fe5470236539a906e11e2072157709e54692" {
					t.Errorf("invalid hash for pkgconfig/zlib.pc")
				}
			}

			// ensure we get the right inode
			ino, err := sqfs.Lookup("lib/libz.a", false)
			if err != nil {
				t.Errorf("failed to find lib/libz.a")
			} else {
				// should be inode 6
				if ino.(*namedInode).ino.ino != 6 {
					t.Errorf("invalid inode found for lib/libz.a")
				}
			}

			// test glob (will test readdir etc)
			res, err := fs.Glob(sqfs, "lib/*.so")
			if err != nil {
				t.Errorf("failed to glob lib/*.so: %v", err)
			} else {
				if len(res) != 1 || res[0] != "lib/libz.so" {
					log.Printf("bad response for glob lib/*.so")
				}
			}

			st, err := fs.Stat(sqfs, "include/zlib.h")
			if err != nil {
				t.Errorf("failed to stat include/zlib.h: %v", err)
			} else {
				if st.Size() != 97323 {
					t.Errorf("bad file size on stat include/zlib.h")
				}
			}

			// test stat vs lstat
			st, err = fs.Stat(sqfs, "lib")
			if err != nil {
				t.Errorf("failed to stat lib: %v", err)
			} else if !st.IsDir() {
				t.Errorf("failed: stat(lib) did not return a directory")
			}

			st, err = sqfs.Lstat("lib")
			if err != nil {
				t.Errorf("failed to lstat lib: %v", err)
			} else if st.IsDir() {
				t.Errorf("failed: lstat(lib) should have returned something that is not a directory")
			}

			// test error
			_, err = fs.ReadFile(sqfs, "pkgconfig/zlib.pc/foo")
			if !errors.Is(err, fs.ErrNotExist) {
				t.Errorf("readfile pkgconfig/zlib.pc/foo returned unexpected err=%v", err)
			}

			// test other error
			_, err = sqfs.Lookup("lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/../lib/libz.a", false)
			if !errors.Is(err, fs.ErrInvalid) {
				t.Errorf("readfile lib/../lib/../(...)/libz.a returned unexpected err=%v", err)
			}
		})
	}
}
