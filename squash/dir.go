package squash

import (
	"io"
)

type dirReader struct {
	sb                          *squashFs
	r                           *io.LimitedReader
	count, startBlock, inodeNum uint32
}

func (sq *squashFs) dirReader(i *inode) (*dirReader, error) {
	tbl, err := i.sb.table(i.sb.dirTableStart+i.startBlock, int(i.offset))
	if err != nil {
		return nil, err
	}

	dr := &dirReader{
		sb: i.sb,
		r:  &io.LimitedReader{R: tbl, N: int64(i.sz)},
	}

	return dr, nil
}

func (dr *dirReader) next() (string, inodeRef, error) {
	name, _, inoR, err := dr.nextfull()
	return name, inoR, err
}

func (dr *dirReader) nextfull() (string, inoType, inodeRef, error) {
	// read next entry
	if dr.r.N == 3 {
		return "", 0, 0, io.EOF // probably
	}

	if dr.count == 0 {
		err := dr.readHeader()
		if err != nil {
			return "", 0, 0, err
		}
	}
	var buf [8]byte
	if _, err := io.ReadFull(dr.r, buf[:]); err != nil {
		return "", 0, 0, err
	}
	offset := order.Uint16(buf[0:2])
	typ := inoType(order.Uint16(buf[4:6]))
	siz := order.Uint16(buf[6:8])
	name := make([]byte, int(siz)+1)
	if _, err := io.ReadFull(dr.r, name); err != nil {
		return "", 0, 0, err
	}
	dr.count -= 1

	inoRef := inodeRef((uint64(dr.startBlock) << 16) | uint64(offset))
	return string(name), typ, inoRef, nil
}

func (dr *dirReader) readHeader() error {
	var buf [12]byte
	if _, err := io.ReadFull(dr.r, buf[:]); err != nil {
		return err
	}
	dr.count = order.Uint32(buf[0:4])
	dr.startBlock = order.Uint32(buf[4:8])
	dr.inodeNum = order.Uint32(buf[8:12])
	dr.count += 1
	return nil
}
