package squash

import (
	"gitlab.com/rackn/rofs/c"
	"io/fs"
	"syscall"
	"time"
)

// namedInode is an inode + a file name.  Shocking.
// It is the base for the exported file-ish things.
type namedInode struct {
	ino  *inode
	name string
}

var _ c.Inode = &namedInode{}

func (ni *namedInode) Type() fs.FileMode {
	return ni.ino.mode().Type()
}

func (ni *namedInode) ReadDir() ([]fs.DirEntry, error) {
	if ni.IsDir() {
		return ni.ino.subs, nil
	}
	return nil, syscall.ENOTDIR
}

func (ni *namedInode) ReadXattrs() map[string]string {
	return nil
}

func (ni *namedInode) ReadLink() (string, error) {
	return ni.ino.readLink()
}

func (ni *namedInode) Stat() (fs.FileInfo, error) {
	return ni, nil
}

func (ni *namedInode) IsDir() bool {
	return ni.ino.isDir()
}

func (ni *namedInode) IsSymlink() bool {
	return ni.ino.typ.IsSymlink()
}

func (ni *namedInode) Mode() fs.FileMode {
	return ni.ino.mode()
}

// Name returns the file's base name
func (ni *namedInode) Name() string {
	return ni.name
}

// Size returns the file's size
func (ni *namedInode) Size() int64 {
	return int64(ni.ino.sz)
}

func (ni *namedInode) Info() (fs.FileInfo, error) {
	return ni, nil
}

func (ni *namedInode) ModTime() time.Time {
	return time.Unix(int64(ni.ino.mtime), 0)
}

func (ni *namedInode) Open() *c.Reader {
	return c.GetExtents(ni.ino.extents).Reader()
}

type SysStat struct {
	Mode     fs.FileMode
	Nlink    uint32
	Ino      uint32
	Uid, Gid uint32
	Mtime    time.Time
	Size     int64
	Blocks   int64
	Blksize  int64
}

// Sys returns an object containing lower level stat information.
// Most of it is a quasi useful lie.
func (ni *namedInode) Sys() any {
	return SysStat{
		Mode:    ni.ino.mode(),
		Nlink:   ni.ino.nLink,
		Ino:     ni.ino.ino,
		Uid:     ni.ino.sb.idTable[ni.ino.uid],
		Gid:     ni.ino.sb.idTable[ni.ino.gid],
		Mtime:   ni.ModTime(),
		Size:    int64(ni.ino.sz),
		Blocks:  int64(len(ni.ino.blocks)),
		Blksize: int64(ni.ino.sb.blockSize),
	}
}
