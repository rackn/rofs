package squash

import (
	"fmt"
	"github.com/klauspost/compress/zlib"
	"github.com/pierrec/lz4/v4"
	"gitlab.com/rackn/rofs/c"

	"github.com/ulikunitz/xz/lzma"
	//"github.com/rasky/go-lzo"
	"github.com/xi2/xz"
	"io"
	"sync"
)

// Compression tracks what type of compression a squashfs is using.
// We support all known formats  except LZO to a greater or lesser degree of efficiency.
// right now, zstd will be the most performant, followed by zlib.
// LZO is excluded due to the only Go native implementation being GPL licensed.
type Compression uint16

const (
	GZip Compression = iota + 1 // GZip is compatible with darn near everything, but is neither the fastest or smallest compression.
	LZMA                        // LZMA decompressors are bloated and slow, avoid.
	LZO                         // LZO is not available due to GPL entanglements.
	XZ                          // XZ has good compression, but it isnt the fastest decompression around.
	LZ4                         // LZ4 has fast decompression, but is by far the least space efficient.
	ZSTD                        // ZSTD has excellent decompression speed and good compression ratios.
)

// Decompressor takes an input buffer and a destination buffer and returns the destination buffer filled with data and an error
// It matches the function signature of github.com/klauspost/compress/zstd Reader.DecodeAll.
// The library internals will eventually leverage it to optimize buffer reuse and caching.
type Decompressor func([]byte, []byte) ([]byte, error)

var decompressHandlers = map[Compression]Decompressor{}

var decompressMux = &sync.Mutex{}

func init() {
	RegisterDecompressor(ZSTD, func(buf, dst []byte) ([]byte, error) { return c.Zstd.DecodeAll(buf, dst[:0]) })
	RegisterDecompressor(GZip, MakeDecompressorErr(func(r io.Reader) (io.Reader, error) { return zlib.NewReader(r) }))
	RegisterDecompressor(LZ4, func(buf, dst []byte) ([]byte, error) {
		n, err := lz4.UncompressBlock(buf, dst)
		dst = dst[:n]
		return dst, err
	})
	RegisterDecompressor(XZ, MakeDecompressorErr(func(r io.Reader) (io.Reader, error) { return xz.NewReader(r, xz.DefaultDictMax) }))
	RegisterDecompressor(LZMA, MakeDecompressorErr(func(r io.Reader) (io.Reader, error) { return lzma.NewReader(r) }))
	//RegisterDecompressor(LZO, func(in, out []byte) ([]byte, error) {
	//	return lzo.Decompress1X(bytes.NewReader(in), len(in), 0)
	//})
}

func (s Compression) String() string {
	switch s {
	case GZip:
		return "GZip"
	case LZMA:
		return "LZMA"
	case LZO:
		return "LZO"
	case XZ:
		return "XZ"
	case LZ4:
		return "LZ4"
	case ZSTD:
		return "ZSTD"
	}
	return fmt.Sprintf("Compression(%d)", s)
}

// RegisterDecompressor can be used to register a Decompressor for squashfs.
// GZip and ZSTD are supported by default.  You can register additional
// decompressor types before opening squashfs archives that use them.  Once
// registered, decompressors cannot be deregistered, only replaced.
func RegisterDecompressor(method Compression, dcomp Decompressor) {
	decompressMux.Lock()
	decompressHandlers[method] = dcomp
	decompressMux.Unlock()
}

func MakeDecompressor(dec func(r io.Reader) io.Reader) Decompressor {
	return func(buf, dst []byte) ([]byte, error) {
		r := c.NewBuffer(buf)
		p := dec(r)
		if cl, ok := p.(io.Closer); ok {
			defer cl.Close()
		}
		w := c.NewWriteBuffer(dst)
		_, err := io.Copy(w, p)
		return w.Bytes(), err
	}
}

// MakeDecompressorErr is similar to MakeDecompressor but the factory method also
// returns an error.
//
// Example use:
// * squashfs.RegisterDecompressor(squashfs.LZMA, squashfs.MakeDecompressorErr(lzma.NewReader))
// * squashfs.RegisterDecompressor(squashfs.XZ, squashfs.MakeDecompressorErr(xz.NewReader))
func MakeDecompressorErr(dec func(r io.Reader) (io.Reader, error)) Decompressor {
	return func(buf, dst []byte) ([]byte, error) {
		r := c.NewBuffer(buf)
		p, err := dec(r)
		if err != nil {
			return nil, err
		}
		if cl, ok := p.(io.Closer); ok {
			defer cl.Close()
		}
		w := c.NewWriteBuffer(dst)
		_, err = io.Copy(w, p)
		return w.Bytes(), err
	}
}
