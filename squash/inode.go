package squash

import (
	"errors"
	"gitlab.com/rackn/rofs/c"
	"io"
	"io/fs"
	"math"
	"sort"
)

type inode struct {
	sb *squashFs
	// file blocks (some have value 0x1001000)
	blocks       []uint32
	blocksOffset []uint64
	subs         []fs.DirEntry
	symTarget    []byte // The target path this symlink points to
	startBlock   uint64
	sz           uint64 // Careful, actual on disk size varies depending on type
	mtime        uint32
	ino          uint32 // inode number
	nLink        uint32
	offset       uint32 // uint16 for directories
	parentIno    uint32 // for directories
	xattrIdx     uint32 // xattr table index (if relevant)
	// fragment
	fragBlock  uint32
	fragOffset uint32
	typ        inoType
	perm       uint16
	gid, uid   uint16
	idxCount   uint16 // index count for advanced directories
}

func (i *inode) findFragment() (offset uint64, size uint32, err error) {
	// this is a fragment, need to decode fragment
	// read table blockOffset
	sub := int64(i.fragBlock) / 512 * 8
	blInfo := [12]byte{}
	_, err = i.sb.src.ReadAt(blInfo[:8], int64(i.sb.fragTableStart)+sub)
	if err != nil {
		return 0, 0, err
	}
	var t *tableReader
	// read table
	t, err = i.sb.table(order.Uint64(blInfo[:8]), int(i.fragBlock%512)*16)
	if err == nil {
		_, err = t.Read(blInfo[:])
	}
	if err == nil {
		offset = order.Uint64(blInfo[:8])
		size = order.Uint32(blInfo[8:])
	}
	return
}

type extent struct {
	sb          *squashFs
	blockOffset int64
	dataOffset  int64
	blockSize   uint32
	dataSize    uint32
}

func (e *extent) WithBytesAt(offset int64, size uint, f func([]byte) error) error {
	var buf []byte
	var err error
	buf, err = e.sb.dataCache.Acquire(uint64(e.blockOffset),
		func(offset uint64) ([]byte, error) {
			blockSize := e.blockSize & 0xfffff
			compressed := e.blockSize&0x1000000 == 0
			in := e.sb.getDataBlock()[:blockSize]
			_, err2 := e.sb.src.ReadAt(in, e.blockOffset)
			if err2 != nil {
				e.sb.dataPool.Put(in)
				return nil, err2
			}
			if !compressed {
				if len(in) < int(blockSize) {
					in = in[:int(blockSize)]
				}
				return in, err2
			}
			defer e.sb.dataPool.Put(in)
			out := e.sb.getDataBlock()
			if out, err2 = e.sb.decompress(in, out); err2 != nil {
				e.sb.dataPool.Put(out)
				return nil, err2
			}
			if len(out) < int(blockSize) {
				out = out[:blockSize]
			}
			return out, nil
		})
	if err != nil {
		return err
	}
	defer e.sb.dataCache.Release(uint64(e.blockOffset))
	buf = buf[int(e.dataOffset) : int(e.dataOffset)+int(e.dataSize)]
	if offset < 0 || offset > int64(len(buf)) {
		return io.EOF
	}
	size += uint(offset)
	if size > uint(len(buf)) {
		size = uint(len(buf))
	}
	return f(buf[int(offset):int(size)])
}

func (e *extent) ReadAt(buf []byte, offset int64) (written int, err error) {
	err = e.WithBytesAt(offset, uint(len(buf)), func(b []byte) error {
		written = copy(buf, b)
		return nil
	})
	return
}

func (ino *inode) extents(offset, size int64) (res c.Extents) {
	if offset < 0 || offset >= int64(ino.sz) {
		return
	}
	runningOffset := int64(0)
	res = c.AllocExtents(int(((offset + size) / int64(ino.sb.blockSize)) + 1))
	for size > 0 {
		i := int(offset / int64(ino.sb.blockSize))
		dataSize := ino.sb.blockSize
		extentOffset := int64(i * int(ino.sb.blockSize))
		dataOffset := int64(offset - extentOffset)
		if i == len(ino.blocks)-1 {
			dataSize = uint32(int64(ino.sz) - int64(ino.sb.blockSize)*int64(len(ino.blocks)-1))
		}
		blockSize := ino.blocks[i]
		var blockOffset uint64
		if blockSize == math.MaxUint32 {
			var err error
			blockOffset, blockSize, err = ino.findFragment()
			if err == nil && ino.fragOffset != 0 {
				dataOffset = int64(ino.fragOffset)
			}
		} else {
			blockOffset = ino.blocksOffset[i]
		}
		var src io.ReaderAt
		if blockSize > 0 {
			src = &extent{
				sb:          ino.sb,
				blockOffset: int64(blockOffset),
				dataOffset:  dataOffset,
				blockSize:   blockSize,
				dataSize:    dataSize,
			}
		}
		res = res.Append(src, 0, int64(dataSize))
		offset += int64(dataSize)
		runningOffset += int64(dataSize)
		size -= int64(dataSize)
	}
	return
}

func (ino *inode) readCommon(r *tableReader) error {
	var buf [16]byte
	if _, err := io.ReadFull(r, buf[:]); err != nil {
		return err
	}
	ino.typ = inoType(order.Uint16(buf[0:2]))
	ino.perm = order.Uint16(buf[2:4])
	ino.uid = order.Uint16(buf[4:6])
	ino.gid = order.Uint16(buf[6:8])
	ino.mtime = order.Uint32(buf[8:12])
	ino.ino = order.Uint32(buf[12:16])
	return nil
}

func (ino *inode) readBasicDir(r *tableReader) error {
	var buf [16]byte
	if _, err := io.ReadFull(r, buf[:]); err != nil {
		return err
	}
	ino.startBlock = uint64(order.Uint32(buf[0:4]))
	ino.nLink = order.Uint32(buf[4:8])
	ino.sz = uint64(order.Uint16(buf[8:10]))
	ino.offset = uint32(order.Uint16(buf[10:12]))
	ino.parentIno = order.Uint32(buf[12:16])
	return nil
}

func (ino *inode) readExtendedDir(r *tableReader) error {
	var buf [24]byte
	if _, err := io.ReadFull(r, buf[:]); err != nil {
		return err
	}
	ino.nLink = order.Uint32(buf[0:4])
	ino.sz = uint64(order.Uint32(buf[4:8]))
	ino.startBlock = uint64(order.Uint32(buf[8:12]))
	ino.parentIno = order.Uint32(buf[12:16])
	ino.idxCount = order.Uint16(buf[16:18])
	ino.offset = uint32(order.Uint16(buf[18:20]))
	ino.xattrIdx = order.Uint32(buf[20:24])
	return nil
}

func (ino *inode) fillFileBlocks(r *tableReader) error {
	// try to find out how many block_sizes entries
	blocks := int(ino.sz / uint64(ino.sb.blockSize))
	if ino.fragBlock == 0xffffffff {
		// file does not end in a fragment
		if ino.sz%uint64(ino.sb.blockSize) != 0 {
			blocks += 1
		}
	}
	blockBuf := make([]byte, blocks*4)
	ino.blocks = make([]uint32, blocks)
	ino.blocksOffset = make([]uint64, blocks)
	runningOffset := ino.startBlock
	if _, err := io.ReadFull(r, blockBuf); err != nil {
		return err
	}

	// read liveBlocks
	for i := range ino.blocks {
		ino.blocks[i] = order.Uint32(blockBuf[4*i : 4*(i+1)])
		ino.blocksOffset[i] = runningOffset
		runningOffset += uint64(ino.blocks[i]) & 0xfffff // 1MB-1, since max block size is 1MB
	}

	if ino.fragBlock != 0xffffffff {
		// this has a fragment instead of last block
		ino.blocks = append(ino.blocks, 0xffffffff) // special code
	}
	return nil
}

func (ino *inode) readBasicFile(r *tableReader) error {
	var buf [16]byte
	if _, err := io.ReadFull(r, buf[:]); err != nil {
		return err
	}
	ino.startBlock = uint64(order.Uint32(buf[0:4]))
	ino.fragBlock = order.Uint32(buf[4:8])
	ino.fragOffset = order.Uint32(buf[8:12])
	ino.sz = uint64(order.Uint32(buf[12:16]))
	return ino.fillFileBlocks(r)
}

func (ino *inode) readExtendedFile(r *tableReader) error {
	var buf [40]byte
	if _, err := io.ReadFull(r, buf[:]); err != nil {
		return err
	}
	ino.startBlock = order.Uint64(buf[0:8])
	ino.sz = order.Uint64(buf[8:16])
	// buf[16:24] is space hidden by sparse file encoding.  Only the Linux kernel cares about it.
	ino.nLink = order.Uint32(buf[24:28])
	ino.fragBlock = order.Uint32(buf[28:32])
	ino.fragOffset = order.Uint32(buf[32:36])
	ino.xattrIdx = order.Uint32(buf[36:40])
	return ino.fillFileBlocks(r)
}

func (ino *inode) readSymlink(r *tableReader) error {
	var buf [8]byte
	if _, err := io.ReadFull(r, buf[:]); err != nil {
		return err
	}
	ino.nLink = order.Uint32(buf[0:4])
	ino.sz = uint64(order.Uint32(buf[4:8]))
	if ino.sz > 4096 {
		// why is symlink length even stored as u32 ?
		return errors.New("symlink target too long")
	}
	ino.symTarget = make([]byte, int(ino.sz))
	_, err := io.ReadFull(r, ino.symTarget)
	return err
}

func (sq *squashFs) getInodeRef(inor inodeRef) (*inode, error) {
	r, err := sq.inodeReader(inor)
	if err != nil {
		return nil, err
	}
	ino := &inode{sb: sq}
	if err = ino.readCommon(r); err != nil {
		return nil, err
	}

	switch ino.typ {
	case DirType:
		err = ino.readBasicDir(r)
	case XDirType:
		err = ino.readExtendedDir(r)
	case FileType:
		err = ino.readBasicFile(r)
	case XFileType:
		err = ino.readExtendedFile(r)
	case SymlinkType, XSymlinkType:
		err = ino.readSymlink(r)
	}
	return ino, err
}

func (i *inode) fillSubs() error {
	if !i.typ.IsDir() {
		return nil
	}
	// basic dir, we need to iterate (dataCache data?)
	dr, err := i.sb.dirReader(i)
	if err != nil {
		return err
	}
	for {
		name, inoR, err := dr.next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
		if name == "" || name == "." || name == ".." {
			continue
		}
		ino, err := i.sb.getInodeRef(inoR)
		if err != nil {
			return err
		}
		i.subs = append(i.subs, &namedInode{name: name, ino: ino})
	}
	sort.Slice(i.subs, func(k, j int) bool { return i.subs[k].Name() < i.subs[j].Name() })
	for _, sub := range i.subs {
		if err = sub.(*namedInode).ino.fillSubs(); err != nil {
			return err
		}
	}
	return nil
}

// mode returns the inode's mode as fs.FileMode
func (i *inode) mode() fs.FileMode {
	return unixToMode(uint32(i.perm)) | i.typ.Mode()
}

// isDir returns true if the inode is a directory inode.
func (i *inode) isDir() bool {
	return i.typ.IsDir()
}

// readLink returns the inode's link
func (i *inode) readLink() (string, error) {
	if !i.typ.IsSymlink() {
		return "", fs.ErrInvalid
	}
	return string(i.symTarget), nil
}
