package squash

import "errors"

var (
	ErrInvalidFile    = errors.New("invalid file, squashfs signature not found")
	ErrInvalidSuper   = errors.New("invalid squashfs superblock")
	ErrInvalidVersion = errors.New("invalid file version, expected squashfs 4.0")
)
