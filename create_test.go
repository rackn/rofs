package rofs

import (
	"bytes"
	"crypto/ed25519"
	"crypto/rand"
	"gitlab.com/rackn/rofs/c"
	"io"
	"io/fs"
	"os"
	"testing"
)

func TestCreate(t *testing.T) {
	pub, priv, err := ed25519.GenerateKey(rand.Reader)
	if err != nil {
		t.Fatalf("Error generating signing keys: %v", err)
	}
	defer os.Remove("test.rofs")
	if err := CreateSigned(".", "test.rofs", priv); err != nil {
		t.Fatalf("Error creating test.rofs: %v", err)
	}
	fi, err := os.Open("test.rofs")
	if err != nil {
		t.Fatalf("Failed to open test.rofs: %v", err)
	}
	defer fi.Close()
	f2 := c.Mmap(fi)
	tfs, err := Open(f2)
	if err != nil {
		t.Fatalf("Failed to acces test.rofs as a read-only filesystem: %v", err)
	}
	ofs := os.DirFS(".")
	err = fs.WalkDir(tfs, ".", func(p string, d fs.DirEntry, e2 error) error {
		other, e3 := fs.Stat(ofs, p)
		if e3 != nil {
			return e3
		}
		ours, e3 := d.Info()
		if e3 != nil {
			return e3
		}
		if other.Name() != ours.Name() {
			t.Errorf("Name mismatch at %s: ours %s, other %s", p, ours.Name(), other.Name())
		}
		if other.Mode().Type() != ours.Mode().Type() {
			t.Errorf("Type mismatch at %s: ours %x, theirs %x", p, ours.Mode().Type(), other.Mode().Type())
		}
		if !other.Mode().IsRegular() {
			return nil
		}
		if other.Size() != ours.Size() {
			t.Errorf("Size mismatch at %s: ours %d, other %d", p, ours.Size(), other.Size())
		}
		of, e3 := tfs.Open(p)
		if e3 != nil {
			t.Errorf("Error opening %s in the archive: %v", p, e3)
			return e3
		}
		defer of.Close()
		othf, e3 := ofs.Open(p)
		if e3 != nil {
			t.Errorf("Error opening %s on the filesystem: %v", p, e3)
			return e3
		}
		defer othf.Close()
		tmta, _, terr := c.ReadMTA(tfs, p)
		omta, _, oerr := c.ReadMTA(ofs, p)
		if oerr == nil && terr == nil {
			if tmta.ModTime.Equal(omta.ModTime) && bytes.Equal(tmta.ShaSum, omta.ShaSum) {
				t.Logf("Recorded MTA and on-disk MTA identical for %s", p)
			} else {
				t.Errorf("Recorded MTA %s is not equal to on-disk MTA %s for %s", tmta, omta, p)
			}
		}
		switch checkErr := c.CheckSignatures(of, pub); checkErr {
		case nil:
			t.Logf("Signature verified")
		case fs.ErrNotExist:
			t.Errorf("Signature not recorded or malformed")
		case fs.ErrInvalid:
			t.Errorf("Signature not recognized")
		}
		b1 := make([]byte, 1<<15)
		b2 := make([]byte, 1<<15)
		var n1, n2 int
		var re1, re2 error
		t.Logf("Testing for identical file contents on %s", p)
		for sz := int64(0); sz < other.Size(); {
			n1, re1 = of.(io.ReaderAt).ReadAt(b1, sz)
			n2, re2 = othf.(io.ReaderAt).ReadAt(b2, sz)
			if re1 == io.ErrUnexpectedEOF {
				re1 = io.EOF
			}
			if re2 == io.ErrUnexpectedEOF {
				re2 = io.EOF
			}
			if re1 != re2 {
				t.Errorf("Mismatching read errors from archive and disk: %v vs %v", re1, re2)
				break
			}
			if n1 != n2 {
				t.Errorf("Mismatched read lengths: %d vs %d", n1, n2)
				break
			}
			if n1 < 0 {
				if !bytes.Equal(b1[:n1], b2[:n2]) {
					t.Errorf("Divergent content read from archive vs filesystem")
					break
				}
			}
			sz += int64(n1)
		}
		if p == "squash/testdata/zstd.squashfs" {
			sq, sqErr := Open(of.(c.Src))
			if sqErr != nil {
				t.Errorf("Error opening %s as a squashfs: %v", p, sqErr)
			}
			expectedContents, err := os.ReadFile("squash/testdata/README.md")
			if err != nil {
				t.Fatalf("Failed to open expected contents: %v", err)
			}
			// Test the boring old read path.
			buf, err := fs.ReadFile(sq, "README.md")
			if err != nil {
				t.Errorf("Error reading README: %v", err)
			}
			if !bytes.Equal(expectedContents, buf) {
				t.Errorf("Want %s, have %s", expectedContents, buf)
			}
			// Test WriteTo
			bb := &bytes.Buffer{}
			f2, _ := sq.Open("README.md")
			_, err = io.Copy(bb, f2)
			if err != nil {
				t.Errorf("Error copying README: %v", err)
			}
			if !bytes.Equal(bb.Bytes(), expectedContents) {
				t.Errorf("Want %s, have %s", expectedContents, buf)
			}
		}
		if p == "iso/testdata/rockridge.iso" {
			sq, sqErr := Open(of.(c.Src))
			if sqErr != nil {
				t.Errorf("Error opening %s as a squashfs: %v", p, sqErr)
			}
			expectedContents := []byte("README\n")
			buf, err := fs.ReadFile(sq, "README.md")
			if err != nil {
				t.Errorf("Error reading README: %v", err)
			}
			if !bytes.Equal(expectedContents, buf) {
				t.Errorf("Want %s, have %s", expectedContents, buf)
			}
			// Test WriteTo
			bb := &bytes.Buffer{}
			f2, _ := sq.Open("README.md")
			_, err = io.Copy(bb, f2)
			if err != nil {
				t.Errorf("Error copying README: %v", err)
			}
			if !bytes.Equal(bb.Bytes(), expectedContents) {
				t.Errorf("Want %s, have %s", expectedContents, buf)
			}
		}
		return nil
	})
}
