package rofs

import (
	"archive/tar"
	"bufio"
	"crypto/ed25519"
	"github.com/klauspost/compress/zstd"
	"gitlab.com/rackn/rofs/c"
	"gitlab.com/rackn/seekable-zstd"
	"io"
	"io/fs"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strings"
)

// CreateSigned creates a new ROFS archive at target from a directory
// hierarchy located at src.  The individual files on the filesystem will
// have extended attributes added to them that cover the SHA256 of the file and
// an ed25519 signature of that SHA256sum if key is not nil.
// It currently creates a tar archive that  is compressed using seekable zstd
// and organized to optimize compressability.
// In the future it may create a  more highly optimized archive format such as
// a squashfs image.

// Size of an individual chunk in a seekable zstd stream.
const chunkSize = 1 << 17

func CreateSigned(src, target string, key ed25519.PrivateKey) error {
	var tw *tar.Writer
	var bw *bufio.Writer
	var hdrs []*tar.Header
	var record func(string) error
	record = func(p string) error {
		st, err := os.Lstat(p)
		if err != nil {
			return err
		}
		fi, err := os.Open(p)
		if err != nil {
			return err
		}
		defer fi.Close()
		mta := &c.ModTimeSha{}
		var signature []byte
		if st.Mode().IsRegular() {
			if _, err = mta.Regenerate(fi); err != nil {
				return err
			}
			if key != nil {
				if signature, err = c.SignHash(key, mta.ShaSum); err != nil {
					return err
				}
			}
			st, err = os.Lstat(p)
			if err != nil {
				return err
			}
		}
		var link string
		if st.Mode()&fs.ModeSymlink > 0 {
			pDir := path.Dir(p)
			link, err = os.Readlink(p)
			if err != nil {
				return err
			}
			if link[0] != '/' {
				link = path.Join(pDir, link)
				if !path.IsAbs(link) {
					// Link is nonsensical.
					return fs.ErrInvalid
				}
			}
			if !strings.HasPrefix(link, src+"/") {
				// Link points outside this archive, it might break our consumers.
				return fs.ErrInvalid
			}
			link, err = filepath.Rel(pDir, link)
			if err != nil {
				return err
			}
		}
		hdr, err := tar.FileInfoHeader(st, link)
		if err != nil {
			return err
		}
		hdr.Name = p
		hdr.Format = tar.FormatPAX
		if mta.ShaSum != nil {
			hdr.Xattrs = map[string]string{}
			if buf, err := mta.MarshalBinary(); err == nil {
				hdr.Xattrs[c.MTATag] = string(buf)
			}
			if signature != nil {
				hdr.Xattrs[c.SignTag] = string(signature)
			}
		}
		hdrs = append(hdrs, hdr)
		if !st.IsDir() {
			return nil
		}
		ents, err := os.ReadDir(p)
		if err != nil {
			return err
		}
		names := make([]string, len(ents))
		for i := range ents {
			names[i] = path.Join(p, ents[i].Name())
		}
		ents = nil
		for i := range names {
			if err = record(names[i]); err != nil {
				return err
			}
		}
		return nil
	}
	save := func(hdr *tar.Header) (err error) {
		if int64(bw.Available()) < hdr.Size+512 {
			// Bias the underlying zstd chunk stream towards keeping
			// unrelated files separate after files sizes start to approach
			// chunk sizes
			if err = bw.Flush(); err != nil {
				return
			}
		}
		if err = tw.WriteHeader(hdr); err != nil {
			return
		}
		if !hdr.FileInfo().Mode().IsRegular() {
			return
		}
		var fi *os.File
		fi, err = os.Open(hdr.Name)
		if err != nil {
			return
		}
		defer fi.Close()
		var sz int64
		sz, err = io.Copy(tw, fi)
		if err != nil {
			return
		}
		if sz != hdr.Size {
			err = fs.ErrInvalid
			return
		}
		return
	}
	ents, err := os.ReadDir(src)
	if err != nil {
		return err
	}
	w, err := os.Create(target)
	if err != nil {
		return err
	}
	zw, _ := zstd.NewWriter(nil, zstd.WithEncoderLevel(zstd.SpeedBetterCompression))
	cw, _ := seekable.NewWriter(w, zw)
	bw = bufio.NewWriterSize(cw, chunkSize)
	tw = tar.NewWriter(bw)
	for i := range ents {
		if err = record(path.Join(src, ents[i].Name())); err != nil {
			return err
		}
	}
	// Pack the smallest entries first.  If entries have the
	// same size, sort them by name.  This will wind up packing
	// all the directories first (since they have zero length),
	// then coalesce files significantly smaller than our chosen chunk
	// size into larger chunks, then graduate to one or more chunks per file.
	sort.Slice(hdrs, func(i, j int) bool {
		if hdrs[i].Size == hdrs[j].Size {
			return hdrs[i].Name < hdrs[j].Name
		}
		return hdrs[i].Size < hdrs[j].Size
	})
	for i := range hdrs {
		if err = save(hdrs[i]); err != nil {
			return err
		}
	}
	if err = tw.Close(); err != nil {
		return err
	}
	if err = bw.Flush(); err != nil {
		return err
	}
	if err = cw.Close(); err != nil {
		return err
	}
	return w.Close()
}

// Create creates an archive that does not include the signature extended attributes.
func Create(src, target string) error {
	return CreateSigned(src, target, nil)
}
