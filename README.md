# rofs
rofs is a [go](https://golang.org) library for making readonly file archives available without requiring
external tools or elevated privileges.

This library primarily concerns itself with removing the need to expand read-only archives when you do not
actually need to modify anything in them -- the goal is to provide uniform access to such archives via the
[fs.FS](https://pkg.go.dev/io/fs) interface.

## Design Goals

* Conform to all io/fs interfaces, including all the optional ones defined in that package along with the
  forthcoming ReadLinkFS interface
* Provide an optimized io.WriterTo interface that is capable of proper sparse file handling where reasonable.
* Allow the rofs/c.File interface to be passed in as the data source to rofs.Open.  This will allow this library to
  be used to handle archives of archives.
* Provide a generic low-effort way of creating an ROFS conformant archive.
* Have reasonable symbolic link handling.

## Filesystems

### ISO9660
Filesystem images in ISO9660 are supported, along with RockRidge extensions including large sparse files.
Joliet support may be added in a future release, but it is not a high priority.

The ISO9660 code is derived in large part from the excellent
[go-diskfs](https://github.com/diskfs/go-diskfs) library by Avi Deitcher.

### Squashfs
All compression variants of Squashfs are supported by default, with the exception of LZO due to GPL entanglements.

Squashfs code is derived from https://github.com/diskfs/go-diskfs/tree/master/filesystem/squashfs

### Tar
Uncompressed tar files are supported, along with tar files compressed using seekable zstd.

Tar code is derived from the Go standard library archive/tar package, modified to remove all write capabilities and
work with our standard FS and extent handling code.

## Usage
Detailed Go documentation is available at [pkg.go.dev](https://pkg.go.dev/gitlab.com/rackn/rofs).

### Examples

There are runnable examples in the [cmds/](./cmds/) directory. Here is one to get you started.

## Tests
Standard unit tests are runnable via `go test ./...`

## Plans
Future plans are to add the following:

* `Joliet` extensions to `iso9660`
* `UDF` filesystems for DVD handling.
